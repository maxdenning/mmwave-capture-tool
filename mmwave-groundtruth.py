import cmapy
import cv2 as cv
import numpy as np
import os
import sys
from mmwave.postprocessing import postprocessing, utils, tracking
from mmwave.radar import RadarConfig


def launch_gui(scene, estimated_windows, show_estimates=False):
    i = 0
    window = [0, 0, 32, 32]

    cv.namedWindow('img', cv.WINDOW_NORMAL)
    cv.resizeWindow('img', int(scene.shape[1]*2.5), int(scene.shape[2]*2.5))

    while i < len(scene):
        # draw image
        frame = cv.applyColorMap(postprocessing.rdm_to_greyscale(scene[i]), cmapy.cmap('viridis'))
        if show_estimates:
            estimated = estimated_windows[i]
            frame = cv.rectangle(frame, (estimated[1], estimated[0]), (estimated[1]+estimated[3], estimated[0]+estimated[2]), 0, 1)
        frame = cv.rectangle(frame, (window[1], window[0]), (window[1]+window[3], window[0]+window[2]), 255, 1)
        cv.imshow('img', cv.rotate(frame, cv.ROTATE_90_COUNTERCLOCKWISE))

        # get input
        key = cv.waitKey(5000)
        if key == ord('x'):
            break
        
        # window position
        elif key == ord('w'):
            window[1] += 1
        elif key == ord('s'):
            window[1] -= 1
        elif key == ord('a'):
            window[0] -= 1
        elif key == ord('d'):
            window[0] += 1
        
        # window size
        elif key == ord('W'):
            window[3] += 1
        elif key == ord('S'):
            window[3] -= 1
        elif key == ord('A'):
            window[2] -= 1
        elif key == ord('D'):
            window[2] += 1

        # add estimate frame
        elif key == ord('e'):
            show_estimates = not show_estimates

        # select window and move forward
        elif key == 13:
            yield i, tuple(window)
            i += 1

            if not i % 25:
                print(f'Frame: {i}')


def estimate_windows(scene):
    tracker = tracking.DynamicCAMShiftTracker((8,8), (32,32), radar_config, 'min', sample_length=50, debug=False)

    windows = [()] * len(scene)
    for i, window in tracker.track(iter(scene)):
        windows[i] = window
    return windows


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print(f'usage: {sys.argv[0]} <input file> <output dir>')
        exit(1)

    input_filepath = sys.argv[1]
    output_filepath = os.path.join(sys.argv[2], os.path.basename(input_filepath).split('.')[0] + '_groundtruth.csv')
    os.makedirs(os.path.dirname(output_filepath), exist_ok=True)
    radar_config = RadarConfig(utils.get_radar_config_path(os.path.dirname(input_filepath)))
    scene = np.load(input_filepath)
    print(f'Input: {scene.shape}\nOutput: {output_filepath}')

    postprocessing.zero_vbin_removal(scene, radar_config)
    estimated_windows = estimate_windows(scene)

    windows = []
    for i, next_window in launch_gui(scene, estimated_windows):
        windows.append((i, next_window))

    with open(output_filepath, 'w') as output_file:
        for i, window in windows:
            output_file.write(f'{i},{",".join(str(v) for v in window)}\n')
    
    with open(os.path.join(sys.argv[2], 'source.txt'), 'w') as ofile:
        ofile.write(input_filepath)
