import multiprocessing as mp
import os
import sys
import time
from processData import processFrame, FRAME_SIZE_BYTES, framePeriod, nChirps
from thread_flag import read_flag, create_safe_flag

import matplotlib.pyplot as plt
import numpy as np

from mmwave.radar import RadarConfig
from functools import partial

from mmwave.postprocessing import postprocessing, utils, tracking
from scipy import signal

from struct import unpack

from multiprocessing import Pool, cpu_count

import time
import cv2 as cv
import json

from mmwave.metadata import SampleDatabase, SampleMetaData, SampleClassification, SampleScenario, SampleSet

from scipy.stats import describe


def peaks(frame, m, n, false_alarm_rate, npeaks, roi_x, roi_y):
    frame_peaks = utils.peaks_of_rdm(frame, npeaks, m=m, n=n, P_fa=false_alarm_rate)
    return [utils.peaks_to_rdm(frame_peaks, frame, roi_x, roi_y), frame_peaks]


def rdm_scene_peaks(scene, m, n, false_alarm_rate, npeaks, roi_x, roi_y):
    frame_peak_pairs = []
    with Pool(cpu_count()) as pool:
        peaks_fn = partial(peaks, m=m, n=n, false_alarm_rate=false_alarm_rate, npeaks=npeaks, roi_x=roi_x, roi_y=roi_y)
        frame_peak_pairs = pool.map(peaks_fn, scene)
    # turn columns into two lists to return
    return (np.array([pair[0] for pair in frame_peak_pairs]), [pair[1] for pair in frame_peak_pairs])


def extract_target_samples(scene, sample_length, windows):
    target_sample = [[]] * sample_length
    for i, window in windows:
        x, y, w, h = window
        target_sample[i % sample_length] = scene[i, x:x+w, y:y+h]
        if not (i + 1) % sample_length:
            yield target_sample


def preprocess_frame(frame, nguard, ntest, fa_rate, npeaks):
    # TODO: add other noise removal here, or in fft step?
    postprocessing.zero_vbin_removal(frame, val=utils.POSTPROC_dB_BASELINE)
    frame_peaks = utils.peaks_of_rdm(frame, nguard, ntest, fa_rate, npeaks)
    return frame, frame_peaks


def buffer_scene_sample(scene_iter, sample_length, nguard, ntest, fa_rate, npeaks):
    frame_0 = next(scene_iter)
    scene_sample = np.zeros((sample_length, *frame_0.shape), dtype=float)
    scene_sample_peaks = [[]] * sample_length
    
    preprocess_frame_fn = partial(preprocess_frame, nguard=nguard, ntest=ntest, fa_rate=fa_rate, npeaks=npeaks)
    with Pool(cpu_count()) as pool:
        for i, (frame, frame_peaks) in enumerate(pool.imap(preprocess_frame_fn, scene_iter, chunksize=1)):
            # j = (i+1) % sample_length  # adjust i into sample_length range
            scene_sample[i % sample_length] = frame
            scene_sample_peaks[i % sample_length] = frame_peaks

            if not (i+1) % sample_length:  # if j = 0, enough frames have been collected
                yield scene_sample, scene_sample_peaks


def is_approaching_target(scenario):
    return scenario in [SampleScenario.S_PEDESTRIAN_STRAIGHT_APPROACHING, SampleScenario.S_PEDESTRIAN_DIAGONAL_APPROACHING,
                        SampleScenario.S_BICYCLE_STRAIGHT_APPROACHING, SampleScenario.S_BICYCLE_DIAGONAL_APPROACHING,
                        SampleScenario.S_CAR_STRAIGHT_APPROACHING_FORWARD, SampleScenario.S_CAR_STRAIGHT_APPROACHING_REVERSE,
                        SampleScenario.S_CAR_DIAGONAL_APPROACHING_FORWARD, SampleScenario.S_CAR_DIAGONAL_APPROACHING_REVERSE,
                        SampleScenario.M_PEDESTRIAN_STRAIGHT_APPROACHING, SampleScenario.M_PEDESTRIAN_DIAGONAL_APPROACHING]


def get_tracker(tracker_type, sample_res, radar_config, **kwargs):
    # validate input
    if tracker_type not in ['es', 'fp', 'fcs', 'dcsn', 'dcsx']:
        raise ValueError(f'Tracker type not recognised: {tracker_type}')


    # determine the type of tracker to use
    if tracker_type == 'es' or kwargs['classification'] is SampleClassification.NONE:
        fixed_res = True
        tracker = tracking.EmptySceneTracker(sample_res, radar_config, kwargs['scene_length'], kwargs['sample_length'])

    elif tracker_type == 'fp':
        fixed_res = True
        tracker = tracking.FixedPeakTracker(sample_res, radar_config, kwargs['dist_threshold'], **kwargs['cfar'])

    elif tracker_type == 'fcs':
        fixed_res = True
        tracker = tracking.FixedCAMShiftTracker(sample_res, radar_config, **kwargs['cfar'], debug=kwargs.get('debug', False))
        
    elif tracker_type == 'dcsn':  # dynamic min
        fixed_res = False
        tracker = tracking.DynamicCAMShiftTracker(kwargs['min_sample_res'], sample_res, radar_config, 'min', **kwargs['cfar'], debug=kwargs.get('debug', False))
    
    elif tracker_type == 'dcsx':  # dynamic max
        fixed_res = False
        tracker = tracking.DynamicCAMShiftTracker(kwargs['min_sample_res'], sample_res, radar_config, 'max', kwargs['sample_length'], **kwargs['cfar'], debug=kwargs.get('debug', False))

    else:
        raise ValueError(f'tracker type not handled: {tracker_type}')
    

    # tracker = tracking.DynamicSmoothedTracker(roi, **cfar_args, debug=True)
    # tracker = tracking.DynamicROIHybridTracker(roi, **cfar_args, debug=True)
    # start_frame = scene[0]
    # postprocessing.zero_vbin_removal(start_frame, val=utils.POSTPROC_dB_BASELINE)
    # tracker = tracking.FixedROIMeanShiftTracker(roi, utils.peaks_of_rdm(start_frame, *cfar_args.values())[0], camshift=True, debug=True)
    
    
    return tracker, fixed_res



# RDM SCENE: arbitrary length, single target(?)
# RDM FRAME: single frame from an rdm scene
# RDM SAMPLE: roi from an rdm scene, 2 seconds long, single target
#
# Post Processing Pipeline
#   Raw ADC Data Packets
#   -- Packet Reorder & Zerofill Utility    >> RAW ADC Data
#   -- Range Doppler FT                     >> Range-Power, RDA, RDM
#   -- Noise Removal                        >> Clean RDM
#   -- Target Extraction                    >> RDM Targets
#   -- Time Slice                           >> RDM Target Time Slice
#   -- More...


# TODO: write notes on speed classifier, not implemented because of limited data set speed variation
# TODO: rename to SCENE/FRAME/SAMPLE

# TODO: variable size frames?
# TODO: summary statistics for scene[0] + make notes
# TODO: allow switching from exporting sample files to exporting directly into a NN for application-style usage

# TODO: find quality of sample using % of frames with peaks over the scene[0] average?
# TODO: use very permissive cfar as noise removal? - e.g. allow >100 points



if __name__ == '__main__':
    if len(sys.argv) < 6:
        print(f'usage: {sys.argv[0]} <input file> <output dir> <output sample db> <classification str> <scenario code> <sample set> <tracker type>')
        print('valid tracker types: es, fp, fcs, dcsn, dcsx')
        exit(1)

    input_filepath = sys.argv[1]
    input_dir = os.path.dirname(input_filepath) # directory containing an rdm scene file
    input_filename = os.path.basename(input_filepath).split('.')[0]
    output_dir = sys.argv[2]
    sample_database_path = utils.DEFAULT_SAMPLE_RECORD if sys.argv[3] == '.' else sys.argv[3]
    classification = SampleClassification[sys.argv[4].upper()]
    scenario = SampleScenario(sys.argv[5].lower())
    sample_set = SampleSet(sys.argv[6].lower())
    tracker_type = sys.argv[7].lower()

    os.makedirs(output_dir, exist_ok=True)
    radar_config = RadarConfig(utils.get_radar_config_path(input_dir))
    scene = np.load(input_filepath)


    # ------------------------------------------------------------------------------------------------------------------


    # postprocessing.zero_vbin_removal(scene, val=utils.POSTPROC_dB_BASELINE)
    # pscene, peaks = rdm_scene_peaks(scene[:100], 9, 9, 0.003, 0, 3, 3)
    # np.save(os.path.join(output_dir, 'noise.npy'), pscene)
    # exit(1)
    # cfar_args = {'nguard': 2, 'ntest': 5, 'fa_rate': 0.528, 'npeaks': 4}


    sample_res = (32, 32) # if tracker_type != 'dcs' else (38, 38)
    sample_length = int(utils.SAMPLE_LENGTH_SEC / radar_config.frame_period)  # frames per sample
    sample_database = SampleDatabase(sample_database_path)
    scene_windows = []

    tracker, is_fixed_res = get_tracker(tracker_type, sample_res, radar_config, **{
        'cfar': {'m': 9, 'n': 9, 'P_fa': 0.05, 'gm': 3, 'gn': 3},
        'classification': classification,
        'debug': False,
        'dist_threshold': 8.5,
        'sample_length': sample_length,
        'scene_length': len(scene),
        'min_sample_res': (8,8)
    })
    print(f'Tracker: {type(tracker).__name__}')
    print(f'Sample Output Resolution: {sample_res}')
    print(f'Scene Length: {scene.shape}')

    postprocessing.zero_vbin_removal(scene, radar_config)

    # peaks are easier to identify at close range, so flip approaching targets before track
    is_flipped = is_approaching_target(scenario)
    offset = 0
    if is_flipped:
        scene = np.flip(scene, axis=0)
        offset = len(scene) % sample_length

    # track targets and extract windows to make samples
    for i, (sample_start, sample_windows) in enumerate(tracker.track_samples(iter(scene), sample_length)):
        sample = tracker.extract(scene[sample_start:sample_start+sample_length], sample_res, sample_windows)

        if is_flipped:
            sample = np.flip(sample, axis=0)  # flip appraoching targets back
            sample_windows = list(reversed(sample_windows))
            sample_start = offset + len(scene) - (len(scene) % sample_length) - sample_length - sample_start  # adjust start idx

        scene_windows.extend(zip(range(sample_start, sample_start + sample_length), sample_windows))  # for debug/graphing

        utils.export_sample(
            sample_database, sample, os.path.join(output_dir, f'{sample_start}.npy'), classification, scenario, sample_set, input_filepath,
            sample_windows[sample_length//2][0] + (sample_windows[sample_length//2][2] // 2),
            sample_windows[sample_length//2][1] + (sample_windows[sample_length//2][3] // 2),
            write_fn=np.save)

    print(f'Samples Created from {input_filename}: {i+1}')

    if is_flipped:
        scene_windows = list(reversed(scene_windows))  # flip approaching target windows back
    utils.write_target_track(os.path.join(output_dir, 'track.csv'), scene_windows)  # output the track window coords
    sample_database.save()
