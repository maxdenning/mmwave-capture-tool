import os
import processData as pd
from functools import partial
from math import ceil
from multiprocessing import Queue
import pyqtgraph as pg
import numpy as np
from pyqtgraph.Qt import QtCore, QtGui
import plots
import sys



header_size = 10
data_size = 1460
packet_size = header_size + data_size
frame_size = pd.FRAME_SIZE_BYTES
packets_per_frame = ceil(frame_size / float(data_size))
print('HEADER:', header_size, 'DATA:', data_size, 'PACKET:', packet_size, 'FRAME:', frame_size, 'P/F:', packets_per_frame)

slice_period = 2000  # ms
frame_periodicity = 40.0  # ms
frames_per_slice = (1/frame_periodicity) * slice_period
print('SLICE PERIOD:', slice_period, 'FRAME PERIOD:', frame_periodicity, 'F/S:', frames_per_slice)

# chunk_counter = 0
# slice_counter = 0

# adc_packets_out_path = '/home/max/radar-project/data/18-11-19/1tx/slices/slice{}.bin'
# adc_packets_out = open(adc_packets_out_path.format(slice_counter), 'wb')


# with open('/home/max/radar-project/data/18-11-19/1tx/raw-adc_Raw_0.bin', 'rb') as adc_packets_in:
#     print(adc_packets_in.name, os.path.getsize(adc_packets_in.name))
#     for chunk in iter(partial(adc_packets_in.read, packets_per_frame * packet_size), b''):
#         # chunk is the size of one frame (plus headers)

#         adc_packets_out.write(chunk)
#         chunk_counter += 1

#         if chunk_counter >= frames_per_slice:  # move to next file
#             chunk_counter = 0
#             slice_counter += 1
#             adc_packets_out.close()
#             print('OUTPUT:', adc_packets_out.name, os.path.getsize(adc_packets_out.name))
#             adc_packets_out = open(adc_packets_out_path.format(slice_counter), 'wb')


# adc_packets_out.close()
# print('OUTPUT:', adc_packets_out.name, os.path.getsize(adc_packets_out.name))




# REM import sys
# REM from os import listdir
# REM from os.path import isfile, join


# REM if __name__ == '__main__':
# REM     if len(sys.argv) < 2:
# REM         exit(1)

# REM     data_dir = sys.argv[1]
# REM     files = [f for f in listdir(data_dir) if isfile(join(mypath, f) and f[])]
    
# REM     for f in listdir(data_dir):
# REM         fpath = join(data_dir, f)
# REM         if isfile(fpath) and f == 'adc_Raw_0.bin':








# def read_packet_header(data):
#     seq_num = int.from_bytes(data[0:4], byteorder='little')
#     byte_count = int.from_bytes(data[4:10], byteorder='little')  # This lags by at least set of data transmitted.
#     #something = int.from_bytes(data[10:14], byteorder='little')
#     adc_data_size = len(data[10:])
#     return seq_num, byte_count, adc_data_size


# file = open('/home/max/radar-project/data/18-11-19/1tx/raw-adc_Raw_0.bin', 'rb')
# print(file.name, os.path.getsize(file.name))

# ofile = open('/home/max/radar-project/data/18-11-19/1tx/slice.bin', 'wb')
# logfile = open('/home/max/radar-project/data/18-11-19/1tx/slicer.log', 'w')


# outqueue = Queue()


# prev_seq_num = 0
# prev_byte_count = 0

# 125000 works!
# next to test: 137500

# for i in range(125000):

# # odata = ofile.read(pd.FRAME_SIZE_BYTES)
# # pd.processFrame(odata, outqueue)



#     # print(len(rangeAxis), len(ydata))
#     # print(rangeAxis[:10], ydata[:10])

    # data = file.read(packet_size)
    # seq_num, byte_count, adc_data_size = read_packet_header(data)
    # frame_id = int( (byte_count + adc_data_size) / frame_size )

#     byte_count_delta = byte_count - prev_byte_count
#     prev_byte_count = byte_count

    # print(seq_num, byte_count, adc_data_size, frame_id)


    # ofile.write(data)
#     logfile.write('{} {} {} {}\t{}\n'.format(seq_num, byte_count, adc_data_size, frame_id, byte_count_delta))


#     # data = file.read(10)
#     # seq_num, byte_count, adc_data_size = read_packet_header(data)
#     # frame_id = int( (byte_count + adc_data_size) / pd.FRAME_SIZE_BYTES  )

#     # if seq_num < 100:
#         # print(seq_num, byte_count, adc_data_size, frame_id)

#     # if seq_num - 1 == prev_seq_num:
#     #     prev_seq_num = seq_num
#     #     print(i, prev_seq_num, seq_num, byte_count)
    
#     # if not i % 1000:
#     #     print('I:', i)


print('close')
# file.close()
# ofile.close()
# logfile.close()
# app = QtGui.QApplication(sys.argv)
# thisapp = plots.App(outqueue, None)
# thisapp.show()


# sys.exit(app.exec_())
