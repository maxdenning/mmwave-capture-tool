import win32com.client

MQ_SEND_ACCESS = 2
MQ_DENY_NONE = 0

queueinfo = win32com.client.Dispatch('MSMQ.MSMQQueueInfo')
queueinfo.PathName = '.\\private$\\mmwave-message-interface'
queue = queueinfo.Open(MQ_SEND_ACCESS, MQ_DENY_NONE)

while True:
    body_input = input('Enter message: ')

    if body_input == 'delete':
        queueinfo.Delete()
        break
    
    elif body_input == 'startframe':
        body_input = 'ar1.StartFrame()'
        
    elif body_input == 'startrecord':
        body_input = 'ar1.CaptureCardConfig_StartRecord("C:\\\\dump\\\\adc.bin")'

    elif body_input == 'stopframe':
        body_input = 'ar1.StopFrame()'

    elif body_input == 'stoprecord':
        body_input = 'ar1.CaptureCardConfig_StopRecord()'


    msg = win32com.client.Dispatch('MSMQ.MSMQMessage')
    msg.Label = 'py2lua'
    msg.Body = body_input
    msg.Send(queue)

    if body_input == 'STOP':
        break

queue.Close()
