import cv2
import numpy as np
import pyqtgraph as pg
import sys
import time
from matplotlib import cm
from PyQt5.QtGui import QInputDialog, QFileDialog
from pyqtgraph.Qt import QtCore, QtGui

# from process_starter import start_adc, start_raw_data_processing
# from thread_flag import create_safe_flag, read_flag, set_flag

import os.path
import multiprocessing as mp

from functools import partial

# app = QtGui.QApplication(sys.argv)
# capture_directory = QFileDialog.getExistingDirectory(None, options=QtGui.QFileDialog.DontUseNativeDialog)
# print(capture_directory, os.path.join(capture_directory, 'data.bin'))




def read_packet_header(data):
    seq_num = int.from_bytes(data[0:4], byteorder='little')
    byte_count = int.from_bytes(data[4:10], byteorder='little')  # This lags by at least set of data transmitted.
    adc_data_size = len(data[10:]) - 4
    return seq_num, byte_count, adc_data_size



header_size = 10
buffer_size = 4
data_size = 1456
packet_size = header_size + buffer_size + data_size

dup_seq = 1710489 
found = False

last_seq = -1
with open('data/19-02-16_morning/15-26/adc_Raw_2__OUT.bin', 'rb') as raw:
    # with open('data/19-02-16_morning/15-26/adc_Raw_2__OUT.bin', 'wb') as out:

    for i, packet in enumerate(iter(partial(raw.read, packet_size), b'')):
        seq_num, byte_count, adc_data_size = read_packet_header(packet)

        if seq_num == dup_seq:
            print('dup', seq_num, last_seq)
            if found:
                continue
            found = True

        # out.write(packet)
        last_seq = seq_num



    # data = raw.read(packet_size)
    




