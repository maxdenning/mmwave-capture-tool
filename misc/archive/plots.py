import cv2
import numpy as np
import pyqtgraph as pg
import sys
import time
from matplotlib import cm
from PyQt5.QtGui import QInputDialog, QFileDialog
from pyqtgraph.Qt import QtCore, QtGui


class App(QtGui.QMainWindow):
    def __init__(self, data_queue, radar_client):
        super().__init__(parent=None)
        self.__init_gui()

        self.data_queue = data_queue
        self.radar_client = radar_client
        self.camera_stream = cv2.VideoCapture(0)
        self.is_capturing = False
        self.capture_directory = ''

        # FPS helpers
        self.fps = 0.
        self.lastupdate = time.time()

        # Start
        self.updater = pg.QtCore.QTimer(self)
        self.updater.timeout.connect(self.update)
        self.updater.start(0)

    def __init_gui(self):
        # Create top-level gui container elements
        self.mainbox = QtGui.QWidget()
        self.setCentralWidget(self.mainbox)
        self.mainbox.setLayout(QtGui.QVBoxLayout())
        self.canvas = pg.GraphicsLayoutWidget()
        self.mainbox.layout().addWidget(self.canvas)

        # Label to display FPS
        self.fps_label = QtGui.QLabel()
        self.mainbox.layout().addWidget(self.fps_label)

        self.capture_btn = QtGui.QPushButton('Begin capture')
        self.mainbox.layout().addWidget(self.capture_btn)
        self.capture_btn.clicked.connect(self.on_capture_btn_clicked)

        # Setup RDM plot within view box
        self.rdm_viewbox = self.canvas.addViewBox()
        self.rdm_viewbox.setAspectLocked(False)
        self.rdm_viewbox.setRange(QtCore.QRectF(0,0, 200, 300))
        self.rdm_plot = pg.ImageItem(border='w')
        self.rdm_viewbox.addItem(self.rdm_plot)

        # Set colormap for RDM
        colormap = cm.get_cmap("jet")
        colormap._init()
        lut = (colormap._lut * 255).view(np.ndarray)  # Convert matplotlib colormap from 0-1 to 0 -255 for Qt
        self.rdm_plot.setLookupTable(lut)  # Set the colour map

        # Camera display
        self.cam_viewbox = self.canvas.addViewBox()
        self.cam_viewbox.setAspectLocked(True)
        self.cam_viewbox.setRange(QtCore.QRectF(0,0, 500, 500))
        self.cam_img = pg.ImageItem(border='w')
        self.cam_viewbox.addItem(self.cam_img)

        self.canvas.nextRow()

        #  Range FFT plot
        self.range_plot_container = self.canvas.addPlot(title='Range FFT', labels={'bottom': 'range', 'left': 'power'})
        self.range_plot_container.setXRange(0, 10, padding=0)
        self.range_plot_container.setYRange(-60, 10, padding=0)
        #self.p1.setXRange(-90, 90, padding=0)
        #self.p1.setYRange(-80, 90, padding=0)
        self.range_plot = self.range_plot_container.plot(pen='y')        

        # Cartesian plot.
        # self.p2 = self.canvas.addPlot()
        # self.h2 = self.p2.plot(pen=None, symbol='o', symbolSize='2', symbolPen='b',brush='b')
        # self.h3 = self.p2.plot(pen=None, symbol='+', symbolSize='4', symbolPen='r', brush='r')
        # self.p2.setXRange(-8, 8, padding=0)
        # self.p2.setYRange(0, 12, padding=0)

        # Angle FFT row.
        '''
        self.canvas.nextRow()
        self.p4 = self.canvas.addPlot()
        self.h4 = self.p4.plot(pen='y')
        self.p4.setXRange(-90, 90, padding=0)
        self.p4.setYRange(-10, 50, padding=0)
        '''

    def update(self):

        # TODO: refactor data_queue items to class?
        # Retrieve data items from the queue.
        # latestData = self.data_queue.get()
        # self.rangeAxis = latestData[0]
        # self.vAxis = latestData[1]
        # self.angAxis = latestData[2]
        # self.angFFT = latestData[3]
        # self.ydata = latestData[4]
        # self.rdm = np.transpose(latestData[5])
        # self.rda = np.transpose(latestData[7])


        # if len(latestData[6][0]) > 0 :
        #     targetCoordsX = latestData[6][0]
        #     targetCoordsY = latestData[6][1]
        #     stationaryFlag = latestData[6][2]

        #     # Set the detected target plots.
        #     self.h2.setData(targetCoordsX[stationaryFlag == True],
        #                     targetCoordsY[stationaryFlag == True])

        #     self.h3.setData(targetCoordsX[stationaryFlag == False],
        #                     targetCoordsY[stationaryFlag == False])

        #     '''
        #     self.h4.setData(self.angAxis,
        #                     self.angFFT[0,:]) # Just display first bin.
        #     '''

        # Set the range doppler map image.
        self.rdm_plot.setImage(self.rdm, levels=[-50,30])

        # Set the range FFT result.
        self.range_plot.setData(self.rangeAxis, self.ydata)

        # Get new image and set in the display.
        if self.camera_stream.isOpened():
            try:
                ret, frame = self.camera_stream.read()
                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                frame = cv2.resize(frame, (640, 480))
                frame = cv2.transpose(frame)
                frame = cv2.flip(frame, 1)
                self.cam_img.setImage(frame)
            except:
                print('Failed to get camera frame')
                self.camera_stream.release()

        # try:
        #     ret, frame = self.camera_stream.read()
        #     print("ret " + str(ret))
        #     frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        #     frame = cv2.resize(frame, (640, 480))
        #     frame = cv2.transpose(frame)
        #     frame = cv2.flip(frame, 1)
        #     self.cam_img.setImage(frame)
        # except:
        #     print('Failed to get camera frame, stopping updater')
        #     self.camera_stream.release()
        #     # self.updater.stop()

        # TODO: as live output save the raw ADC, raw video,
        #       and stitch video and RDM into a side-by-side frame-to-frame match video

        # Update FPS label
        self.fps_label.setText('Mean Frame Rate:  {fps:.3f} FPS'.format(fps=self.getfps()))
    
    def getfps(self):
        now = time.time()
        dt = (now-self.lastupdate)
        if dt <= 0:
            dt = 0.000000000001
        fps2 = 1.0 / dt
        self.lastupdate = now
        self.fps = self.fps * 0.9 + fps2 * 0.1
        return self.fps

    def closeEvent(self, event):
        self.updater.stop()
        self.radar_client.end_capture()
        self.radar_client.close()
        self.camera_stream.release()
        print('Closed')

    def on_capture_btn_clicked(self):
        if not self.is_capturing:
            self.begin_capture()
        else:
            self.end_capture()

    def begin_capture(self):
        self.capture_directory = QFileDialog.getExistingDirectory()
        if not self.capture_directory:
            return
        self.radar_client.end_capture()
        self.radar_client.begin_capture(self.capture_directory + '/raw-adc.bin') # TODO: use / in all paths
        self.is_capturing = True
        self.capture_btn.setText('End Capture')

    def end_capture(self):
        self.close()
