import cv2
import numpy
import pyqtgraph as pg
from matplotlib import cm
from operator import add
import time

'''
In: processed radar data
Out:
    to file: raw video, composite rdm + synced video frames
    to queue: all visual data, processed and ready for gui display
'''


# def output_writer(data_queue, visual_queue):
def output_writer():
    video_res = (1280, 720,) #,
    rdm_res = (480, 480,)
    composite_res = tuple(map(add,video_res, rdm_res))

    # video_in = cv2.VideoCapture(0)
    raw_video_out = cv2.VideoWriter('raw-video.avi', cv2.VideoWriter_fourcc(*'XVID'), 1.0, video_res)
    # rdm_composite_out = cv2.VideoWriter('rdm-composite.avi', cv2.VideoWriter_fourcc(*'XVID'), 20.0, composite_res)


    # while True:  # video_stream_out.isOpen():
    for i in range(200):

        # latest_data = rdata(data_queue.get())
        # ret, frame = video_in.read()

        # if ret:
        #     # process video frame
        #     frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        #     frame = cv2.resize(frame, video_res)
        #     frame = cv2.transpose(frame)
        #     frame = cv2.flip(frame, 1)
        # else:
            # write blank image to video out
            # frame = numpy.zeros(video_res + (3,), numpy.uint8)
        frame = numpy.zeros((720, 1280, 3), numpy.uint8)
        # print(frame.shape)
        # write video out
        raw_video_out.write(frame)

        time.sleep(1/20.0)

        # write composite out
        # plot = pg.ImageItem()
        # colormap = cm.get_cmap("jet")
        # colormap._init()
        # lut = (colormap._lut * 255).view(numpy.ndarray)  # Convert matplotlib colormap from 0-1 to 0 -255 for Qt
        # plot.setLookupTable(lut)  # Set the colour map

        # plot.setImage(lastest_data.rdm, levels=[-50,30])
        # plot.getPixmap()

        # exporter = pg.exporters.ImageExporter(plot)
        # exporter.export('')

        

        # package data for gui
        
        # if ret:
        #     video_stream_out.write(frame)
    
    # video_stream_in.release()
    # video_stream_out.release()

    # video_in.release()
    raw_video_out.release()

    return

# TODO: replace with something better / update processData.py to match
class rdata:
    def __init__(self, init):
        self.range_axis = init[0]
        self.v_axis_range = init[1]
        self.angle_axis_range = init[2]
        self.angle_fft = init[3]
        self.range_power = init[4]
        self.rdm = numpy.transpose(init[5])
        self.cartesian_target_coords = init[6]
        self.rda = numpy.transpose(init[7])

if __name__ == '__main__':
    output_writer()