


# ----------------------------------------------------------------------------------------------------------------------
# Used in old pipeline - entire scene would load in, be cfar'd, then the start and end frames would be found before
#                        meanshift/camshift was applied from which samples were exported
# ----------------------------------------------------------------------------------------------------------------------
def find_start_end_frames(scene, peaks, threshold_val=0.0):
    # iterates from start, or backwards from end, to find the first/last frames for which
    # the highest peak passes a threshold
    start_frame, start_peak = -1, (0,0)
    end_frame = -1

    def threshold(i, val):
        if not len(peaks[i]):
            return None
        max_idx = np.argmax([scene[i, x, y] for x,y in peaks[i]])  # index of largest peak value
        if scene[(i, *peaks[i][max_idx])] >= val:  # threshold the peak value
            return peaks[i][max_idx]  # return the peak coordinates if above threshold
        return None  # fail state is None

    # first frame above threshold
    for i in range(len(peaks)):
        start_peak = threshold(i, threshold_val)
        if start_peak:
            start_frame = i
            break

    # final frame above threshold
    for i in range(len(peaks) - 1, -1, -1):
        if threshold(i, threshold_val):
            end_frame = i
            break

    return start_frame, start_peak, end_frame




# ----------------------------------------------------------------------------------------------------------------------
# Looking at exponential decay of power return over time - fits a * e^(-bx) + c
# Was to be used for selecting start/end frame by thresholding vs the exponential fit line
# ----------------------------------------------------------------------------------------------------------------------

# from scipy.stats import describe, linregress
# from scipy.optimize import curve_fit

def find_start_end_2(scene, threshold, peaks):

    max_peaks = [
        peaks[i][
            np.argmax(
                [scene[i, co[0], co[1]] for co in peaks[i]]
            )
        ] if len(peaks[i]) else (-1,-1) for i in range(len(peaks))
    ]

    # TODO: use smoothing filter on x and y

    # max_co = peaks[s][np.argmax([scene[(s, co[0], co[1])] for co in peaks[s]])]

    x0 = [co_list[0][0] if len(co_list) else 0 for co_list in peaks]
    y0 = [co_list[0][1] if len(co_list) else 0 for co_list in peaks]

    x1 = [co[0] if co[0] > -1 else max_peaks[i-1][0] for i, co in enumerate(max_peaks)]
    y1 = [co[1] if co[1] > -1 else max_peaks[i-1][1] for i, co in enumerate(max_peaks)]

    plt.plot(x1)
    plt.plot(y1)
    plt.show()
    input('...')

    raise ValueError(':(')

    # print('\n'.join([str(i) + ', ' + str(np.percentile(f, [90, 99, 100])) for i, f in enumerate(scene)]))
    # a = np.array([np.percentile(frame, 100) for frame in scene])
    # print(a.mean(), describe(a, axis=None))
    # print(np.percentile(a, [0, 25, 50, 75, 100]))

    # print(describe(scene[0], axis=None))
    # print(np.percentile(scene[0], [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]))
    # print(np.percentile(scene[0], [99.9, 99.91, 99.92, 99.93, 99.94, 99.95, 99.96, 99.97, 99.98, 99.99, 100]))



    # b = np.array([np.max(frame) for frame in scene])

    # slope, intercept, r_value, p_value, std_err = linregress(range(len(scene)), b)

    # def func(x, a, b, c):
    #     return a * np.exp(-b * x) + c

    # popt, pcov = curve_fit(func, range(len(scene)),  b)

    # print(slope, intercept, r_value, p_value, std_err)
    # print(popt, pcov)

    # y0 = [(slope * x) + intercept for x in range(len(scene))]

    # plt.plot(b)
    # plt.plot(y0)
    # plt.plot(func(range(len(scene)), *popt), '-')
    # plt.show()
    # input('...')


    for s in range(len(scene)):
        if (scene[s] >= threshold).any():
            break

    for e in range(len(scene) - 1, -1, -1):
        if (scene[e] >= threshold).any():
            break

    return s, e

