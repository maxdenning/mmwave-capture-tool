import multiprocessing as mp
import os
import plots
import sys
from pyqtgraph.Qt import QtCore, QtGui
from simple_radar_client import simple_radar_client
from common import start_adc, start_raw_data_processing
from thread_flag import create_safe_flag


if __name__ == '__main__':

    '''
    enter app, begins demo mode -- showing video and plots, not saving any data, adc is put in generic dump
        begin capture
            puts data in given path
        end capture
            stops capture
            exits app

        exit app
            exits app
        

        threads
            udp
                end
            data
                end
            gui
                switch
                end
            message
                end
    '''

    raw_dump_directory = 'C:\\\\dump'
    raw_dump_file = raw_dump_directory + '\\\\adc_bin.bin'
    # assert(os.path.isdir(raw_dump_directory) and os.path.isabs(raw_dump_directory), 
    #        'Dump directory "' + raw_dump_directory + '" is not valid')

    # Init thread-safe queues moving data between udp, image analysis, and plotting processes
    raw_data_queue = mp.Queue()  # raw data queue for processing
    data_queue = mp.Queue()  # processed radar data for display

    # Start message sender process, and trigger radar
    radar_client = simple_radar_client()  # handles messages to be sent to the lua environment
    radar_client.begin_capture(raw_dump_file)

    # Start listening on the UDP port.
    join_flag = create_safe_flag()
    udp_process = start_adc(join_flag, raw_data_queue)

    # Start processing any data that is available.
    #raw_data_process = start_raw_data_processing(join_flag, raw_data_queue, data_queue)


    # Setup real time plotting.
    app = QtGui.QApplication(sys.argv)
    thisapp = plots.App(data_queue, radar_client)
    thisapp.show()

    # Wait for all processes to rejoin.
    #udp_process.join()
    #raw_data_process.join()

    sys.exit(app.exec_())
