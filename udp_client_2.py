import matplotlib.pyplot as plt
plt.ion()  # Note this correction
import processData
import socket
import types
from thread_flag import read_flag


'''
raw adc data stream handler
converts raw adc data streams into RDM frames
input: stream       output: frame queue
must accept input from both socket during live demo
    and file stream during post-fact conversion
'''

# class FileAsSocketWrapper:
#     def __init__(file_object):
#         self.file_object
    
#     def recv(bytes):
#         return self.file_object.read(bytes)


def run_adc_udp_client(jflag, server_address, adc_frame_queue):
    udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    udp_socket.bind(server_address)
    udp_socket.settimeout(10.0)

    # stop_flag = mp.Value(ctypes.c_bool, False) # TODO: move this


    # NOTE: may require wrapping of socket object
    try:
        data_stream_to_adc_frames(jflag, udp_socket, adc_frame_queue)
    finally:
        print('closed socket')
        udp_socket.close()


def file_as_socket_recv(file, rbytes):
    return file.read(rbytes)


def run_adc_file_client(jflag, path, adc_frame_queue):
    with open(path, 'rb') as file:
        file.recv = types.MethodType(file_as_socket_recv, file)
        data_stream_to_adc_frames(jflag, file, adc_frame_queue)


def data_stream_to_adc_frames(jflag, input_stream, adc_frame_queue):
    # NOTE: input_stream should be non-blocking, but with a long timeout (10s)
    HEADER_SZ = 10
    RECV_SZ = 4096

    print ('waiting to receive...')
    for _ in range(500):
        data = input_stream.recv(RECV_SZ)
    print('receiving')

    prev_seq_num, byte_count, adc_data_sz = read_packet_header(data)
    prev_frame_id = int((byte_count + adc_data_sz) / processData.FRAME_SIZE_BYTES)  # id of the last COMPLETE(?) frame
    # next_frame = bytearray()
    # next_frame.extend(data[HEADER_SZ:])
    frame = bytearray()
    frame.extend(data[HEADER_SZ:])

    # while not read_flag(stop_flag):
    #     data = input_stream.recv(RECV_SZ)  # NOTE: need to make socket with timeout?
    #     frame, next_frame, prev_frame_id, prev_seq_num = build_frame(data, next_frame, prev_frame_id, prev_seq_num)

    #     # print('LEN C, N: [' + str(processData.FRAME_SIZE_BYTES) + '] ' + str(len(frame)) + ', ' + str(len(next_frame)))

    #     if frame:
    #         # print('\n\n~~~~~~~~~~~~~~~~~~  got frame  ~~~~~~~~~~~~~~~~~~\n\n')
    #         if adc_frame_queue.qsize() <= 1:  # NOTE: look at this
    #             adc_frame_queue.put(frame)

    while not read_flag(jflag):
        data = input_stream.recv(RECV_SZ)

        seq_num, byte_count, adc_data_sz = read_packet_header(data)
        frame_id = int((byte_count + adc_data_sz) / processData.FRAME_SIZE_BYTES)

        seq_id_flag = (seq_num - 1 == prev_seq_num)  # flags if sequence is in order
        # DEBUG_prev_seq_num = prev_seq_num  # FIXME
        prev_seq_num = seq_num

        #           y sk seq    n sk seq
        # y fr tr    gt end       splt data
        # n fr tr    gt rid       take all
        #
        #

        frame_id_flag = prev_frame_id < frame_id  # true if transition

        # if seq_id_flag:            # no skip
        #     if not frame_id_flag:     # no transition
        #         # take all
        #         pass
        #     else:                     # ys transition
        #         # split
        #         pass
        # else:                      # ys skip
        #     print('ndrop [%i %i] [%i %i]' % (DEBUG_prev_seq_num, seq_num, prev_frame_id, frame_id))
        #     if not frame_id_flag:     # no transition
        #         # drop
        #         pass
        #     else:                     # ys transition
        #         # get end
        #         pass



        if prev_frame_id >= frame_id:  # no frame transition
            frame.extend(data[HEADER_SZ:])  # just add all of this packet
            continue  # do not step frame_id forward
        
        # there is a frame transition
        # DEBUG_prev_frame_id = prev_frame_id  # FIXME
        prev_frame_id = frame_id  # step frame_id forward
        frame_end_idx = frame_id * processData.FRAME_SIZE_BYTES - byte_count  # end of frame within current data

        if not seq_id_flag:  # out of order, drop frame NOTE: this is bad
            frame = bytearray()
            frame.extend(data[HEADER_SZ + frame_end_idx:])
            # print('ndrop [%i %i] [%i %i]' % (DEBUG_prev_seq_num, seq_num, DEBUG_prev_frame_id, frame_id))
            continue
        
        frame.extend(data[HEADER_SZ:frame_end_idx + HEADER_SZ])


        if len(frame) == processData.FRAME_SIZE_BYTES:
            adc_frame_queue.put(frame)
        #     print('ngot [%i %i] [%i %i]' % (DEBUG_prev_seq_num, seq_num, DEBUG_prev_frame_id, frame_id))
        # else:
            # print('ndrop [%i %i] [%i %i] [%i %i]' % (DEBUG_prev_seq_num, seq_num, DEBUG_prev_frame_id, frame_id, len(frame), processData.FRAME_SIZE_BYTES))

        
        frame = bytearray()
        frame.extend(data[HEADER_SZ + frame_end_idx:])







def read_packet_header(data):
    seq_num = int.from_bytes(data[0:4], byteorder='little')
    byte_count = int.from_bytes(data[4:10], byteorder='little')  # This lags by at least set of data transmitted.
    adc_data_size = len(data[10:])
    return seq_num, byte_count, adc_data_size


def build_frame(data, frame, prev_frame_id, prev_seq_num):
    HEADER_SZ = 10

    seq_num, byte_count, adc_data_sz = read_packet_header(data)
    frame_id = int((byte_count + adc_data_sz) / processData.FRAME_SIZE_BYTES)
    seq_id_flag = (seq_num - 1 == prev_seq_num)  # flags if sequence is in order


    # print('SEQ P, S: ' + str(prev_seq_num) + ', ' + str(seq_num) + ', ' + str(seq_id_flag))
    # print('FRM P, S: ' + str(prev_frame_id) + ', ' + str(frame_id))


    if prev_frame_id >= frame_id:  # no frame transition
        frame.extend(data[HEADER_SZ:])  # just add all of this packet
        return bytearray(), frame, prev_frame_id, seq_num  # do not step frame_id forward

    # there is a frame transition
    
    frame_end_idx = frame_id * processData.FRAME_SIZE_BYTES - byte_count  # end of frame within current packet

    next_frame = bytearray()
    next_frame.extend(data[HEADER_SZ + frame_end_idx:])  # build start of next frame

    if not seq_id_flag:
        print('ndrop [%i %i] [%i %i]' % (prev_seq_num, seq_num, prev_frame_id, frame_id))
        return bytearray(), next_frame, frame_id, seq_num  # out of order, drop frame NOTE: this is bad

    # not out of order

    frame.extend(data[HEADER_SZ:frame_end_idx + HEADER_SZ])

    print('ngot [%i %i] [%i %i] [%i %i]' % (prev_seq_num, seq_num, prev_frame_id, frame_id, len(frame), processData.FRAME_SIZE_BYTES))

    if len(frame) != processData.FRAME_SIZE_BYTES:
        # print('BAD LEN: ' + str(len(frame)))
        frame = bytearray()
    
    return frame, next_frame, frame_id, seq_num
