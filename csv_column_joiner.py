import csv
import os
import re
import sys
from functools import partial


def sum_lists(*lists):
    acc = []
    for l in lists:
        acc.extend(l)
    return acc


def join_files_like(match_str, input_filepaths, output_filepath):
    r = re.compile('.*' + match_str + '.*')
    matching_paths = list(filter(r.match, input_filepaths))
    ifiles = [open(fp, 'r') for fp in matching_paths]

    with open(output_filepath, 'w') as ofile:
        writer = csv.writer(ofile)
        for row in zip(*map(csv.reader, ifiles)):
            writer.writerow(sum_lists(*row))

    for fo in ifiles:
        fo.close()  # close files


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print(f'usage: {sys.argv[0]} <input dir> <output dir>')
        exit(1)

    input_dir = sys.argv[1]
    output_dir = sys.argv[2]

    input_filenames = os.listdir(input_dir)
    input_filepaths = list(map(lambda fn: os.path.join(input_dir, fn), input_filenames))
    # print(input_filepaths)

    join_files_like('-loss', input_filepaths, os.path.join(output_dir, 'loss.csv'))
    join_files_like('-acc', input_filepaths, os.path.join(output_dir, 'acc.csv'))
    join_files_like('-val_loss', input_filepaths, os.path.join(output_dir, 'val_loss.csv'))
    join_files_like('-val_acc', input_filepaths, os.path.join(output_dir, 'val_acc.csv'))
