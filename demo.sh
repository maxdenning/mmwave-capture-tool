#!/bin/bash

printf "\n----------------\nFrame Processing: Raw ADC radar data --> RDM frames (N x 512 x 128)\n"
printf "python mmwave-pp-fft.py data/demo/ data/demo/slices/rp.npy data/demo/slices/ram.npy data/demo/slices/rdm.npy\n----------------\n"
python mmwave-pp-fft.py data/demo/ data/demo/slices/rp.npy data/demo/slices/ram.npy data/demo/slices/rdm.npy

printf "\n\n----------------\nTarget Extraction: RDM frames --> Target Samples (N x 50 x 32 x 32)\n"
printf "python mmwave-pp-extractor.py data/demo/slices/rdm.npy data/demo/samples/camshift/rdm data/demo/data-demo.json PEDESTRIAN 1.0.a.i TRAINING fcs\n----------------\n"
python mmwave-pp-extractor.py data/demo/slices/rdm.npy data/demo/samples/camshift/rdm data/demo/data-demo.json PEDESTRIAN 1.0.a.i TRAINING fcs

printf "\n\n----------------\nPrediction: Target Sample --> Class label\n"
printf "python mmwave-predictor.py etl results/classification/EncoderTransferLearning/example_weights.h5 data/demo/samples/camshift/rdm/0.npy\n----------------\n"
python mmwave-predictor.py etl results/classification/EncoderTransferLearning/example_weights.h5 data/demo/samples/camshift/rdm/0.npy

