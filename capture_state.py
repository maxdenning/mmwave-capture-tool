
class capture_state:
    in_capture_mode = False

    @classmethod
    def switch(cls):
        mutex(obj)
        in_capture_mode = !in_capture_mode
        mutex(obj)
