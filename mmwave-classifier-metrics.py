import matplotlib.pyplot as plt
import numpy as np
import os
import pickle
import sys
from mmwave.classifier import evaluation
from mmwave.metadata import SampleClassification
from scipy.stats import describe
from sklearn import metrics


def load_results(input_dir, match_str):
    # find results files
    result_filepaths = []
    for root, subdirs, files in os.walk(input_dir):
        result_filepaths.extend([os.path.join(root, f) for f in files if match_str in f])
    # load in numpy files
    results = []
    for fpath in result_filepaths:
        results.append(np.load(fpath))
    return results


def calculate_scores(results):
    scores = {}
    scores['model_recall'], scores['model_precision'], scores['model_f1_score'] = [], [], []
    scores['class_recall'], scores['class_precision'], scores['class_f1_score'] = {c.name: [] for c in SampleClassification}, {c.name: [] for c in SampleClassification}, {c.name: [] for c in SampleClassification}

    acc_true, acc_pred, acc_pred_prob = np.array([]), np.array([]), np.array([[0]* len(SampleClassification)]) #np.argmax(results[0][0], axis=1), np.argmax(results[0][1], axis=1), results[0][1]
    for r in results:
        true, pred = np.argmax(r[0], axis=1), np.argmax(r[1], axis=1)
        acc_true = np.concatenate((acc_true, true))
        # acc_pred = np.concatenate((acc_pred, pred))
        acc_pred_prob = np.concatenate((acc_pred_prob, r[1]))

        scores['model_recall'].append(metrics.recall_score(true, pred, average='macro'))
        scores['model_precision'].append(metrics.precision_score(true, pred, average='macro'))
        scores['model_f1_score'].append(metrics.f1_score(true, pred, average='macro'))

        confusion = metrics.confusion_matrix(true, pred)
        for c in SampleClassification:
            ct = evaluation.confusion_table(confusion, c.value)
            scores['class_recall'][c.name].append(evaluation.recall(ct))
            scores['class_precision'][c.name].append(evaluation.precision(ct))
            scores['class_f1_score'][c.name].append(evaluation.f1_score(ct))

    fpr, fpr_average, tpr, tpr_average, auc, auc_average = evaluation.multiclass_auc_roc(acc_true, acc_pred_prob[1:])
    fig = evaluation.graph_multiclass_auc_roc(fpr, fpr_average, tpr, tpr_average, [c.name for c in SampleClassification])

    # scores['recall'] = metrics.recall_score(acc_true, acc_pred, average='macro')
    # scores['precision'] = metrics.precision_score(acc_true, acc_pred, average='macro')
    # scores['f1_score'] = metrics.f1_score(acc_true, acc_pred, average='macro')
    scores['model_recall_avg'] = sum(scores['model_recall']) / len(scores['model_recall'])
    scores['model_precision_avg'] = sum(scores['model_precision']) / len(scores['model_precision'])
    scores['model_f1_score_avg'] = sum(scores['model_f1_score']) / len(scores['model_f1_score'])
    scores['model_recall_stddev'] = np.std(scores['model_recall'])
    scores['model_precision_stddev'] = np.std(scores['model_precision'])
    scores['model_f1_score_stddev'] = np.std(scores['model_f1_score'])


    scores['class_recall_avg'], scores['class_precision_avg'], scores['class_f1_score_avg'] = {}, {}, {}
    scores['class_recall_stddev'], scores['class_precision_stddev'], scores['class_f1_score_stddev'] = {}, {}, {}
    for c in SampleClassification:
        scores['class_recall_avg'][c.name] = sum(scores['class_recall'][c.name]) / len(scores['class_recall'][c.name])
        scores['class_precision_avg'][c.name] = sum(scores['class_precision'][c.name]) / len(scores['class_precision'][c.name])
        scores['class_f1_score_avg'][c.name] = sum(scores['class_f1_score'][c.name]) / len(scores['class_f1_score'][c.name])
        scores['class_recall_stddev'][c.name] = np.std(scores['class_recall'][c.name])
        scores['class_precision_stddev'][c.name] = np.std(scores['class_precision'][c.name])
        scores['class_f1_score_stddev'][c.name] = np.std(scores['class_f1_score'][c.name])

    scores['fpr'] = {'average': fpr_average}
    scores['tpr'] = {'average': tpr_average}
    scores['auc'] = {'average': auc_average}
    for i in range(len(auc)):
        scores['fpr'][f'{SampleClassification(i).name.lower()}'] = fpr[i]
        scores['tpr'][f'{SampleClassification(i).name.lower()}'] = tpr[i]
        scores['auc'][f'{SampleClassification(i).name.lower()}'] = auc[i]

    return scores, fig


if __name__ == '__main__':
    if len(sys.argv) < 4:
        print(f'usage: {sys.argv[0]} <input dir> <output dir> <graph title>')
        exit(1)
    input_dir = sys.argv[1]
    output_dir = sys.argv[2]
    fig_title = sys.argv[3]
    text_output_filepath = os.path.join(output_dir, 'scores.txt')
    scores_output_filepath = os.path.join(output_dir, 'scores_dump.p')
    graph_output_filepath = os.path.join(output_dir, 'roc.png')

    results = load_results(input_dir, 'results.npy')

    scores, graph = calculate_scores(results)

    with open(text_output_filepath, 'w') as ofile:
        ofile.write('\n'.join(f'{k}: {v},' for k,v in scores.items()))

    with open(scores_output_filepath, 'wb') as ofile:
        pickle.dump(scores, ofile)

    graph.suptitle(f'{fig_title}, Receiver Operating Characteristic (ROC)')
    graph.savefig(graph_output_filepath)
