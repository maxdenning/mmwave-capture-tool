
MSMQ must be enabled.

Requires `numpy+mkl 1.15.1` installed, download `numpy-1.15.1+mkl-cp37-cp37m-win_amd64.whl` [here](https://www.lfd.uci.edu/~gohlke/pythonlibs/#numpy)
`pywin32` requires post install to be run
    `python Scripts/pywin32_postinstall.py -install`
