import numpy as np
import os
import sys
import time
from mmwave.radar import RadarConfig
from mmwave.grapher.simple_grapher_app import SimpleGrapherGuiApp, launch_qt_app
from mmwave.grapher.utils import range_axis
from pyqtgraph.Qt import QtCore, QtGui


class RangeFFTGrapherApp(SimpleGrapherGuiApp):
    def __init__(self, data_paths, config_paths):
        super().__init__()
        self.rps = list(map(np.load, data_paths))
        self.raxis = [range_axis(RadarConfig(p)) for p in config_paths]
        self.idx = [0] * len(self.rps)

        # Create all RDM plots
        self.viewboxs = []
        self.plots = []
        for i in range(len(self.rps)):
            self.viewboxs.append(self.canvas.addPlot(title='Range FFT', labels={'bottom': 'range', 'left': 'power'}))
            # self.viewboxs[i].setXRange(0, 10, padding=0)
            self.viewboxs[i].setYRange(-60, 10, padding=0)
            self.plots.append(self.viewboxs[i].plot(pen='y'))

            if i != len(self.rps):
                self.canvas.nextRow()

        # Start
        self.start_updating(25)

    def update(self):
        for i in range(len(self.plots)):
            if self.idx[i] >= len(self.rps[i]):
                continue
            self.plots[i].setData(self.raxis[i], self.rps[i][self.idx[i]])
            self.idx[i] += 1

        # check if all plots are finished 
        if all([self.idx[i] >= len(self.rps[i]) for i in range(len(self.rps))]):
            self.closeEvent(None)



if __name__ == '__main__':
    if len(sys.argv) <= 1:
        print('useage: {} <input data> <radar config>'.format(sys.argv[0]))
        exit(1)

    input_filepaths = sys.argv[1:]
    input_data = input_filepaths[0:][::2]  # select even
    input_conf = [fp if fp != '.' else 'config/config.mmwave.json' for fp in input_filepaths[1::2]]  # if '.' use default

    if not all(map(os.path.exists, input_data + input_conf)) or not all(map(os.path.isfile, input_data + input_conf)):
        raise ValueError(f'input file does not exist, or is not a file: {input_filepaths}')

    launch_qt_app(RangeFFTGrapherApp, input_data, input_conf)
