import multiprocessing as mp
# import processData
import udpClient
# import udp_client_2
from thread_flag import read_flag

from mmwave.postprocessing.postprocessing import range_doppler_ft

def start_adc_listener(jflag, adc_frame_queue):
    """Start the udp client and wait for data."""
    
    adc_listener_proc = mp.Process(target=udpClient.runUDPClient, args=(jflag, adc_frame_queue,))

    # adc_listener_proc = mp.Process(
    #     target=udp_client_2.run_adc_udp_client,
    #     args=(jflag, ('192.168.33.30', 4098), adc_frame_queue))
    
    adc_listener_proc.start()
    return adc_listener_proc


def start_adc_frame_processing(jflag, radar_config, adc_frame_queue, processed_frame_queue):
    """Start processing raw adc frames."""
    adc_frame_processor_proc = mp.Process(target=run_adc_frame_processor, args=(jflag, radar_config, adc_frame_queue, processed_frame_queue,))
    adc_frame_processor_proc.start()
    return adc_frame_processor_proc


# def run_adc_frame_processor(jflag, adc_frame_queue, processed_frame_queue):
#     while not read_flag(jflag):
#         try:
#             next_frame_data = adc_frame_queue.get()
#             processData.processFrame(next_frame_data, processed_frame_queue)
#         except Exception as e:
#             print('err', e, type(e))
#             break
#     return

def run_adc_frame_processor(jflag, radar_config, adc_frame_queue, processed_frame_queue): 
    while not read_flag(jflag):
        try:
            next_frame_data = adc_frame_queue.get(timeout=1)
            processed_frame_queue.put(range_doppler_ft(next_frame_data, radar_config))
        except Exception as e:
            print('err: run_adc_frame_processor', e, type(e))
            break
    print('closed frame processor')
    return
