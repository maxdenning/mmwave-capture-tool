import numpy as np
import os
import pyqtgraph as pg
import time
import sys
from pyqtgraph import AxisItem
from matplotlib import cm
from mmwave.grapher.simple_grapher_app import SimpleGrapherGuiApp, launch_qt_app
from mmwave.grapher.utils import range_axis, velocity_axis
from mmwave.postprocessing import postprocessing, utils
from mmwave.radar import RadarConfig


class RDMGrapherApp(SimpleGrapherGuiApp):
    def __init__(self, data_filepaths, conf_filepaths, playback_multiplier, concat=False):
        super().__init__()
        
        self.rdms = list(map(np.load, data_filepaths))
        if concat:
            self.rdms = [np.concatenate(self.rdms, axis=0)]
        self.confs = list(map(RadarConfig, conf_filepaths))
        self.idx = [0] * len(self.rdms)
        self.minmax = [(utils.POSTPROC_dB_BASELINE, 16)] * len(self.rdms) # [(r.min(), r.max()) for r in self.rdms]
        
        print('RDM Sizes:', [(self.rdms[i].shape, f'{self.minmax[i][0]:.2f}', f'{self.minmax[i][1]:.2f}') for i in range(len(self.rdms))])

        # Create colormap for RDM
        colormap = cm.get_cmap("viridis")
        colormap._init()
        lut = (colormap._lut * 255).view(np.ndarray)  # Convert matplotlib colormap from 0-1 to 0 -255 for Qt

        # Create all RDM plots
        self.viewboxs = []
        self.plots = []
        for i in range(len(self.rdms)):

            xrange = range_axis(self.confs[i])
            yrange = velocity_axis(self.confs[i])

            xaxis = AxisItem('bottom')
            xaxis.setLabel(text='Range', units='m')
            xaxis.setScale((xrange[-1] - xrange[0]) / self.rdms[i].shape[1])

            yaxis = AxisItem('left')
            yaxis.setLabel(text='Velocity', units='ms^-1')
            yaxis.setScale((yrange[-1] - yrange[0]) / self.rdms[i].shape[2])

            self.viewboxs.append(self.canvas.addPlot(axisItems={'bottom': xaxis, 'left': yaxis}))
            # self.viewboxs.append(self.canvas.addViewBox())
            # self.viewboxs[i].setAspectLocked(False)
            # self.viewboxs[i].setRange(QtCore.QRectF(0,0, 300, 300))
            self.plots.append(pg.ImageItem(border='w'))
            self.viewboxs[i].addItem(self.plots[i])
            self.plots[i].setLookupTable(lut)

            if i != len(self.rdms):
                self.canvas.nextRow()

        # Start
        self.maxfps = (1 / self.confs[0].frame_period) * playback_multiplier
        self.start_updating(self.maxfps)

    def update(self):
        for i in range(len(self.plots)):
            if self.idx[i] >= len(self.rdms[i]):
                continue
            self.viewboxs[i].setTitle('Frame: {} ({:.2f} sec)'.format(self.idx[i], self.idx[i]/self.maxfps))
            self.plots[i].setImage(self.rdms[i][self.idx[i]], levels=self.minmax[i])
            self.idx[i] += 1

        # check if all plots are finished
        if all([self.idx[i] >= len(self.rdms[i]) for i in range(len(self.rdms))]):
            self.closeEvent(None)


if __name__ == '__main__':
    if len(sys.argv) <= 3:
        print(f'usage: {sys.argv[0]} <playback multiplier> <input 0> <config 0> ... <input N> <config N>')
        exit(1)

    playback_multiplier = float(sys.argv[1])
    input_filepaths = sys.argv[2:]
    data_filepaths = input_filepaths[0::2]
    conf_filepaths = [fp if fp != '.' else 'config/config.mmwave.json' for fp in input_filepaths[1::2]]  # if '.' use default

    concat_samples_mode = False  # if true, concat all rdm data into a single scene
    if len(data_filepaths) == 1 and len(conf_filepaths) == 1 and os.path.isdir(data_filepaths[0]):
        concat_samples_mode = True
        # get list of data files in directory, and order them by their number
        data_filepaths = list(
            filter(lambda f: os.path.isfile(f) and os.path.splitext(f)[1] == '.npy',
            map(lambda f: os.path.join(data_filepaths[0], f), os.listdir(data_filepaths[0]))))
        try:
            order = np.argsort([int(os.path.splitext(os.path.basename(p))[0]) for p in data_filepaths])
            data_filepaths = [data_filepaths[idx] for idx in order]
        except:
            print('Reading files unordered')
            print('\n'.join(data_filepaths))
    if not all(map(os.path.exists, data_filepaths + conf_filepaths)) or not all(map(os.path.isfile, data_filepaths + conf_filepaths)):
        raise ValueError(f'input file does not exist, or is not a file: {input_filepaths}')

    launch_qt_app(RDMGrapherApp, data_filepaths, conf_filepaths, playback_multiplier, concat_samples_mode)
