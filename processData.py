import numpy as np
import struct
# from scipy import ndimage

speedLight = 3e8
freq = 77.5e9
wavelength = speedLight/freq
framePeriod = 2*50e-3
vres = wavelength/(2*framePeriod)
samplingRate = 5000e3
slopeHzPerSec = 29.982e12
nADCSamples = 256
nChirps = 128 # Chirp loops.
nAngBins = 16
nRx = 4
nTx = 1
complexFactor = 2
bit16ToBytes = 2
# FRAME_SIZE_BYTES = nADCSamples*complexFactor*nRx*bit16ToBytes*nChirps
FRAME_SIZE_BYTES = 304*complexFactor*nRx*bit16ToBytes*128

numbADCBits = 16
normFactor = np.sqrt(2)/(2**(numbADCBits-1)-1)

# lastFrame = np.zeros(nADCSamples)


# def cfar(rdmap):
#     dataForCFAR = np.copy(rdmap)
#     # dataForCFAR[:, 64:66] = -100 # Zero off zero doppler.
#     rankedRDMOutput = ndimage.rank_filter(dataForCFAR, rank=15, size=5)
#     betaOS = 10
#     targetIds = np.transpose(np.where(dataForCFAR > (betaOS + rankedRDMOutput)))

#     return targetIds


def createCube(dataForFFT, nADCSamples, nTx, nRx, nChirps):

    global normFactor

    # Always assume 4 TX active.
    # 4 RX and 1 TX case.
    if nTx == 1:

        # Create holders of data for first chirp and initialise to zero.
        cCube = np.zeros((nADCSamples, nTx * nRx, nChirps), dtype=np.complex)

        # Table 22.8 of mmwave_studio_user_guide.pdf implies following indices
        # for real (R) and complex (C) data.
        step = 4
        indicesR1 = np.arange(0, nADCSamples * 2, step)
        indicesR2 = np.arange(1, nADCSamples * 2, step)
        indicesC1 = np.arange(2, nADCSamples * 2, step)
        indicesC2 = np.arange(3, nADCSamples * 2, step)

        # Form the complex data array.
        cCube[0:nADCSamples:2, :, :] = dataForFFT[indicesR1, :, :] + 1j * dataForFFT[indicesC1, :, :]
        cCube[1:nADCSamples:2, :, :] = dataForFFT[indicesR2, :, :] + 1j * dataForFFT[indicesC2, :, :]

        # Normalise the data.
        cCube = normFactor * cCube

    # Case of 4 RX and 2 TX (TDM).
    elif nTx == 2:

        # Reorder chirps from TDM.
        # Even chirps corresponds to TX1, Odd chirps to TX2.
        rx1To4Idx = np.arange(0, nRx*nTx, nTx)
        rx5To8Idx = np.arange(1, nRx*nTx, nTx)

        ##    dataForFFT = np.reshape(dataForFrameUint16, (nADCSamples * complexFactor, nRx*nTx, -1), order="F")

        tdmReorder = np.zeros((nADCSamples*complexFactor, nRx*nTx, nChirps), dtype=np.complex)
        tdmReorder[:,0:4,:] = dataForFFT[:,rx1To4Idx,: ]
        tdmReorder[:, 4:8, :] = dataForFFT[:, rx5To8Idx, :]


        # Create holders of data for first chirp and initialise to zero.
        cCube = np.zeros((nADCSamples, nRx*nTx, nChirps), dtype=np.complex)

        # Table 22.8 of mmwave_studio_user_guide.pdf implies following indices
        # for real (R) and complex (C) chirp data.
        step = 4
        indicesR1 = np.arange(0, nADCSamples * 2, step)
        indicesR2 = np.arange(1, nADCSamples * 2, step)
        indicesC1 = np.arange(2, nADCSamples * 2, step)
        indicesC2 = np.arange(3, nADCSamples * 2, step)


        # Form the complex data array.
        cCube[0:nADCSamples:2, :, :] = tdmReorder[indicesR1, :, :] + 1j * tdmReorder[indicesC1, :, :]
        cCube[1:nADCSamples:2, :, :] = tdmReorder[indicesR2, :, :] + 1j * tdmReorder[indicesC2, :, :]

        # Normalise the data.
        cCube = normFactor * cCube

    return cCube


def processFrame(dataForFrame, dataQueue):

    # global lastFrame
    # Convert byte array to uint16 (2 bytes per uint16).
    count = int(len(dataForFrame) / 2)
    dataForFrameUint16 = struct.unpack('<' + 'h' * count, dataForFrame)

    # Reshape the data for frame so that the data it is in a processable format.
    dataForFFT = np.reshape(dataForFrameUint16, (nADCSamples * complexFactor, nRx*nTx, -1), order="F")

    # nChirps now corresponds to the number of chirp loops composed of TX1 and TX2 chirps.
    # Therefore number chirps per TX is nChirps/nTx.
    nChirpsInFrame = int(nChirps / nTx)

    cCube = createCube(dataForFFT, nADCSamples, nTx, nRx, nChirpsInFrame)

    # Now perform FFT for each chirp in frame.
    rangeAxis = (((np.arange(0, nADCSamples) / nADCSamples) * samplingRate) / slopeHzPerSec) * speedLight / 2
    vAxisRange = np.arange(-nChirpsInFrame/2,nChirpsInFrame/2)*vres
    angleAxisRange = np.arange(-nAngBins/2,nAngBins/2) * (180/nAngBins)  # np.arcsin(2*phase*(np.pi/180))

    windowR = np.hanning(nADCSamples)
    windowD = np.hanning(nChirpsInFrame)

    # Compute range FFT and average it.
    rangePower = np.zeros(nADCSamples)
    Y = np.zeros((nADCSamples, nRx*nTx), dtype=np.complex)
    rdAngle = np.zeros((nADCSamples, nAngBins))
    for rxIdx in range(0,nRx*nTx):
        for chirpIdx in range(0, nChirpsInFrame):
            y = cCube[:, rxIdx, chirpIdx] * windowR
            Y[:,rxIdx] = np.fft.fft(y)
            Yabs = (np.abs(Y[:,rxIdx]))
            rangePower = rangePower + Yabs

            '''
            if chirpIdx == 0:
                for rangeIdx in range(0, nADCSamples):
                    rdAngle[rangeIdx, :] = np.abs(np.fft.fft(Y[rangeIdx, :], nAngBins))
                    rdAngle[rangeIdx, :] = np.fft.fftshift(rdAngle[rangeIdx, :])

                rdAngle = 20 * np.log10(rdAngle)
            '''

    rangePower = rangePower / (nChirpsInFrame*rxIdx*nTx)
    rangePower = 20*np.log10(rangePower)

    # Form range doppler map.
    Y2 = np.zeros((nADCSamples, nChirpsInFrame))
    rdCube = np.zeros((nADCSamples,nChirpsInFrame,nRx*nTx), dtype=np.complex)
    for rxIdx in range(0, nRx * nTx):
        Y1 = (np.fft.fft(windowR[:,np.newaxis]*cCube[:,rxIdx,:],axis=0))
        rdCube[:,:,rxIdx] = np.fft.fftshift(np.fft.fft(windowD*Y1,axis=1),1)

        # Amplitude detection and non-coherent integration for RDMAP.
        Y2 = Y2 + np.abs(rdCube[:,:,rxIdx])

    # Output RDMAP as power (dB).
    logY2 =  20*np.log10(Y2/(nRx*nTx))

    # Perform target detection.
    targetIds = np.transpose(np.where(logY2 > 10))
    #targetIds = cfar(logY2)

    angFFT = np.zeros([len(targetIds),nAngBins])
    xTarg = np.zeros(len(targetIds))
    yTarg = np.zeros(len(targetIds))
    stationaryIdx = np.zeros(len(targetIds))
    # TODO: uncomment line below
    # print('Number of targets %d' % (len(targetIds)))

    if len(targetIds) > 0:
        angTarget = np.zeros(len(targetIds))

        targIdx = 0
        for targId in targetIds:

            # For now just do simple angle FFT.
            angFFT[targIdx,:] = np.abs(np.fft.fft(rdCube[targId[0], targId[1], :], nAngBins))
            angFFT[targIdx,:] = np.fft.fftshift(angFFT[targIdx,:])
            # Get the index with the highest magnitude (this will be what is chosen as the target).
            # If the radar angular resolution is good could possibly choose two or more peaks.
            angInd = np.argmax(20 * np.log10(angFFT[targIdx,:]))

            # Convert this index to a useful angle (think formula is correct).
            # np.arcsin(2*phase*(np.pi/180))
            angTarget[targIdx] = angleAxisRange[angInd]

            # Form spatial covariance matrix.
            #numbSnapshots = 1
            #R = rdCube[targId[0], targId[1], :] * np.transpose(rdCube[targId[0], targId[1], :])/1

            rangeInMetres = rangeAxis[targId[0]]

            #print('The phase is: %d and angle is %d' % (phase, angTarget[targIdx]))

            xTarg[targIdx] = rangeInMetres*np.sin(angTarget[targIdx]*np.pi/180)
            yTarg[targIdx] = rangeInMetres*np.cos(angTarget[targIdx] * np.pi / 180)

            # Add an indicator whether this is a stationary target or not.
            zeroDopplerArea = np.arange(nChirpsInFrame/2 - 2, nChirpsInFrame/2 + 2, 1)
            stationaryIdx = ((targetIds[:,1] == zeroDopplerArea[0]) |
                             (targetIds[:,1] == zeroDopplerArea[1]) |
                             (targetIds[:,1] == zeroDopplerArea[2]) |
                             (targetIds[:, 1] == zeroDopplerArea[3]))

            targIdx = targIdx + 1

    cartesianTargCoord = (xTarg, yTarg, stationaryIdx)

    # For each range sum results.
    #if dataQueue.qsize() < 1:
    # print('add', dataQueue.qsize())
    dataQueue.put((rangeAxis,vAxisRange, angleAxisRange, angFFT, rangePower, logY2, cartesianTargCoord,rdAngle))
    # print('.')

