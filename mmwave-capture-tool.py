import cv2
import datetime
import multiprocessing as mp
import numpy as np
import os
import pyqtgraph as pg
import sys
import time
from matplotlib import cm
from mmwave.grapher.utils import range_axis
from mmwave.postprocessing.utils import POSTPROC_dB_BASELINE
from mmwave.radar import RadarConfig, SimpleRadarClient
from process_starter import start_adc_listener, start_adc_frame_processing
# from PyQt5.QtGui import QInputDialog, QFileDialog
from pyqtgraph.Qt import QtCore, QtGui
from thread_flag import create_safe_flag, read_flag, set_flag


class MMWaveCaptureTool():
    """
    Has two modes: Preview and Capture
        Preview: shortcuts output from the radar to displays graphic and video output, captures nothing.
        Capture: disables preview graphics/local processing and captures data from the radar and video.
    Launches into Preview mode, and Capture mode is enabled/disabled later.
    """

    def __init__(self):
        super().__init__()
        self.ADC_DUMP_PATH = 'C:\\ti\\mmwave_studio_02_00_00_02\\mmWaveStudio\\PostProc\\adc_data.bin' #'C:\\dump\\adc_dump.bin'
        self.ADC_DATA_ROOT_DIR = 'E:\\radar-project\\data'
        self.ADC_DATA_FILENAME = 'adc.bin'
        self.VIDEO_DATA_FILENAME = 'scene_video.avi'
        self.RADAR_CONFIG_PATH = 'config\\config.mmwave.json'

        self.adc_frame_queue = mp.Queue()  # raw adc frame queue for processing
        self.processed_frame_queue = mp.Queue()  # processed radar data for display
        self.radar_client = SimpleRadarClient()  # handles messages/commands to be sent to the radar
        
        self.video_in = cv2.VideoCapture(0)  # 0 = integrated, 1 = usb
        self.video_out = None
        self.video_res = (320, 240) # (640, 480)
        self.video_fps = 8.0
        self.video_lastupdate = time.time()

        self.is_remote_processing = create_safe_flag(False)  # once true, all extra local processes join/terminate
        self.radar_client.begin_capture(self.ADC_DUMP_PATH)
        self.radar_config = RadarConfig(self.RADAR_CONFIG_PATH)
        # self.udp_process = start_adc_listener(self.is_remote_processing, self.adc_frame_queue)  # start listening on the UDP port
        # self.data_process = start_adc_frame_processing(self.is_remote_processing, self.radar_config, self.adc_frame_queue, self.processed_frame_queue)  # start processing any available raw data

    def end_preview_mode(self):
        if self.is_in_capture_mode():
            return  # preview already ended

        set_flag(self.is_remote_processing, True)
        time.sleep(2)
        # self.udp_process.terminate()
        # self.data_process.terminate()

    def begin_capture_mode(self):
        if self.is_in_capture_mode():
            return  # already in capture mode

        self.end_preview_mode()  # end local processing
        self.radar_client.end_capture()  # end old radar capture        
        self.radar_client.sleep(3000)

        # get new dir name
        datestr = datetime.datetime.now().strftime('%y-%m-%d')
        timestr = datetime.datetime.now().strftime('%H-%M')
        capture_dir = os.path.join(self.ADC_DATA_ROOT_DIR, datestr, timestr)
        os.makedirs(capture_dir, exist_ok=True)

        self.radar_client.begin_capture(os.path.join(capture_dir, self.ADC_DATA_FILENAME))
        time.sleep(6)  # attempt to sync the radar start with the video start

        # initialise video output
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        self.video_out = cv2.VideoWriter(os.path.join(capture_dir, self.VIDEO_DATA_FILENAME), fourcc, self.video_fps, self.video_res)

    def end_capture_mode(self):
        self.radar_client.end_capture()
        self.radar_client.close()
        self.video_in.release()
        if self.video_out is not None:
            self.video_out.release()

    def end(self):
        self.end_preview_mode()
        self.end_capture_mode()
    
    def is_in_capture_mode(self):
        return read_flag(self.is_remote_processing)

    def capture_video_frame(self):
        now = time.time()
        if max(now - self.video_lastupdate, 0.000000000001) < 1 / self.video_fps:
            return False, None

        ret, frame = self.video_in.read()
        if not ret:
            return False, None
        
        frame = cv2.resize(frame, self.video_res)
        self.video_lastupdate = now

        if self.is_in_capture_mode():
            self.video_out.write(frame)

        return ret, frame


class MMWaveCaptureToolGUI(QtGui.QMainWindow, MMWaveCaptureTool):
    def __init__(self):
        super(MMWaveCaptureToolGUI, self).__init__(parent=None)
        self._init_gui()

        # FPS helpers
        self.fps = 0.0
        self.fps_lastupdate = time.time()

        # Start
        self.updater = pg.QtCore.QTimer(self)
        self.updater.timeout.connect(self.update)
        self.updater.start(1000/max(16, self.video_fps))
    
    def _init_gui(self):
        # Create top-level gui container elements
        self.mainbox = QtGui.QWidget()
        self.setCentralWidget(self.mainbox)
        self.mainbox.setLayout(QtGui.QVBoxLayout())
        self.canvas = pg.GraphicsLayoutWidget()
        self.mainbox.layout().addWidget(self.canvas)

        # Label to display FPS
        self.fps_label = QtGui.QLabel()
        self.mainbox.layout().addWidget(self.fps_label)

        # Capture button
        self.capture_btn = QtGui.QPushButton('Begin capture')
        self.mainbox.layout().addWidget(self.capture_btn)
        self.capture_btn.clicked.connect(self.on_capture_btn_click)

        # Setup RDM plot within view box
        self.rdm_viewbox = self.canvas.addViewBox()
        self.rdm_viewbox.setAspectLocked(False)
        self.rdm_viewbox.setRange(QtCore.QRectF(0,0, 200, 300))
        self.rdm_plot = pg.ImageItem(border='w')
        self.rdm_viewbox.addItem(self.rdm_plot)

        # Set colormap for RDM
        colormap = cm.get_cmap("jet")
        colormap._init()
        lut = (colormap._lut * 255).view(np.ndarray)  # Convert matplotlib colormap from 0-1 to 0 -255 for Qt
        self.rdm_plot.setLookupTable(lut)  # Set the colour map

        # Camera display
        self.cam_viewbox = self.canvas.addViewBox()
        self.cam_viewbox.setAspectLocked(True)
        # self.cam_viewbox.setRange(QtCore.QRectF(0,0, 500, 500))
        self.cam_img = pg.ImageItem(border='w')
        self.cam_viewbox.addItem(self.cam_img)

        self.canvas.nextRow()

        #  Range FFT plot
        self.range_plot_container = self.canvas.addPlot(title='Range FFT', labels={'bottom': 'range', 'left': 'power'})
        self.range_plot_container.setXRange(0, 10, padding=0)
        self.range_plot_container.setYRange(-60, 10, padding=0)
        self.range_plot = self.range_plot_container.plot(pen='y')        

    def update(self):
        # Update FPS label
        self.fps_label.setText('Mean Frame Rate:  {fps:.3f} FPS'.format(fps=self.get_fps()))
        
        # Graphing (radar preview)
        # if not self.is_in_capture_mode():
        #     try:
        #         # TODO: refactor data queue items to class?
        #         range_power, rda, rdm = self.processed_frame_queue.get(timeout=0.2)
        #         self.rdm_plot.setImage(rdm, levels=[-POSTPROC_dB_BASELINE, 18])  # Set the range doppler map image
        #         self.range_plot.setData(range_axis(self.radar_config), range_power)  # Set the range FFT graph

        #         # data = self.processed_frame_queue.get(timeout=1)
        #         # rangeAxis = data[0]
        #         # vAxis = data[1]
        #         # angAxis = data[2]
        #         # angFFT = data[3]
        #         # ydata = data[4]
        #         # rdm = np.transpose(data[5])
        #         # rda = np.transpose(data[7])

        #         # self.rdm_plot.setImage(rdm, levels=[-50,30])  # Set the range doppler map image
        #         # self.range_plot.setData(rangeAxis, ydata)  # Set the range FFT graph

        #     except Exception as e:
        #         print('Error (graphing)', type(e), e)
        
        # Video stream
        if self.video_in.isOpened():
            try:
                ret, frame = self.capture_video_frame()  # handles preview/capture mode
                if ret and not self.is_in_capture_mode():
                    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                    frame = cv2.transpose(frame)
                    frame = cv2.flip(frame, 1)  # flip vert = 0, horz = 1
                    self.cam_img.setImage(frame)

            except Exception as e:
                print('Error (video)', type(e), e)
                self.video_in.release()

    def get_fps(self):
        now = time.time()
        dt = max(now - self.fps_lastupdate, 0.000000000001)
        fps2 = 1.0 / dt
        self.fps_lastupdate = now
        self.fps = self.fps * 0.9 + fps2 * 0.1
        return self.fps

    def closeEvent(self, event):
        self.updater.stop()  # stop updating gui
        self.end()
        print('Closed')

    def on_capture_btn_click(self):
        if self.is_in_capture_mode():  # if in capture mode, exit on 'End capture'
            self.closeEvent(None)
            QtCore.QCoreApplication.instance().quit()
            return
        
        # capture_directory = QFileDialog.getExistingDirectory(self, options=QtGui.QFileDialog.DontUseNativeDialog)
        # if not capture_directory:
        #     return

        self.capture_btn.setText('End Capture')
        self.begin_capture_mode()



if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    thisapp = MMWaveCaptureToolGUI()
    thisapp.show()
    sys.exit(app.exec_())
