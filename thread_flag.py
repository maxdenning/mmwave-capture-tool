import ctypes
import multiprocessing as mp


def read_flag(flag):
    with flag.get_lock():
        val = flag.value
    return val


def set_flag(flag, val):
    with flag.get_lock():
        flag.value = val
    return


def create_safe_flag(init = False):
    return mp.Value(ctypes.c_bool, init)
