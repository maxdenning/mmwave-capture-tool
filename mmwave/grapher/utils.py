import numpy as np
from mmwave.radar import RadarConfig


def angle_axis(rconf):
    return np.arange(-rconf.n_angle_bins/2, rconf.n_angle_bins/2) * (180/rconf.n_angle_bins)  # np.arcsin(2*phase*(np.pi/180))


def range_axis(rconf):
    return np.arange(0, rconf.range_max, rconf.range_max/rconf.n_range_bins)  # inter-bin resolution, not physical
    # return np.arange(0, rconf.range_max, rconf.range_res)
    # return (((np.arange(0, rconf.n_adc_samples) / rconf.n_adc_samples) * rconf.sample_rate) / rconf.frequency_slope) * rconf.SPEED_LIGHT / 2


def velocity_axis(rconf):
    # return np.arange(-rconf.n_chirp_loops_per_frame/2, rconf.n_chirp_loops_per_frame/2) * rconf.vres
    return np.arange(-rconf.doppler_max, rconf.doppler_max, rconf.doppler_res)
