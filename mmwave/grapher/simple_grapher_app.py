import numpy as np
import os
import pyqtgraph as pg
import sys
import time
from pyqtgraph.Qt import QtCore, QtGui


class SimpleGrapherGuiApp(QtGui.QMainWindow):
    def __init__(self):
        super().__init__(parent=None)

        # Create top-level gui container elements
        self.mainbox = QtGui.QWidget()
        self.setCentralWidget(self.mainbox)
        self.mainbox.setLayout(QtGui.QVBoxLayout())
        self.canvas = pg.GraphicsLayoutWidget()
        self.mainbox.layout().addWidget(self.canvas)

        # Label to display FPS
        self.fps_label = QtGui.QLabel()
        self.mainbox.layout().addWidget(self.fps_label)

        # FPS helpers
        self.fps = 0.0
        self.fps_lastupdate = time.time()

        # Init updater
        self.updater = pg.QtCore.QTimer(self)
        self.updater.timeout.connect(self._update)

    def start_updating(self, max_fps):
        self.updater.start(1000/max_fps)

    def _update(self):
        # Update FPS label
        self.fps_label.setText('Mean Frame Rate:  {fps:.3f} FPS'.format(fps=self.get_fps()))
        # Do custom update
        self.update()

    def update(self):
        pass  # to be overloaded
    
    def get_fps(self):
        now = time.time()
        dt = max(now - self.fps_lastupdate, 0.000000000001)
        fps2 = 1.0 / dt
        self.fps_lastupdate = now
        self.fps = self.fps * 0.9 + fps2 * 0.1
        return self.fps

    def close(self):
        self.updater.stop()  # stop updating gui

    def closeEvent(self, event):  # include in case it is not overloaded
        self.close()


def launch_qt_app(qtappcls, *args):
    app = QtGui.QApplication(sys.argv)
    thisapp = qtappcls(*args)
    thisapp.show()
    sys.exit(app.exec_())
