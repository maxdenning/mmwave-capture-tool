import numpy as np
import os
from mmwave.metadata import SampleMetaData, SampleScenario
from mmwave.postprocessing.postprocessing import ca_cfar_2d


POSTPROC_dB_BASELINE = -60.0  # dB
SAMPLE_LENGTH_SEC = 2.0  # seconds
DEFAULT_RADAR_CONFIG = 'config/config.mmwave.json'
DEFAULT_SAMPLE_RECORD = 'config/data-master.json'


def export_sample(sample_database, sample_data, output_path, classification, scenario, sample_set, source_file,
                  range_bin, doppler_bin, write_fn=np.save, overwrite=True):
    if not overwrite and os.path.isfile(output_path):
        print(f'File already exists: {output_path}')
        return

    write_fn(output_path, sample_data)
    sample_metadata = SampleMetaData(output_path, classification, scenario, sample_set, source_file, range_bin, doppler_bin)
    if not sample_database.add_sample(sample_metadata, overwrite=overwrite):
        print(f'Sample ID Conflict: {sample_metadata.id}')


def get_radar_config_path(input_dir):
    """Returns the path to a radar config. If config.mmwave.json is found locally, that is returned, otherwise the
    default is given"""
    path = os.path.join(os.path.dirname(input_dir), 'config.mmwave.json')  # use specific config if present
    if not os.path.exists(path):
        path = DEFAULT_RADAR_CONFIG  # default to standard config if specific does not exist
    return path


def peaks_of_rdm(frame, npeaks=0, **kwargs):
    peaks = ca_cfar_2d(frame, **kwargs)

    # keep only the n highest peaks
    if npeaks <= 0 or npeaks > len(peaks):
        npeaks = len(peaks)

    # sort and return peaks by descending power value
    desc_order_peak_value_idx = np.argsort([frame[x, y] for x, y in peaks])[-npeaks:][::-1]
    return [peaks[i] for i in desc_order_peak_value_idx]


def peaks_to_rdm(peaks, orig_frame, roi_x=1, roi_y=1, base_val=POSTPROC_dB_BASELINE):
    if not roi_x % 2 or not roi_y % 2:
        raise ValueError(f'Region of interest dimensions odd: {roi_x}, {roi_y}')
    rx = (roi_x - 1) // 2
    ry = (roi_y - 1) // 2

    pframe = np.full(orig_frame.shape, base_val, np.float)
    for x,y in peaks:
        pframe[x-rx : x+rx+1, y-ry : y+ry+1] = orig_frame[x-rx : x+rx+1, y-ry : y+ry+1]
    return pframe


def read_serialised_sample(input_filepath):
    return None  # TODO: implement


def read_target_track(input_filepath):
    indexed_windows = {}
    with open(input_filepath, 'r') as csv:
        lines = csv.readlines()

    for line in lines:
        n = [int(v) for v in line.strip().split(',')]
        indexed_windows[n[0]] = (n[1], n[2], n[3], n[4])
    return indexed_windows


def write_serialised_sample(output_path, sample):
    with open(output_path, 'w+') as file:
        for frame in sample:
            for i, row in enumerate(frame):
                if i > 0:
                    file.write(',')
                file.write(' '.join(str(v) for v in row))
            file.write('\n')


def write_target_track(output_path, windows):
    with open(output_path, 'w') as file:
        file.write('\n'.join(f'{i},{",".join(str(v) for v in window)}' for i, window in windows))
