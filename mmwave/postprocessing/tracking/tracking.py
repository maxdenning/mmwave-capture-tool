import abc
import cv2 as cv
import numpy as np
from multiprocessing import Pool, cpu_count
from mmwave.postprocessing import postprocessing, utils


class TargetTrackerBase(abc.ABC):
    def track_samples(self, scene_itr, sample_length):
        windows = [[]] * sample_length
        for i, (frame_idx, window) in enumerate(self.track(scene_itr)):
            windows[i % sample_length] = window  # buffer window coords
            if not (i+1) % sample_length:
                yield (frame_idx+1) - sample_length, windows

    def track(self, scene_itr):
        with Pool(cpu_count()) as pool:
            preprocessed_scene_itr = pool.imap(self._preprocess, scene_itr, chunksize=1)
            for frame_idx, window in self._track(preprocessed_scene_itr):
                yield frame_idx, window
    
    def extract(self, scene_slice, sample_res, windows):
        sample = np.zeros((len(scene_slice), *sample_res))
        for j, (x,y,w,h) in enumerate(windows):
            sample[j] = scene_slice[j, x:x+w, y:y+h]
        return sample

    @abc.abstractmethod
    def _preprocess(self, frame):
        pass

    @abc.abstractmethod
    def _track(self, scene_itr):
        pass
