import numpy as np
from mmwave.postprocessing.tracking import tracking, utils as tr_utils
from mmwave.postprocessing import postprocessing, utils as pp_utils


class FixedPeakTracker(tracking.TargetTrackerBase):
    def __init__(self, roi_sz, radar_config, dist_threshold, m=9, n=9, P_fa=0.003, gm=3, gn=3):
        self.roi_sz = roi_sz
        self.rconf = radar_config
        self.dist_threshold = dist_threshold
        self.cfar_args = {'m': m, 'n': n, 'P_fa': P_fa, 'gm': gm, 'gn': gn}

    def _preprocess(self, frame):
        # TODO: add other noise removal here, or in fft step?
        postprocessing.zero_vbin_removal(frame, self.rconf)
        return pp_utils.peaks_of_rdm(frame, **self.cfar_args, npeaks=8)

    def _track(self, scene_peaks_itr):
        momentum = np.zeros((2), dtype=float)
        cx, cy = next(scene_peaks_itr)[0]
        yield 0, tr_utils.make_window(cx, cy, *self.roi_sz)  # return first window

        # iterate through frames selecting the nearest peak
        for i, peaks in enumerate(scene_peaks_itr):
            (cx, cy), momentum = tr_utils.track_peaks(peaks, (cx, cy), momentum, self.dist_threshold)
            yield i+1, tr_utils.make_window(cx, cy, *self.roi_sz)
