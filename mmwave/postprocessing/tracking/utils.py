import cv2 as cv
import numpy as np
from mmwave.postprocessing import postprocessing


def backproject(frame, target_histogram):
    hsv = cv.applyColorMap(postprocessing.rdm_to_greyscale(frame), cv.COLORMAP_HSV)
    dst = cv.calcBackProject([255 - hsv], [0], target_histogram, [0, 255], 1)
    return dst


def calculate_target_histrogram(frame, window):
    roi = postprocessing.rdm_to_greyscale(frame[window[0]:window[0] + window[2], window[1]:window[1] + window[3]])
    hsv_roi = cv.applyColorMap(roi, cv.COLORMAP_HSV)
    # mask = cv.inRange(hsv_roi, np.array([0, 0, 0]), np.array([255, 255, 255]))
    mask = None  # no mask as there are no false values to remove (is this true? - noise?)
    roi_hist = cv.calcHist([hsv_roi], [0], mask, [255], [0, 180])  # use 0 channel so only hue is considered
    cv.normalize(roi_hist, roi_hist, 0, 255, cv.NORM_MINMAX)
    return roi_hist


def make_window(cx, cy, w, h):
    # converts xywh -> ltwh
    window = int(cx - (w/2)), int(cy - (h/2)), int(w), int(h)
    if window[0] < 0 or window[1] < 0:
        raise ValueError(f'window coords out of bounds: {window}')
    return window


def swap_window_context(window):
    # rotates from cv image to numpy matrix
    return window[1], window[0], window[3], window[2]


def track_peaks(peaks, last, momentum, dist_threshold=5.0, hist_weight=0.65):
    current = last + momentum
    step = np.zeros((2))

    if peaks:
        dists = [np.sqrt(np.sum((np.array([x,y]) - last) ** 2)) for x,y in peaks]
        mindist = np.argmin(dists)
        
        if dists[mindist] < dist_threshold:
            current = np.array(peaks[mindist])
            step = last - current

    momentum = (momentum * hist_weight) + (step * (1 - hist_weight))
    last = current
    return current, momentum
