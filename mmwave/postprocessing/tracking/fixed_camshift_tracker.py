import cv2 as cv
import numpy as np
from mmwave.postprocessing.tracking import tracking, utils as tr_utils
from mmwave.postprocessing import postprocessing, utils as pp_utils


class FixedCAMShiftTracker(tracking.TargetTrackerBase):
    def __init__(self, roi_sz, radar_config, m=9, n=9, P_fa=0.003, gm=3, gn=3, debug=False):
        self.roi_sz = roi_sz
        self.rconf = radar_config
        self.cfar_args = {'m': m, 'n': n, 'P_fa': P_fa, 'gm': gm, 'gn': gn}
        self.debug = debug
    
    def _preprocess(self, frame):
        postprocessing.zero_vbin_removal(frame, self.rconf)
        return postprocessing.high_pass_filter(frame, -30.0, val=-30.0)

    def _track(self, scene_itr):
        if self.debug:
            cv.namedWindow('img', cv.WINDOW_NORMAL)
            cv.resizeWindow('img', 1024, 256)

        frame = next(scene_itr)
        init_coords = pp_utils.peaks_of_rdm(frame, npeaks=1, **self.cfar_args) #[0]
        if not init_coords:
            raise ValueError('Cannot detect any peaks for camshift initialisation')
        window = tr_utils.make_window(*init_coords[0], *self.roi_sz)
        yield 0, window

        target_histogram = tr_utils.calculate_target_histrogram(frame, window)
        term_crit = (cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, 10, 4)  # termination critera
        window = tr_utils.swap_window_context(window)  # swap from np to cv

        for i, frame in enumerate(scene_itr):
            dst = tr_utils.backproject(frame, target_histogram)
            ret, window = cv.CamShift(dst, window, term_crit)
            window = tr_utils.make_window(window[0] + (window[2] // 2), window[1] + (window[3] // 2), *self.roi_sz)
            yield i + 1, tr_utils.swap_window_context(window)  # swap from cv to np

            if self.debug:
                img = cv.applyColorMap(255 - postprocessing.rdm_to_greyscale(frame), cv.COLORMAP_HSV)
                img = cv.rectangle(img, (window[0], window[1]), (window[0]+window[2], window[1]+window[3]), 180, 1)
                cv.imshow('img', cv.rotate(img, cv.ROTATE_90_COUNTERCLOCKWISE))
                if cv.waitKey(40) > -1:
                    break
        
        if self.debug:
            cv.destroyAllWindows()
