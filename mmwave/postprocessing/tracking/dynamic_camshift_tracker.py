import cv2 as cv
import numpy as np
from mmwave.postprocessing.tracking import fixed_camshift_tracker, tracking, utils as tr_utils
from mmwave.postprocessing import postprocessing, utils as pp_utils


class DynamicCAMShiftTracker(fixed_camshift_tracker.FixedCAMShiftTracker):
    def __init__(self, min_roi_sz, max_roi_sz, radar_config, mode, sample_length=-1, m=9, n=9, P_fa=0.003, gm=3, gn=3, debug=False):
        super().__init__(min_roi_sz, radar_config, m, n, P_fa, gm, gn, debug)
        if min_roi_sz[0] > max_roi_sz[0] or min_roi_sz[1] > max_roi_sz[1]:
            raise ValueError('min roi is not smaller than max roi')            
        if mode not in ['min', 'max']:
            raise ValueError(f'mode not recognised: {mode}')
        self.min_roi_sz = min_roi_sz
        self.max_roi_sz = max_roi_sz
        self.in_minimise_mode = mode == 'min'  # defines whether to use the smallest bounding box or largest
        self.sample_length = sample_length
        if not self.in_minimise_mode and sample_length <= 0:
            raise ValueError('invalid sample length')

    def _track(self, scene_itr):
        if self.debug:
            cv.namedWindow('img', cv.WINDOW_NORMAL)
            cv.resizeWindow('img', 1024, 256)
        
        frame = next(scene_itr)
        init_coords = pp_utils.peaks_of_rdm(frame, npeaks=1, **self.cfar_args)[0]
        window = tr_utils.make_window(*init_coords, *self.max_roi_sz)

        target_histogram = tr_utils.calculate_target_histrogram(frame, window)
        term_crit = (cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, 10, 4)  # termination critera
        window = tr_utils.swap_window_context(window)  # swap from np to cv

        dst = tr_utils.backproject(frame, target_histogram)
        ret, window = cv.CamShift(dst, window, term_crit)

        if self.in_minimise_mode:
            yield 0, self.adjust_window_size(tr_utils.swap_window_context(window))  # swap from cv to np, and adjust
        else:
            window_buffer = [()] * self.sample_length
            window_buffer[0] = tr_utils.swap_window_context(window)  # swap from cv to np


        for i, frame in enumerate(scene_itr):
            dst = tr_utils.backproject(frame, target_histogram)
            ret, window = cv.CamShift(dst, window, term_crit)

            if self.in_minimise_mode:
                yield i + 1, self.adjust_window_size(tr_utils.swap_window_context(window))  # swap from cv to np, and adjust
            else:
                if not (i+1) % self.sample_length:
                    window_buffer = self.adjust_all_windows_size(window_buffer)
                    for j, win in enumerate(window_buffer):
                        yield i - self.sample_length + j + 1, win

                window_buffer[(i+1) % self.sample_length] = tr_utils.swap_window_context(window)  # swap from cv to np

            if self.debug:
                img = cv.applyColorMap(postprocessing.rdm_to_greyscale(frame), cv.COLORMAP_HSV)
                img = cv.rectangle(img, (window[0], window[1]), (window[0]+window[2], window[1]+window[3]), 180, 1)
                cv.imshow('img', cv.rotate(img, cv.ROTATE_90_COUNTERCLOCKWISE))
                if cv.waitKey(40) > -1:
                    break

        if self.debug:
            cv.destroyAllWindows()


    def adjust_window_size(self, window):
        # expects windows in np context, not cv
        x, y, w, h = window
        # make even & ensure size is within bounds
        w += w % 2
        h += h % 2
        w = min(max(w, self.min_roi_sz[0]), self.max_roi_sz[0])
        h = min(max(h, self.min_roi_sz[1]), self.max_roi_sz[1])
        # adjust left and bottom points
        x += (window[2] - w) // 2
        y += (window[3] - h) // 2
        return x, y, w, h

    
    def adjust_all_windows_size(self, windows):
        # expects windows in np context, not cv
        max_w, max_h = self.min_roi_sz
        
        # get largest window in each dimension
        for window in windows:
            max_w = max(max_w, window[2])
            max_h = max(max_h, window[3])

        # make even and apply upper bound
        max_w = min(max_w + (max_w % 2), self.max_roi_sz[0])
        max_h = min(max_h + (max_h % 2), self.max_roi_sz[1])

        # TODO: adjust centre for each window

        xs, ys = [], []
        for window in windows:
            xs.append(window[0] + (window[2] - max_w) // 2)
            ys.append(window[1] + (window[3] - max_h) // 2)

        # adjusted_windows = [(x,y,max_w, max_h) for x,y,w,h in windows]
        adjusted_windows = [(xs[i], ys[i], max_w, max_h) for i in range(len(windows))]
        return adjusted_windows


    def extract(self, scene_slice, sample_res, windows):
        sample = np.full((len(scene_slice), *sample_res), pp_utils.POSTPROC_dB_BASELINE)

        for j, (x,y,w,h) in enumerate(windows):
            if w > sample_res[0] or h > sample_res[1]:
                raise ValueError(f'Dynamic window size is larger than max sample resolution: ({w},{h}) > {sample_res}')
            # centre each window in the sample so it is equally padded on all sides
            xpad = (sample_res[0] - w) // 2
            ypad = (sample_res[1] - h) // 2
            # print(j,x,y,w,h,xpad,ypad)
            sample[j, xpad:xpad+w, ypad:ypad+h] = scene_slice[j, x:x+w, y:y+h]

        return sample
