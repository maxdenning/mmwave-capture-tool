import numpy as np
from mmwave.postprocessing.tracking import tracking
from mmwave.postprocessing import postprocessing, utils


class EmptySceneTracker(tracking.TargetTrackerBase):
    def __init__(self, roi_sz, radar_config, scene_length, sample_length):
        self.roi_sz = roi_sz
        self.rconf = radar_config
        self.scene_length = scene_length  # assumes scene is a finite length, unlike the other trackers
        self.sample_length = sample_length

    def _preprocess(self):
        return None

    def _track(self, _):
        range_scale_factor = self.rconf.n_range_bins / self.rconf.range_max
        centre_vbin = self.rconf.n_doppler_bins // 2
        centre_vbin_deadzone = 4  # size of zone either side, total width = 2 * deadzone

        if self.roi_sz[0] > self.rconf.n_range_bins or self.roi_sz[1] > centre_vbin - centre_vbin_deadzone:
            raise ValueError(f'ROI size is too large: {self.roi_sz} > ({self.rconf.n_range_bins}, {centre_vbin - centre_vbin_deadzone})')

        # moving sample windows (define left and bottom for each)
        r_win_bot = (int(5 * range_scale_factor), centre_vbin + centre_vbin_deadzone)
        # r_win_top = (int(5 * range_scale_factor), centre_vbin + centre_vbin_deadzone + self.roi_sz[1]*2 + 1)
        r_win_top = (int(5 * range_scale_factor), self.rconf.n_doppler_bins - self.roi_sz[1])
        a_win_bot = (int(40 * range_scale_factor), centre_vbin + centre_vbin_deadzone)
        # a_win_top = (int(40 * range_scale_factor), centre_vbin + centre_vbin_deadzone + self.roi_sz[1]*2 + 1)
        a_win_top = (int(40 * range_scale_factor), self.roi_sz[1])

        length = self.scene_length - (self.scene_length % self.sample_length)
        step_x = min(1, ((40 - 5) * range_scale_factor) / length)
        
        r_win_bot_ls = [(int(r_win_bot[0] + (step_x*i)), r_win_bot[1], *self.roi_sz) for i in range(length)]
        r_win_top_ls = [(int(r_win_top[0] + (step_x*i)), r_win_top[1], *self.roi_sz) for i in range(length)]
        a_win_bot_ls = [(int(a_win_bot[0] - (step_x*i)), a_win_bot[1], *self.roi_sz) for i in range(length)]
        a_win_top_ls = [(int(a_win_top[0] - (step_x*i)), a_win_top[1], *self.roi_sz) for i in range(length)]

        for i, window_list in enumerate([r_win_bot_ls, r_win_top_ls, a_win_bot_ls, a_win_top_ls]):
            for j, window in enumerate(window_list):
                yield i+j, window  # i+j to offset the start
