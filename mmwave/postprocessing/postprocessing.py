import numpy as np
import os
from mmwave.radar import RadarConfig
from struct import unpack


def ca_cfar_2d(rdm, m, n, P_fa, gm=3, gn=3):
    # 2d cell averaging cfar
    # MxN reference area, excluding CUT at centre surrounded by (gm * gn - 1) guard cells
    # Nr: number of reference cells
    # u: average power noise of reference area
    # alpha_CA: threshold factor -> alpha_CA = Nr * (P_fa^[-1/Nr] - 1)
    # P_fa: false alarm rate -> P_fa = P(Y0 >= alpha_CA * u)
    # T: theshold -> T = alpha_CA * u (threshold factor * power noise of reference area)
    # returns: list of co-ordinate tuples, e.g. [(x,y),(x,y)]

    if not m % 2 or not n % 2 or not gm % 2 or not gn % 2:
        raise ValueError(f'Reference area and guard area must have odd dimensions: {m}x{n} {gm}x{gn}')

    if m < gm or n < gn:
        raise ValueError(f'Reference area of size {m}x{m} cannot contain guard area of size {gm}x{gn}')

    nref = (m * n) - (gm * gn)  # Nr = MxN - centre region (guard and CUT)
    threshold_factor = nref * (P_fa ** (-1/nref) - 1)  # alpha_CA
    nside_r = (m - 1) // 2  # number of range bins per side of the CUT
    nside_d = (n - 1) // 2  # number of doppler bins per size of the CUT
    nside_gm = (gm - 1) // 2
    nside_gn = (gn - 1) // 2
    peaks = []
    
    for r in range(nside_r, rdm.shape[0] - nside_r):
        for d in range(nside_d, rdm.shape[1] - nside_d):
            aut = rdm[r - nside_r:r + nside_r + 1, d - nside_d:d + nside_d + 1]  # area under test

            # if CUT is not max in its area, continue (using adjusted r and d co-ordinates)
            max_val_r, max_val_d = np.unravel_index(np.argmax(aut), aut.shape)  # get coords of max value in area
            if r != r - nside_r + max_val_r or d != d - nside_d + max_val_d:
                continue

            # average of reference cells (= [sum(MxN) - sum(gm * gn)] / number of reference cells)
            sum_guard = np.sum(rdm[r - nside_gm:r + nside_gm + 1, d - nside_gn: d + nside_gn + 1])
            avg_power_noise = (np.sum(aut) - sum_guard) / nref
            
            # cell under test > threshold
            if rdm[r, d] > threshold_factor * avg_power_noise:
                peaks.append((r, d))

    return peaks


def create_cube(data_for_fft, rconf):
    # Create holders of data for first chirp and initialise to zero.
    cCube = np.zeros((rconf.n_adc_samples, rconf.nTx * rconf.nRx, rconf.n_chirp_loops_per_frame), dtype=np.complex)

    # Table 23.8 of mmwave_studio_user_guide.pdf implies following indices for real (R) and complex (C) data.
    indices_r1 = np.arange(0, rconf.n_adc_samples * rconf.complex_factor, 4)
    indices_r2 = np.arange(1, rconf.n_adc_samples * rconf.complex_factor, 4)
    indices_c1 = np.arange(2, rconf.n_adc_samples * rconf.complex_factor, 4)
    indices_c2 = np.arange(3, rconf.n_adc_samples * rconf.complex_factor, 4)

    # Always assume 4 RX active
    # 4 RX and 1 TX case
    if rconf.nTx == 1:
        # Form the complex data array
        cCube[0:rconf.n_adc_samples:2, :, :] = data_for_fft[indices_r1, :, :] + 1j * data_for_fft[indices_c1, :, :]
        cCube[1:rconf.n_adc_samples:2, :, :] = data_for_fft[indices_r2, :, :] + 1j * data_for_fft[indices_c2, :, :]

    # Case of 4 RX and 2 TX (TDM).
    elif rconf.nTx == 2:

        # Reorder chirps from TDM
        # Even chirps corresponds to TX1, odd chirps to TX2
        rx1_to_4idx = np.arange(0, rconf.nRx * rconf.nTx, rconf.nTx)
        rx5_to_8idx = np.arange(1, rconf.nRx * rconf.nTx, rconf.nTx)

        tdmReorder = np.zeros((rconf.n_adc_samples * rconf.complex_factor, rconf.nRx * rconf.nTx, rconf.n_chirp_loops_per_frame), dtype=np.complex)
        tdmReorder[:, 0:4, :] = data_for_fft[:, rx1_to_4idx, :]
        tdmReorder[:, 4:8, :] = data_for_fft[:, rx5_to_8idx, :]

        # Form the complex data array
        cCube[0:rconf.n_adc_samples:2, :, :] = tdmReorder[indices_r1, :, :] + 1j * tdmReorder[indices_c1, :, :]
        cCube[1:rconf.n_adc_samples:2, :, :] = tdmReorder[indices_r2, :, :] + 1j * tdmReorder[indices_c2, :, :]

    # Normalise the data
    cCube = rconf.norm_factor * cCube
    return cCube


def high_pass_filter(rdm_frames, cutoff_db, val):
    nf = np.vectorize(lambda c: val if c < cutoff_db else c, otypes=[np.float])
    return nf(rdm_frames)


def normalise(scene, scale=(0,1)):
    # rescales scene into given range
    return np.array(((scene - scene.min()) * (scale[1] - scale[0])) / max(1, (scene.max() - scene.min())), dtype=float)


def range_doppler_ft(frame_data, rconf):
    # Convert byte array to uint16 (2 bytes per uint16)
    count = int(len(frame_data) / 2)
    frame_data_uint16 = unpack('<' + 'h' * count, frame_data)

    # Reshape the data for frame so that the data it is in a processable format
    data_for_fft = np.reshape(frame_data_uint16, (rconf.n_adc_samples * rconf.complex_factor, rconf.nRx * rconf.nTx, -1), order="F")    
    cCube = create_cube(data_for_fft, rconf)

    # Now perform FFT for each chirp in frame
    windowR = np.hanning(rconf.n_adc_samples)
    windowD = np.hanning(rconf.n_chirp_loops_per_frame)

    # Compute range FFT and average it
    range_power = np.zeros(rconf.n_range_bins)
    Y = np.zeros((rconf.n_range_bins, rconf.nRx * rconf.nTx), dtype=np.complex)
    for rx_idx in range(rconf.nRx * rconf.nTx):
        for chirp_idx in range(rconf.n_chirp_loops_per_frame):
            y = cCube[:, rx_idx, chirp_idx] * windowR
            Y[:, rx_idx] = np.fft.fft(y, rconf.n_range_bins)
            range_power = range_power + np.abs(Y[:, rx_idx])

    range_power = range_power / (rconf.n_chirp_loops_per_frame * rx_idx * rconf.nTx)
    range_power = 20 * np.log10(range_power)

    # Compute range-angle map
    rd_angle = np.zeros((rconf.n_range_bins, rconf.n_angle_bins))
    for range_idx in range(rconf.n_range_bins):
        rd_angle[range_idx, :] = np.abs(np.fft.fft(Y[range_idx, :], rconf.n_angle_bins))
        rd_angle[range_idx, :] = np.fft.fftshift(rd_angle[range_idx, :])

    rd_angle = 20 * np.log10(rd_angle)

    # Form range-doppler map
    # Y2 = np.zeros((rconf.n_range_bins, rconf.n_doppler_bins))
    rdCube = np.zeros((rconf.n_range_bins, rconf.n_doppler_bins, rconf.nRx * rconf.nTx), dtype=np.complex)
    for rx_idx in range(rconf.nRx * rconf.nTx):
        Y1 = np.fft.fft(windowR[:, np.newaxis] * cCube[:, rx_idx, :], rconf.n_range_bins, axis=0)
        rdCube[:, :, rx_idx] = np.fft.fftshift(np.fft.fft(windowD * Y1, rconf.n_doppler_bins, axis=1), 1)

        # Amplitude detection and non-coherent integration for rdm
        # Y2 = Y2 + np.abs(rdCube[:, :, rx_idx])

    # Output rdm as power (dB).
    # logY2 =  20 * np.log10(Y2 / (rconf.nRx * rconf.nTx))

    # use median blending to reduce noise and form final map from Rx readings
    Y2 = np.median(np.abs(rdCube), axis=2)
    logY2 = 20 * np.log10(Y2)  # output rdm as power (dB)


    #### range doppler angle ####
    # rdCube = np.zeros((rconf.n_range_bins, rconf.n_doppler_bins, rconf.nRx * rconf.nTx), dtype=np.complex)
    # for rx_idx in range(rconf.nRx * rconf.nTx):
    #     Y1 = np.fft.fft(windowR[:, np.newaxis] * cCube[:, rx_idx, :], rconf.n_range_bins, axis=0)
    #     rdCube[:, :, rx_idx] = np.fft.fft(windowD * Y1, rconf.n_doppler_bins, axis=1)

    # rda = np.zeros((rconf.n_range_bins, rconf.n_doppler_bins, rconf.n_angle_bins), dtype=np.complex)
    # rda = np.fft.fft(rdCube, rconf.n_angle_bins, axis=2)
    # rda = np.fft.fftshift(rda, axes=2)
    # rda = 20 * np.log10
    # # for range_idx in range(rconf.n_range_bins):
    # #     rda[range_idx, :, :] = np.abs(np.fft.fft(rdCube[range_idx, :, :], rconf.n_angle_bins), axis=0)
    # #     # rda[range_idx, :, :] = np.fft.fftshift(rda[range_idx, :, :])

    return range_power, rd_angle, logY2  # range power, range-angle map, range-doppler map


def rdm_to_greyscale(frame):
    img = (frame + np.abs(frame.min())) / max(1, np.abs(frame.max() - frame.min())) * 255  # scale values 0 - 255
    return np.array(img, dtype=np.uint8)


def zero_vbin_removal(rdm, radar_config, vlimit=0.25, val=None):
    # remove zero velocity bins (and those either side of it)
    # rdm: accepts either frame or scene
    # vupper: velocity up to which bins should be removed (ms^-1)
    # val: if None, then bins are replaces with outer bin noise, otherwise they are replaced with this value

    if val is None and (vlimit is None or vlimit < 0):
        raise ValueError('Invalid args')

    # check if input is a single frame or multiple frames
    is_scene = len(rdm.shape) >= 3
    zero_bin = rdm.shape[2 if is_scene else 1] // 2

    # select bins to remove
    lim = int(round(vlimit / radar_config.doppler_res))
    velocity_bins_to_remove = range(zero_bin - lim, zero_bin + lim + 1)

    # replace bins
    if is_scene:
        rdm[:, :, velocity_bins_to_remove] = val if val is not None else rdm[:, :, 0:len(velocity_bins_to_remove)]
    else:
        rdm[:, velocity_bins_to_remove] = val if val is not None else rdm[:, 0:len(velocity_bins_to_remove)]


    # --- replace centre bin with outer bins
    # NOT using a repeat of the inner bins (e.g. 63 = 62, 64 = 62, 65 = 66) because this amplifies the importance of
    # (/repeats) the effect of any noise that creeps out of the deadzone bins. (the signals/noise gets stretched over
    # 5 bins)
    # INSTEAD use contiguous bins from an outer edge, the noise is not completely random (could be environmental?), so
    # this maintains the underlying pattern. (instead of using both outer edges and not being contiguous)
    # not suitable for large number of bins


    # --- butterworth filter approach
    # t63 = rdmf_n[0, :, 63]
    # t64 = rdmf_n[0, :, 64]
    # t65 = rdmf_n[0, :, 65]

    # b, a = signal.butter(4, 0.04, 'high', analog=True)

    # f63 = signal.filtfilt(b, a, rdmf_n[0, :, 63])
    # f64 = signal.filtfilt(b, a, rdmf_n[0, :, 64])
    # f65 = signal.filtfilt(b, a, rdmf_n[0, :, 65])
    # ---
