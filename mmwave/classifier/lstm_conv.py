import tensorflow as tf
from mmwave.classifier import network_base, utils
from mmwave.metadata import SampleClassification
from tensorflow import keras
from tensorflow.python.keras import activations, layers, backend as K


class ComplexLSTM(network_base.TensorflowNetwork):

    def build(self, input_dims, **kwargs):
        in0 = layers.Input(shape=input_dims)
        x = layers.ConvLSTM2D(6, (3, 3), strides=(1, 1), activation='relu', recurrent_activation='tanh', padding='same', return_sequences=True)(in0)
        x = layers.TimeDistributed(layers.MaxPool2D((2, 2)))(x)
        x = layers.ConvLSTM2D(16, (3, 3), strides=(1, 1), activation='relu', recurrent_activation='tanh', padding='same', return_sequences=True)(in0)
        x = layers.TimeDistributed(layers.MaxPool2D((2, 2)))(x)
        x = layers.Flatten()(x)
        x = layers.Dense(units=120, activation='relu')(x)
        x = layers.Dense(units=84, activation='relu')(x)
        out0 = layers.Dense(units=len(SampleClassification), activation='softmax')(x)

        model = keras.models.Model(inputs=[in0], outputs=[out0])
        model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
        return model



    def evaluate(self, x, y, meta, batch_size, **kwargs):
        loss, acc = self.model.evaluate(x, y, batch_size=batch_size, verbose=0)
        print(f'Loss: {loss:.5f}\tAccuracy: {acc:.5f}')
