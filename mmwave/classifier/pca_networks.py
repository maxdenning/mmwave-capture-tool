import sklearn.decomposition
from mmwave.classifier import LeNet5, LeNet5_3D, SelfOrganisingMap, T_SNE, utils

def pca_preprocess_wrapper(variance_prop, preprocess_fn, *args, reconstruct=True, **kwargs):
    train, train_meta, validation, validation_meta, test, test_meta = preprocess_fn(*args, **kwargs)
    train_s_re, validation_s_re, test_s_re = utils.pca_preprocess_sample_sets(variance_prop, train[0], validation[0], test[0], reconstruct=reconstruct, verbose=kwargs.get('verbose', False))

    # utils.visualise_multi(10, train[1], train[0], train_s_re)
    # utils.visualise_multi(10, validation[1], validation[0])
    # utils.visualise_multi(10, test[1], test[0], test_s_re)

    return \
        (train_s_re, *train[1:]), train_meta, \
        (validation_s_re, *validation[1:]), validation_meta, \
        (test_s_re, *test[1:]), test_meta


class LeNet5_PCA(LeNet5):
    @staticmethod
    def preprocess(train_samples, train_meta, validation_samples, validation_meta, test_samples, test_meta, pca_variance, **kwargs):
        return pca_preprocess_wrapper(pca_variance, LeNet5.preprocess, train_samples, train_meta, validation_samples, validation_meta, test_samples, test_meta, **kwargs)


class LeNet5_3D_PCA(LeNet5_3D):
    @staticmethod
    def preprocess(train_samples, train_meta, validation_samples, validation_meta, test_samples, test_meta, pca_variance, **kwargs):
        return pca_preprocess_wrapper(pca_variance, LeNet5_3D.preprocess, train_samples, train_meta, validation_samples, validation_meta, test_samples, test_meta, **kwargs)


class SelfOrganisingMap_PCA(SelfOrganisingMap):
    @staticmethod
    def preprocess(train_samples, train_meta, validation_samples, validation_meta, test_samples, test_meta, n_samples, pca_variance, **kwargs):
        (samples,), meta, _, _, _, _ = SelfOrganisingMap.preprocess(train_samples, train_meta, validation_samples, validation_meta, test_samples, test_meta, n_samples, **kwargs)
        pca = sklearn.decomposition.PCA(pca_variance)
        pca_samples = pca.fit_transform(samples)


        # class_idxs = [i for i in range(len(meta)) if meta[i]['class'] == SampleClassification.CAR.value]
        # pca_samples = pca_samples[class_idxs]
        # meta = [meta[i] for i in class_idxs]

        # print(pca_samples.shape)

        # recon = pca.inverse_transform(pca_samples).reshape(-1, 32, 32)
        # utils.visualise_multi(10, utils.sample_labels(meta), samples.reshape(-1, 32, 32), recon)
        
        # exit(1)

        return (pca_samples,), meta, (pca_samples,), meta, (pca_samples,), meta


class T_SNE_PCA(T_SNE):
    @staticmethod
    def preprocess(train_samples, train_meta, validation_samples, validation_meta, test_samples, test_meta, n_samples, pca_variance, **kwargs):
        return pca_preprocess_wrapper(pca_variance, T_SNE.preprocess, train_samples, train_meta, validation_samples, validation_meta, test_samples, test_meta, n_samples, reconstruct=False, **kwargs)
