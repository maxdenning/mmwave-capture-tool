import numpy as np
from mmwave.metadata import SampleClassification

from sklearn import metrics
import matplotlib.pyplot as plt
# from tensorflow.python.keras import backend as K
# import tensorflow as tf


# def confusion_matrix(actuals, predictions):
#     # confusion[ ACTUAL ][ PREDICTED ]
#     # confusion = np.zeros((len(SampleClassification), len(SampleClassification)), dtype=int)
#     # for i in range(len(predictions)):
#     #     predicted = np.argmax(predictions[i])
#     #     actual = np.argmax(actuals[i])
#     #     confusion[actual, predicted] += 1


#     y_true = np.argmax(actuals, axis=1)
#     y_pred = np.argmax(predictions, axis=1)


#     # y_true_r = np.round(actuals)
#     # y_pred_r = np.round(predictions)


#     # print('_____1_____')
#     # print(confusion)

#     # print('_____2_____')
#     # print(y_true.shape, y_pred.shape)
#     # print(metrics.confusion_matrix(y_true, y_pred))

#     # print('_____3_____')
#     # print(actuals.shape, predictions.shape)
#     # f1s0 = metrics.f1_score(y_true, y_pred, average='macro')
#     # print(f1s0)

#     print('_____4_____')
#     print(metrics.classification_report(y_true, y_pred, target_names=[c.name for c in SampleClassification], digits=4))

#     # print('_____5_____')
#     # print(y_true_r.shape, y_pred_r.shape, y_pred_r[:5])
#     # print(metrics.confusion_matrix(y_true_r, y_pred_r))

#     # print('_____6_____')
#     # print(metrics.f1_score(y_true_r, y_pred_r, average='macro'))


#     # return confusion
#     return metrics.confusion_matrix(y_true, y_pred)


def confusion_table(matrix, classification):
    # table[ ACTUAL ][ PREDICTED ]
    table = np.zeros((2, 2))
    table[0, 0] = matrix[classification, classification] # True Positive
    table[0, 1] = sum(matrix[:, classification]) - table[0, 0] # False Positive
    table[1, 0] = sum(matrix[classification, :]) - table[0, 0] # False Negative
    table[1, 1] = matrix.sum() - table.sum() # True Negative
    return table


def accuracy(table):
    # (TP + TN) / (TP + TN + FP + FN)
    return (table[0, 0] + table[1, 1]) / table.sum()


def precision(table):
    # TP / (TP + FP)
    if table[0, 0] == 0 and table[0, 1] == 0:
        return 0.0
    return table[0, 0] / (table[0, 0] + table[0, 1])


def recall(table):
    # TP / (TP + FN)
    if table[0, 0] == 0 and table[1, 0] == 0:
        return 0.0
    return table[0, 0] / (table[0, 0] + table[1, 0])


def f1_score(table):
    # (2 * TP) / ([2 * TP] + FP + FN)
    if precision(table) == 0 and recall(table) == 0:
        return 0.0
    return 2 * ((precision(table) * recall(table)) / (precision(table) + recall(table)))


def confusion_matrix_to_str(matrix):
    title = 'PRED \\ ACTU |' + '|'.join(f'{c.name:^12}' for c in SampleClassification) + '|'
    body = []
    for classification in SampleClassification:
        s = f'{classification.name:<12}|{"|".join(f"{v:>12}" for v in matrix[:, classification.value])}|'
        body.append(s)
    return title + '\n' + '\n'.join(body)


def confusion_table_to_str(table, classification):
    title = 'PRED \\ ACTU    |' + f'{classification.name:^15}|' + f'{("NON-" + classification.name):^15}' 
    ln0 =            f'{classification.name:<15}|' + f'{(str(table[0,0]) + " TP"):>15}|' + f'{(str(table[0,1]) + " FP"):>15}'
    ln1 = f'{("NON-" + classification.name):<15}|' + f'{(str(table[1,0]) + " FN"):>15}|' + f'{(str(table[1,1]) + " TN"):>15}'
    metrics = f'Accuracy: {accuracy(table):.5f}, F1 Score: {f1_score(table):.5f}'
    return '\n'.join([title, ln0, ln1, metrics])


def classification_report(y_true, y_pred):
    true = np.argmax(y_true, axis=1)
    pred = np.argmax(y_pred, axis=1)

    confusion_matrix = metrics.confusion_matrix(true, pred)
    print(confusion_matrix_to_str(confusion_matrix), '\n')
    print(metrics.classification_report(true, pred, target_names=[c.name for c in SampleClassification], digits=4))

    table = confusion_table(confusion_matrix, SampleClassification.NONE.value)
    print(confusion_table_to_str(table, SampleClassification.NONE), '\n')

    table = confusion_table(confusion_matrix, SampleClassification.PEDESTRIAN.value)
    print(confusion_table_to_str(table, SampleClassification.PEDESTRIAN), '\n')

    table = confusion_table(confusion_matrix, SampleClassification.PEDEST_TWO.value)
    print(confusion_table_to_str(table, SampleClassification.PEDEST_TWO), '\n')

    table = confusion_table(confusion_matrix, SampleClassification.BICYCLE.value)
    print(confusion_table_to_str(table, SampleClassification.BICYCLE), '\n')

    table = confusion_table(confusion_matrix, SampleClassification.CAR.value)
    print(confusion_table_to_str(table, SampleClassification.CAR), '\n')


# def metric_f1_score(y_true, y_pred):
#     print('SHAPE:', y_true.shape, y_pred.shape)
#     # K.argmax(y_true, axis=1)
#     # K.argmax(y_pred, axis=1)

#     y_pred = K.round(y_pred)
#     tp = K.sum(K.cast(y_true*y_pred, 'float'), axis=0)
#     # tn = K.sum(K.cast((1-y_true)*(1-y_pred), 'float'), axis=0)
#     fp = K.sum(K.cast((1-y_true)*y_pred, 'float'), axis=0)
#     fn = K.sum(K.cast(y_true*(1-y_pred), 'float'), axis=0)

#     p = tp / (tp + fp + K.epsilon())
#     r = tp / (tp + fn + K.epsilon())

#     f1 = 2*p*r / (p+r+K.epsilon())
#     # f1 = tf.where(tf.is_nan(f1), tf.zeros_like(f1), f1)
#     return K.mean(f1)


def multiclass_auc_roc(true_labels, pred_probs):
    y_test = true_labels
    y_predict_proba = pred_probs

    # roc curve and auc for each class
    fpr = [0] * len(SampleClassification)
    tpr = [0] * len(SampleClassification)
    auc = [0] * len(SampleClassification)
    all_y_test_i = np.array([])
    all_y_predict_proba = np.array([])

    for i in range(len(SampleClassification)):
        y_test_i = list(map(lambda x: 1 if x == i else 0, y_test))
        fpr[i], tpr[i], _ = metrics.roc_curve(y_test_i, y_predict_proba[:, i])
        auc[i] = metrics.auc(fpr[i], tpr[i])
        all_y_test_i = np.concatenate((all_y_test_i, y_test_i))
        all_y_predict_proba = np.concatenate((all_y_predict_proba, y_predict_proba[:, i]))

    # micro-average roc curve and auc
    fpr_average, tpr_average, _ = metrics.roc_curve(all_y_test_i, all_y_predict_proba)
    auc_average = metrics.auc(fpr_average, tpr_average)

    return fpr, fpr_average, tpr, tpr_average, auc, auc_average


def graph_multiclass_auc_roc(fpr, fpr_average, tpr, tpr_average, labels):
    fig = plt.figure()

    # plot individual curves
    for i in range(len(labels)):
        auc = metrics.auc(fpr[i], tpr[i])
        plt.plot(fpr[i], tpr[i], label=f'{labels[i]} (auc = {auc:.3f})', lw=2)

    # plot average curve
    auc_average = metrics.auc(fpr_average, tpr_average)
    plt.plot(fpr_average, tpr_average, label=f'AVERAGE (auc = {auc_average:0.3f})', color='deeppink', ls=':', lw=4)

    plt.plot([0, 1], [0, 1], 'k--', lw=2)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    # plt.title('Receiver Operating Characteristic (ROC)')
    plt.legend(loc="lower right")
    plt.show()
    return fig
