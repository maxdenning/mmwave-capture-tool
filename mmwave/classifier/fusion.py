import tensorflow as tf
from mmwave.classifier import network_base, evaluation, utils, partitioned_autoencoder
from mmwave.metadata import SampleClassification
import numpy as np
from tensorflow import keras
from tensorflow.python.keras import backend as K, layers



class Fusion(network_base.TensorflowNetwork):

    def build(self, input_dims, **kwargs):

        in0 = layers.Input(shape=input_dims)


        # x = layers.Conv3D(filters=6, kernel_size=(5,5,5), activation='relu', padding='same')(in0)
        # x = layers.MaxPooling3D(pool_size=(2,2,2), strides=(2,2,2))(x)
        # x = layers.Conv3D(filters=16, kernel_size=(5,5,5), activation='relu', padding='same')(x)
        # x = layers.MaxPooling3D(pool_size=(2,2,2), strides=(2,2,2))(x)


        # b0 = layers.Lambda(lambda x: tf.slice(x, [0, 25, 0, 0, 0], [-1, 1, 32, 32, 1]))(in0)  # extract middle frame
        # b0 = layers.Reshape((32,32,1))(b0)
        # b0 = Fusion._build_spacial_unit(b0, 64)
        # # b0 = layers.Reshape((1,2,2,))(b0)
        # b0 = layers.Flatten()(b0)


        b1 = layers.Permute((3, 2, 1, 4))(in0)  # move time dimension
        b1 = Fusion._build_temporal_unit(b1, 64)
        # b1 = layers.Permute((3, 2, 1, 4))(b1)  # move time dimension
        b1 = layers.Flatten()(b1)

        # x = layers.concatenate([b0, b1])

        # x = layers.Flatten()(x)
        x = layers.Flatten()(b1)
        x = layers.Dense(units=120, activation='relu')(x)
        x = layers.Dense(units=84, activation='relu')(x)

        out0 = layers.Dense(units=len(SampleClassification), activation='softmax')(x)
        
        model = keras.models.Model(inputs=[in0], outputs=[out0])
        model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
        return model


    @staticmethod
    def _build_spacial_unit(input_layer, f):
        x = Fusion._load_encoder()(input_layer)
        r = layers.Conv2D(f, (1, 1), activation='relu', padding='same')(x)

        x = layers.Conv2D(f, (2, 2), activation='relu', padding='same')(r)
        x = layers.BatchNormalization(axis=-1)(x)
        x = layers.LeakyReLU(0)(x)

        x = layers.Conv2D(f, (2, 2), activation='relu', padding='same')(x)
        x = layers.BatchNormalization(axis=-1)(x)
        x = layers.LeakyReLU(0)(x)

        x = layers.add([x, r])
        x = layers.MaxPooling2D((2, 2))(x)
        return x


    @staticmethod
    def _build_temporal_unit(input_layer, f):
        r = layers.Conv3D(f, (1, 1, 1), activation='relu', padding='same')(input_layer)
        
        x = layers.Conv3D(f, (5, 5, 2), activation='relu', padding='same')(r)
        x = layers.BatchNormalization(axis=-1)(x)
        x = layers.LeakyReLU(0)(x)

        x = layers.Conv3D(f, (5, 5, 2), activation='relu', padding='same')(x)
        x = layers.BatchNormalization(axis=-1)(x)
        x = layers.LeakyReLU(0)(x)

        x = layers.add([x, r])
        x = layers.MaxPooling3D((2, 2, 2))(x)
        x = layers.Dropout(0.2)(x)

        # ---

        r = layers.Conv3D(f, (1, 1, 1), activation='relu', padding='same')(x)

        x = layers.Conv3D(f, (5, 5, 2), activation='relu', padding='same')(r)
        x = layers.BatchNormalization(axis=-1)(x)
        x = layers.LeakyReLU(0)(x)

        x = layers.Conv3D(f, (5, 5, 2), activation='relu', padding='same')(x)
        x = layers.BatchNormalization(axis=-1)(x)
        x = layers.LeakyReLU(0)(x)

        x = layers.add([x, r])
        x = layers.MaxPooling3D((2, 2, 2))(x)
        x = layers.Dropout(0.2)(x)

        # ---

        r = layers.Conv3D(f, (1, 1, 1), activation='relu', padding='same')(x)

        x = layers.Conv3D(f, (5, 5, 2), activation='relu', padding='same')(r)
        x = layers.BatchNormalization(axis=-1)(x)
        x = layers.LeakyReLU(0)(x)

        x = layers.Conv3D(f, (5, 5, 2), activation='relu', padding='same')(x)
        x = layers.BatchNormalization(axis=-1)(x)
        x = layers.LeakyReLU(0)(x)

        x = layers.add([x, r])
        x = layers.MaxPooling3D((2, 2, 2))(x)

        x = layers.MaxPooling3D((2, 2, 2))(x)
        x = layers.Dropout(0.2)(x)
        return x



    @staticmethod
    def _load_encoder():
        path = 'dump\\models\\PartitionedAutoencoder\\19-04-07_14-33-10\\00\\weights.h5'
        encoder_model = partitioned_autoencoder.PartitionedAutoencoder(load=path, load_model=False, verbose=True, input_dims=(32,32,1)).export_encoder_model()
        for layer in encoder_model.layers:
            layer.trainable=False
        encoder_model.trainable = False
        return encoder_model


    # @staticmethod
    # def preprocess(train_samples, train_meta, validation_samples, validation_meta, test_samples, test_meta, batch_size, **kwargs):
    #     utils.z_score_normalise_sets(train_samples, validation_samples, test_samples)
    #     train_labels = utils.to_categorical(utils.sample_labels(train_meta))
    #     validation_labels = utils.to_categorical(utils.sample_labels(validation_meta))
    #     test_labels = utils.to_categorical(utils.sample_labels(test_meta))



    #     # train_samples = np.moveaxis(train_samples, 1, 3)
    #     # validation_samples = np.moveaxis(validation_samples, 1, 3)
    #     # test_samples = np.moveaxis(test_samples, 1, 3)

    #     # a = train_samples[1]
    #     # b = time_a[1]
    #     # c = time_b[1]
    #     # utils.visualise_multi([10,12,14,16,18,20,22,24,26], [0]*50, a, b, c)
    #     # exit(1)

    #     return \
    #         (train_samples, train_labels), train_meta, \
    #         (validation_samples, validation_labels), validation_meta, \
    #         (test_samples, test_labels), test_meta


    def evaluate(self, *args, **kwargs):
        pass
