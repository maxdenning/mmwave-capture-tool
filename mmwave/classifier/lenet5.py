import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from mmwave.classifier import network_base, evaluation, utils
from mmwave.metadata import SampleClassification
from tensorflow import keras
from tensorflow import metrics
# from tensorflow.python.keras.preprocessing.image import ImageDataGenerator
from tensorflow.python.keras import backend as K, layers


class LeNet5(network_base.TensorflowNetwork):
    # Based on:
    # https://engmrk.com/lenet-5-a-classic-cnn-architecture/


    def build(self, input_dims, **kwargs):
        in0 = layers.Input(shape=input_dims)
        x = layers.Conv2D(filters=6, kernel_size=(5, 5), strides=(1, 1), activation='relu')(in0)
        x = layers.AveragePooling2D(pool_size=(2, 2), strides=(2, 2))(x)
        x = layers.Conv2D(filters=16, kernel_size=(5, 5), strides=(1, 1), activation='relu')(x)
        x = layers.AveragePooling2D(pool_size=(2, 2), strides=(2, 2))(x)
        x = layers.Flatten()(x)
        x = layers.Dense(units=120, activation='relu')(x)
        x = layers.Dense(units=84, activation='relu')(x)
        out0 = layers.Dense(units=len(SampleClassification), activation='softmax')(x)

        model = keras.models.Model(inputs=[in0], outputs=[out0])
        model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
        return model


    def evaluate(self, x, y, meta, batch_size, **kwargs):
        loss, acc = self.model.evaluate(x, y, batch_size=batch_size, verbose=0)
        print(f'Loss: {loss:.5f}\tAccuracy: {acc:.5f}')

    
    # success by range-bin plot
    # def test(self, x, y, meta, batch_size, **kwargs):
    #     super().test(x, y, meta, batch_size, **kwargs)

    #     y_true = utils.from_categorical(y)
    #     y_pred = np.argmax(self.model.predict(x, batch_size), axis=1)
    #     success, failure = [], []
    #     for i in range(len(y_true)):
    #         if y_true[i] == y_pred[i]:
    #             success.append(meta[i])
    #         else:
    #             failure.append(meta[i])
    #     plt.hist([m['centre_range_bin'] for m in success], bins=40, label='Correct Predictions')
    #     plt.hist([m['centre_range_bin'] for m in failure], bins=40, label='False Predictions')
    #     plt.show()
