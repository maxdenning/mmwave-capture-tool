import abc
import numpy as np
import os
from mmwave.classifier import evaluation, utils
from mmwave.metadata import SampleClassification
from tensorflow import keras


class NetworkBase(abc.ABC): 
    def __init__(self, load=None, verbose=False, **kwargs):
        self.verbose = verbose
        self.model = self.build(**kwargs)
        if load:
            self.load(load, kwargs.get('load_model', True))

    @staticmethod
    def preprocess(train_samples, train_meta, validation_samples, validation_meta, test_samples, test_meta, **kwargs):
        utils.z_score_normalise_sets(train_samples, validation_samples, test_samples)
        # utils.minmax_normalise_sets(train_samples, validation_samples, test_samples)
        train_labels = utils.to_categorical(utils.sample_labels(train_meta))
        validation_labels = utils.to_categorical(utils.sample_labels(validation_meta))
        test_labels = utils.to_categorical(utils.sample_labels(test_meta))
        return \
            (train_samples, train_labels), train_meta, \
            (validation_samples, validation_labels), validation_meta, \
            (test_samples, test_labels), test_meta

    @abc.abstractmethod
    def build(self, **kwargs):
        pass

    @abc.abstractmethod
    def train(self, train_x, train_y, validation_x, validation_y, epochs, **kwargs):
        pass

    @abc.abstractmethod
    def evaluate(self, *args, **kwargs):
        pass

    @abc.abstractmethod
    def test(self, test_x, test_y, test_meta, **kwargs):
        pass

    @abc.abstractmethod
    def predict(self, x, **kwargs):
        pass

    @abc.abstractmethod
    def load(self, path):
        pass

    @abc.abstractmethod
    def save(self, path):
        pass

    @abc.abstractmethod
    def summary(self):
        pass


class TensorflowNetwork(NetworkBase):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if not hasattr(self, 'initial_epoch'):
            self.initial_epoch = 0

        self.callbacks = []
        # add tensorboard callbacks
        if 'logs' in kwargs and kwargs['logs']:
            self.callbacks.append(keras.callbacks.TensorBoard(
                log_dir=kwargs['logs'], histogram_freq=0, batch_size=16, write_graph=False, write_grads=False, write_images=False))

        # add checkpoint model saving callbacks
        if 'checkpoints' in kwargs and kwargs['checkpoints']:
            checkpoint_path = os.path.join(kwargs['checkpoints'], 'checkpoint-{epoch:06d}-{val_loss:06.3f}.model.h5')
            self.callbacks.append(keras.callbacks.ModelCheckpoint(
                filepath=checkpoint_path, monitor='val_loss', save_weights_only=False, period=25, verbose=self.verbose))


    def train(self, train_x, train_y, validation_x, validation_y, epochs, batch_size, **kwargs):
        self.model.fit(
            train_x, train_y, validation_data=(validation_x, validation_y), epochs=epochs, batch_size=batch_size,
            shuffle=True, initial_epoch=self.initial_epoch, callbacks=self.callbacks, verbose=2)


    def evaluate(self, x, y, meta, **kwargs):
        print(self.model.evaluate(x, y, batch_size=kwargs.get('batch_size', None), verbose=self.verbose))


    def test(self, test_x, test_y, test_meta, batch_size=8, save_results=None, **kwargs):
        predictions = self.model.predict(test_x, batch_size=batch_size, verbose=0)
        evaluation.classification_report(test_y, predictions)
        if save_results:
            print(f'Saving prediction results: {save_results}')
            np.save(save_results, np.stack((test_y, predictions)))


    def predict(self, x, **kwargs):
        labels, confidences = utils.from_categorical(self.model.predict(x, batch_size=1, verbose=1), confidence=True)
        class_label = [SampleClassification(v) for v in labels]
        return list(zip(class_label, confidences))


    def load(self, path, model=True):
        if self.verbose:
            print(f'Loading {"model" if model else "weights"}: {path}')
            if model:
                print('WARNING: loaded model will overwrite existing structure!')

        # attempt to get initial epoch number
        filename = os.path.basename(path)
        if filename[:10] == 'checkpoint' and filename[-9:] == '.model.h5':
            self.initial_epoch = int(filename[11:17])
            if self.verbose:
                print(f'Detected epoch in loaded filename: initial_epoch = {self.initial_epoch}')

        if model:
            self.model = keras.models.load_model(path, custom_objects=self.custom_objects)#, custom_objects={'metric_f1_score': evaluation.metric_f1_score})
        else:
            self.model.load_weights(path)


    def save(self, path, model=True):
        if self.verbose:
            print(f'Saving {"model" if model else "weights"}: {path}')
        if model:
            self.model.save(path)
        else:
            self.model.save_weights(path)


    def summary(self):
        print(self.model.summary())


    @property
    def custom_objects(self):
        return {}
