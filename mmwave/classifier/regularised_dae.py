import matplotlib.pyplot as plt
import numpy as np
import random
import tensorflow as tf
from mmwave.classifier import network_base, utils, encoder_decoder
from tensorflow import keras
from tensorflow.python.keras import activations, layers, backend as K

from functools import partial, update_wrapper

from scipy.stats import describe


class RegularisedDAE(network_base.TensorflowNetwork):
    # Based on:
    # https://arxiv.org/pdf/1509.05982.pdf
    # https://github.com/danstowell/autoencoder-specgram


    def build(self, input_dims, **kwargs):
        # self.batch_size = batch_size
        # self.latents = 1024
        # self.background_ratio = background


        # X
        in0 = layers.Input(shape=input_dims)

        # # encoder -> f(X)
        # x = layers.Conv1D(32, 9, activation='relu', padding='same', use_bias=False)(x)
        # encoder = layers.MaxPooling1D(16)(x)  # TODO: fix this
        # # decoder -> g(f(X))
        # x = layers.UpSampling1D(16)(encoder)  # TODO: fix this
        # decoder = layers.Conv1D(32, 9, activation='sigmoid', padding='same', use_bias=False)(x)
        # # output -> f(X) concat g(f(X))
        # out0 = decoder
        # # out0 = layers.concatenate([encoder, decoder], axis=1)

        
        # x = layers.BatchNormalization(axis=2)(in0)


        # x = layers.ZeroPadding2D(padding=(0, 4))(in0)
        # x = layers.Conv2D(32, (32, 9), activation='relu', use_bias=False)(x)  # TODO: disable bias values?
        # # x = layers.LeakyReLU(alpha=0.3)(x)
        # x = layers.Permute((3, 2, 1))(x)

        # x = layers.MaxPool2D((1, 16))(x)
        # x = layers.UpSampling2D((1, 16))(x)
        
        # x = layers.ZeroPadding2D(padding=(0, 4))(x)
        # x = layers.Conv2D(32, (32, 9), activation='relu', use_bias=False)(x)
        # # x = layers.LeakyReLU(alpha=0.3)(x)
        # x = layers.Permute((3, 2, 1))(x)


        # batch, latents, background = 16, 32, 0.25
        # x = layers.Flatten()(in0)
        # x = layers.Dense(2048, activation='relu', use_bias=True)(x)
        # x = layers.Dense(1024, activation='relu', use_bias=True)(x)
        # x = layers.Dense(latents, activation='linear', use_bias=False)(x)
        # x = MaskBackgroundLayer(batch, latents, background)(x)
        # x = layers.Dense(1024, activation='relu', use_bias=True)(x)
        # x = layers.Dense(2048, activation='relu', use_bias=True)(x)
        # x = layers.Dense(1024, activation='linear', use_bias=False)(x)
        # x = layers.Reshape((32, 32, 1))(x)


        batch, latents, background = 16, 32, 0.25
        x = layers.Conv2D(128, (3,3), activation='relu', padding='same')(in0)
        x = layers.MaxPool2D((2,2))(x)
        x = layers.Conv2D(128, (3,3), activation='relu', padding='same')(x)
        x = layers.MaxPool2D((2,2))(x)
        x = layers.Conv2D(128, (3,3), activation='relu', padding='same')(x)
        x = layers.MaxPool2D((2,2))(x)

        # x = layers.Conv2D(latents, (1,1), activation='linear', padding='same', use_bias=False)(x)
        # x = MaskBackgroundConvLayer(batch, latents, background)(x)

        x = layers.Flatten()(x)
        x = layers.Dense(latents, activation='linear', use_bias=False)(x)
        x = MaskBackgroundLayer(batch, latents, background)(x)
        x = layers.Dense(4*4*latents, activation='linear', use_bias=False)(x)
        x = layers.Reshape((4, 4, latents))(x)

        
        x = layers.UpSampling2D((2,2))(x)
        x = layers.Conv2D(128, (3,3), activation='relu', padding='same')(x)
        x = layers.UpSampling2D((2,2))(x)
        x = layers.Conv2D(128, (3,3), activation='relu', padding='same')(x)
        x = layers.UpSampling2D((2,2))(x)
        x = layers.Conv2D(128, (3,3), activation='relu', padding='same')(x)

        x = layers.Conv2D(1, (1,1), activation='sigmoid', padding='same')(x)



        out0 = x
        model = keras.models.Model(inputs=[in0], outputs=[out0])

        self.y_labels = np.array(([1] * 4) + ([0] * 12), dtype=int)
        # print(self.y_labels, 'Labels only valid for training set due to batch organisation')
        # fn_loss = partial(custom_loss, encoder=model.get_layer(index=4), lam=0.75, y_labels=self.y_labels)
        fn_loss = partial(loss_dense, encoder=model.get_layer(index=9), lam=0.75, y_labels=self.y_labels, latents=latents, background_ratio=background)
        # fn_loss = partial(loss_conv, encoder=model.get_layer(index=8), lam=0.75, y_labels=self.y_labels, latents=latents, background_ratio=background)

        update_wrapper(fn_loss, loss_dense)
        # update_wrapper(fn_loss, loss_conv)

        # model.compile(optimizer='adadelta', loss='binary_crossentropy', metrics=['accuracy'])
        model.compile(optimizer='adadelta', loss=fn_loss, metrics=['accuracy'])
        return model


    @staticmethod
    def preprocess(train_samples, train_meta, validation_samples, validation_meta, test_samples, test_meta, batch_size, **kwargs):
        if batch_size < 16:
            raise ValueError(f'Batch size too small: {batch_size} < 16')

        # train_samples = np.concatenate((train_samples, test_samples), axis=0)   # TODO: change this?
        # train_meta = np.concatenate((train_meta, test_meta), axis=0)

        train_samples, train_meta = organise_batches(train_samples, train_meta, batch_size, 0.25)
        validation_samples, validation_meta = organise_batches(validation_samples, validation_meta, batch_size, 0.25)
        # t_samples = t_samples#.squeeze()#.transpose((0, 2, 1))
        # v_samples = v_samples#.squeeze()#.transpose((0, 2, 1))

        print([m['class'] for m in train_meta[:16]], [m['class'] for m in train_meta[-16:]])
        print([m['class'] for m in validation_meta[:16]], [m['class'] for m in validation_meta[-16:]])

        # validation_samples = validation_samples[:-(len(validation_samples) % batch_size)]
        # validation_meta = validation_meta[:-(len(validation_meta) % batch_size)]
        test_samples = test_samples[:-(len(test_samples) % batch_size)]
        test_meta = test_meta[:-(len(test_meta) % batch_size)]


        # n = random.randint(0, len(train_samples)-17)
        # encoder_decoder.visualise_encoder_decoder(list(range(n, n+16)), samples, predicted_before, predicted_after, [m['class'] for m in meta])


        utils.minmax_normalise_sets(train_samples, validation_samples, test_samples)
        # utils.z_score_normalise_sets(train_samples, validation_samples, test_samples)
        return \
            (train_samples,), train_meta, \
            (validation_samples,), validation_meta, \
            (test_samples,), test_meta                    # TODO: ???

        # t_labels = [int(m['class'] == 0) for m in t_meta]  # 1 if noise only, 0 if not
        # v_labels = [int(m['class'] == 0) for m in v_meta]  #TODO: keras.utils.to_categorical

        # # t_comp = np.zeros((t_samples.shape[0], t_samples.shape[1], t_samples.shape[2]+1))
        # # t_comp[:, :, 1:] = t_samples
        # # t_comp[:,0,0] = t_labels
        # t_comp = np.zeros((t_samples.shape[0], t_samples.shape[1], t_samples.shape[2], t_samples.shape[3]+1))
        # t_comp[:, :, :, 1:] = t_samples
        # t_comp[:, 0, 0, 0] = t_labels

        # # v_comp = np.zeros((v_samples.shape[0], v_samples.shape[1], v_samples.shape[2]+1))
        # # v_comp[:,:,1:] = v_samples
        # # v_comp[:,0,0] = v_labels
        # v_comp = np.zeros((v_samples.shape[0], v_samples.shape[1], v_samples.shape[2], v_samples.shape[3]+1))
        # v_comp[:, :, :, 1:] = v_samples
        # v_comp[:, 0, 0, 0] = v_labels

        # return (t_samples, t_comp), t_meta, (v_samples, v_comp), v_meta


    def train(self, train_x, validation_x, epochs, batch_size, **kwargs):
        if len(self.y_labels) != batch_size:
            raise ValueError('t_labels length != batch size')

        self.model.fit(
            train_x, train_x, validation_data=(validation_x, validation_x), epochs=epochs, batch_size=batch_size,
            shuffle=False, initial_epoch=self.initial_epoch, callbacks=self.callbacks, verbose=2)


    def evaluate(self, x, meta, **kwargs):
        pass


    def test(self, x, meta, batch_size, **kwargs):
        # predicted = self.model.predict(samples)
        # encoded_samples = predicted[:, :, :32]
        # decoded_samples = predicted[:, :, 32:]
        # encoded_samples = predicted[:, :2, :]
        # decoded_samples = predicted[:, 2:, :]

        # print(predicted.shape)
        # print(encoded_samples.shape, encoded_samples.min(), encoded_samples.max())
        # print(decoded_samples.shape, decoded_samples.min(), decoded_samples.max())

        # c_samples = labels[:, :, :, 1:]
        # c_labels = labels[:,0 ,0, 0]

        # encoder_decoder.visualise_encoder_decoder(10, c_samples, decoded_samples, encoded_samples, c_labels)
        # encoder_decoder.visualise_encoder_decoder(10, c_samples, predicted, predicted, c_labels)
        # encoder_decoder.visualise_encoder_decoder(10, samples, predicted, predicted, labels)
        # encoder_decoder.visualise_encoder_decoder(list(range(10)), samples, predicted, predicted, labels)

        

        predicted_before = self.model.predict(x, batch_size=batch_size)
        self.model.get_layer(index=9).switch_no_background()
        predicted_after = self.model.predict(x, batch_size=batch_size)
        self.model.get_layer(index=9).switch_only_background()
        predicted_after_noise = self.model.predict(x, batch_size=batch_size)

        print(describe(x, axis=None))
        print(describe(predicted_before, axis=None))
        print(describe(predicted_after, axis=None))
        print(describe(predicted_after_noise, axis=None))
        
        # encoder_decoder.visualise_encoder_decoder(10, x, predicted_before, predicted_after, [m['class'] for m in meta])
        # encoder_decoder.visualise_encoder_decoder(10, x, predicted_after, predicted_after_noise, [m['class'] for m in meta])
        utils.visualise_multi(10, utils.sample_labels(meta), x, predicted_before, predicted_after, predicted_after_noise)


    def mask_background(self):
        # set background latents to zero
        
        # mask_values = np.zeros((16, 32), dtype=int)
        # mask_values[:, :24] = 1
        mask = np.zeros((32), dtype=int)
        mask[:24] = 1

        # output_layer = self.model.get_layer(index=7)
        output_layer = self.model.get_layer(index=2)
        print(mask.shape, output_layer.name)

        weights = output_layer.get_weights()
        mask = mask[np.newaxis, np.newaxis, np.newaxis, :]
        masked_weights = mask * weights[0]
        
        print(mask.shape, weights[0].shape, masked_weights.shape)

        weights[0] = masked_weights

        # os0 = output_layer.get_output_shape_at(0)
        # om0 = output_layer.get_output_mask_at(0)
        # os1 = output_layer.get_output_shape_at(1)
        # os2 = output_layer.get_output_shape_at(2)
        # [0] = weights, [1] = biases
        # print(len(weights), weights[0].shape, weights[1].shape, os0, om0)

        self.model.get_layer(index=7).set_weights(weights)




class MaskBackgroundLayer(layers.Layer):
    def __init__(self, batch, latents, background_ratio):
        super().__init__()
        self.batch = batch
        self.latents = latents
        self.background_ratio = background_ratio
        self.k_mask = K.variable(np.ones((self.batch, self.latents), dtype=float))
        print('MASK:', self.k_mask.shape, int(latents * background_ratio), self.k_mask.name)

    def call(self, input_tensor):
        res = self.k_mask * input_tensor
        print('CALL:', input_tensor.shape, (self.k_mask * input_tensor).shape, self.k_mask.name, input_tensor.name, res.name)
        return res

    def switch_no_background(self):
        # select background latents and zero them
        mask = np.ones((self.batch, self.latents), dtype=float)
        # mask[:, -int(self.latents * self.background_ratio):] = 0
        mask[:, :int(self.latents * self.background_ratio)] = 0
        
        # mask = np.zeros((self.batch, self.latents), dtype=int)

        K.set_value(self.k_mask, mask)
        print('SWITCH NO', f'{int(self.latents * self.background_ratio)}')

    def switch_only_background(self):
        # select foreground latents and zero them
        mask = np.ones((self.batch, self.latents), dtype=float)
        mask[:, int(self.latents * self.background_ratio):] = 0
        K.set_value(self.k_mask, mask)
        print('SWITCH ONLY', f'{int(self.latents * self.background_ratio)}')


class MaskBackgroundConvLayer(MaskBackgroundLayer):
    def __init__(self, batch, latents, background_ratio):
        super().__init__(batch, latents, background_ratio)

        self.k_mask = K.variable(np.ones((self.batch, 1, 1, self.latents), dtype=int))
        print('MASK:', self.k_mask.shape, int(latents * background_ratio))

    def switch_no_background(self):
        # select background latents and zero them
        mask = np.ones((self.batch, 1, 1, self.latents), dtype=int)
        # mask[:, :, :, -int(self.latents * self.background_ratio):] = 0
        mask[:, :, :, :int(self.latents * self.background_ratio)] = 0
        
        # mask = np.zeros((self.batch, self.latents), dtype=int)

        K.set_value(self.k_mask, mask)
        print('CONV SWITCH NO', f'-{int(self.latents * self.background_ratio)}:', np.mean(mask))

    def switch_only_background(self):
        # select foreground latents and zero them
        mask = np.ones((self.batch, 1, 1, self.latents), dtype=int)
        mask[:, :, :, int(self.latents * self.background_ratio):] = 0
        K.set_value(self.k_mask, mask)
        print('CONV SWITCH ONLY', f'{int(self.latents * self.background_ratio)}:', np.mean(mask))




def loss_conv(y_true, y_pred, encoder=None, lam=0.0, y_labels=None, latents=0, background_ratio=0.0):
    y_true_labels = K.variable(y_labels)
    y_true_samples = y_true
    y_pred_encoded = encoder.output
    y_pred_decoded = y_pred
    lamv = K.variable(lam)
    batch = len(y_labels)

    print(type(encoder), encoder.name)
    print(y_true.shape, y_true_labels.shape, y_true_samples.shape, y_pred.shape, y_pred_encoded.shape, y_pred_decoded.shape)
    print('lam:', lam, 'batch:', batch)

    # select foreground latents for noise samples and set to one - so they are penalised
    # mask_values = np.zeros((latents), dtype=int)
    # mask_values[:-int(latents * background_ratio)] = 1
    mask_values = np.zeros((batch, 1, 1, latents), dtype=int)
    # mask_values[:, :, :, :-int(latents * background_ratio)] = 1
    mask_values[:, :, :, int(latents * background_ratio):] = 1

    mask = K.variable(mask_values)
    print('mask:', mask.shape, f':-{int(latents * background_ratio)}')

    t1 = K.pow(Knorm(y_true_samples - y_pred_decoded, axis=[1,2,3]), 2)
    print('t1:', t1.shape, t1.dtype)

    t2 = (lamv * y_true_labels) / K.mean(mask)
    print('t2:', t2.shape, t2.dtype)

    # t3 = K.pow(Knorm(mask[np.newaxis, np.newaxis, np.newaxis, :] * y_pred_encoded, axis=[1,2,3]), 2)
    t3 = K.pow(Knorm(mask * y_pred_encoded, axis=[1,2,3]), 2)
    print('t3:', t3.shape, t3.dtype)

    res = t1 + (t2 * t3)
    print('res:', res.shape)
    res = K.reshape(res, (16,-1))
    print('res:', res.shape)

    return res


def loss_dense(y_true, y_pred, encoder=None, lam=0.0, y_labels=None, latents=0, background_ratio=0.0):
    y_true_labels = K.variable(y_labels)
    y_true_samples = y_true
    y_pred_encoded = encoder.output
    y_pred_decoded = y_pred
    print(type(encoder), encoder.name)
    print(y_true.shape, y_true_labels.shape, y_true_samples.shape, y_pred.shape, y_pred_encoded.shape, y_pred_decoded.shape)

    lamv = K.constant(lam)
    print('lam:', lam, lamv.name)

    batch = len(y_labels)
    # select foreground latents for noise samples and set to one - so they are penalised
    # mask_values = np.zeros((batch, latents), dtype=int)
    # mask_values[:int(batch * 0.25), :-int(latents * background_ratio)] = 1

    # mask_values = np.zeros((latents), dtype=int)
    mask_values = np.zeros((batch, latents), dtype=float)
    # mask_values[:-int(latents * background_ratio)] = 1
    # mask_values[int(latents * background_ratio):] = 1  # latents = background + foreground
    mask_values[:, int(latents * background_ratio):] = 1  # latents = background + foreground


    mask = K.constant(mask_values)
    print('mask:', mask.shape, f':{int(batch * 0.25)}, :-{int(latents * background_ratio)}', mask.name)


    # res = keras.losses.binary_crossentropy(y_true_samples, y_pred_decoded)
    # print('desired output shape:', res.shape)  # res.shape -> (batch, feature vector)
    # res = keras.losses.mean_squared_error(y_true_samples, y_pred_decoded)
    # print('desired output shape:', res.shape)  # res.shape -> (batch, feature vector)


    t1 = K.pow(Knorm(y_true_samples - y_pred_decoded, axis=[1,2,3]), 2)
    print('t1:', t1.shape, t1.dtype, t1.name)

    t2 = (lamv * y_true_labels) / K.mean(mask)
    print('t2:', t2.shape, t2.dtype, t2.name)

    t3 = K.pow(Knorm(mask * y_pred_encoded, axis=-1), 2)
    print('t3:', t3.shape, t3.dtype, t3.name)

    # t3 = t3.reshape(32,32,1)
    # print('t3:', t3.shape, t3.dtype)

    res = t1 + (t2 * t3)
    print('res:', res.shape, res.name)
    res = K.reshape(res, (-1,1,1))
    print('res:', res.shape, res.name)


    # res = K.sum(res)
    # print(res.shape)

    return res







def custom_loss(y_true, y_pred, encoder=None, lam=0, y_labels=None):
    # y_true.shape -> (batch, ,)
    # y_pred.shape -> (batch, feature vectors, filters)

    y_true_labels = K.variable(y_labels) #y_true[:,0,0,0]
    y_true_samples = y_true #[:, :, :, 1:]

    # y_pred_encoded = y_pred[:, :, :32]  # concat -> (x, y1) + (x, y2) = (x, y1 + y2)
    # y_pred_decoded = y_pred[:, :, 32:]
    
    # y_pred_encoded = y_pred[:, :2, :]  # concat -> (x1, y) + (x2, y) = (x1 + x2, y)
    # y_pred_decoded = y_pred[:, 2:, :]

    y_pred_encoded = encoder.output
    y_pred_decoded = y_pred

    print(type(encoder), encoder.name)
    print(y_true.shape, y_true_labels.shape, y_true_samples.shape, y_pred.shape, y_pred_encoded.shape, y_pred_decoded.shape)

    # (16,32,32)

    res = keras.losses.categorical_crossentropy(y_true_samples, y_pred_decoded)
    print('desired output shape:', res.shape)  # res.shape -> (batch, feature vector)


    BATCH = 16
    FILTERS = 32
    lamv = K.variable(lam)  # regularisation coeff

    # mask_values = np.zeros((16,32,32), dtype=int)
    # mask_values[:, :4, :24] = 1

    mask_values = np.zeros((BATCH, FILTERS), dtype=int)
    mask_values[:, :int(FILTERS * 0.75)] = 1  # :4, :24
    mask = K.variable(mask_values)  # TODO: fix mask

    print('m0:', mask.shape)

    # mask = K.placeholder(ndim=3, dtype='int32') # shape=(y_pred_encoded.shape))
    # K.set_value(mask, np.zeros((16,32,32), dtype=np.int32))


    # np.power(np.abs(y_true_samples - y_pred_decoded), 2) + ((lam * y_true_labels)/np.mean(mask)) * np.power(np.abs(np.multiply(mask, y_pred_encoded)), 2)



    t1 = K.pow(Knorm(y_true_samples - y_pred_decoded), 2)

    # t10 = Knorm(y_true_samples - y_pred_decoded)
    # t1 = K.batch_dot(t10, t10, axes=[1, 2])
    print('t1:', t1.shape, t1.dtype)
    # return t1

    # because of fixed batch organisation this always evaluates to the same thing
    #   lam * label / mean(mask) = [>0, >0, >0, >0, 0, ..., 0]

    t2 = (lamv * y_true_labels) / K.mean(mask)
    print('t2:', t2.shape, t2.dtype)

    # t3 = K.pow(Knorm(np.multiply(mask, y_pred_encoded)), 2)
    # t3 = K.pow(Knorm(mask[:, np.newaxis, np.newaxis, :] * y_pred_encoded), 2)
    t3 = K.pow(Knorm(mask[:, :, np.newaxis, np.newaxis] * y_pred_encoded), 2)
    print('t3:', t3.shape, t3.dtype)



    # t4 = K.concatenate([t2[:, np.newaxis] * t3, K.variable(np.zeros((16, 30)))], axis=1)
    # print('t4:', t4.shape)

    # res = t1 + t4
    # res1 = K.concatenate([t3, res], axis=1)
    # print('res:', res.shape)
    # print('res1:', res1.shape)
    # return res1



    # res = t1 + (t2[:, np.newaxis] * t3)
    res = t1 + (t2 * t3)
    print(res.shape)
    res = K.sum(res)
    print(res.shape)

    return res  # must be of size (batch, feature vector)




def Knorm(a, axis):
    return tf.sqrt(K.sum(K.square(a), axis=axis))




def organise_batches(samples, meta, batch_size, noise_prop):
    # split indices into all zero and all non-zero samples
    t_labels = np.array([m['class'] for m in meta], dtype=int)
    si = np.argsort(t_labels)
    for split in range(len(si)):
        if t_labels[si][split] > 0:
            break
    zero, other = si[:split], si[split:]
    

    # organise into batches of zero and non-zero parts
    n_noise_only = int(batch_size * noise_prop)
    batched_idxs_len_upper = int(len(zero) * batch_size/n_noise_only)
    batched_idxs = np.zeros((batched_idxs_len_upper - (batched_idxs_len_upper % batch_size)), dtype=int)
    # batched_idxs = np.zeros((int(len(zero) * batch_size/n_noise_only)), dtype=int)
    for i in range(0, len(batched_idxs), batch_size):
        selected = random.sample(range(len(zero)), n_noise_only)
        batched_idxs[i:i+n_noise_only] = zero[selected]
        zero = np.delete(zero, selected)

        selected = random.sample(range(len(other)), batch_size - n_noise_only)
        batched_idxs[i+n_noise_only:i+batch_size] = other[selected]
        other = np.delete(other, selected)

    return samples[batched_idxs], [meta[i] for i in batched_idxs]
