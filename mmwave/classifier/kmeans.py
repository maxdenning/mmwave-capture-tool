from sklearn.cluster import KMeans
from sklearn.metrics import accuracy_score
import numpy as np

from mmwave.classifier import evaluation
from tensorflow import keras



def cluster(samples, labels):
    samples = samples.reshape(samples.shape[0], -1)
    labels = np.argmax(labels, axis=1)

    n_clusters = 5 # len(np.unique(labels))

    print(samples.shape, labels.shape, n_clusters)

    # Runs in parallel 4 CPUs
    kmeans = KMeans(n_clusters=n_clusters, n_init=20, n_jobs=4)

    # Train K-Means.
    y_pred_kmeans = kmeans.fit_predict(samples)

    # Evaluate the K-Means clustering accuracy.
    print(accuracy_score(labels, y_pred_kmeans))

    actuals = keras.utils.to_categorical(labels)
    predictions = keras.utils.to_categorical(np.array(y_pred_kmeans, dtype=np.uint8))

    confusion_matrix = evaluation.confusion_matrix(actuals, predictions)
    print(evaluation.confusion_matrix_to_str(confusion_matrix))


from minisom import MiniSom
import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn.preprocessing import scale
import os
import random

def som_clustering(samples, labels):
    idxs = random.sample(range(len(samples)), 2000)
    samples = samples.reshape((samples.shape[0], -1))[idxs]
    labels = np.argmax(labels, axis=1)[idxs]

    # cosine dist vs euclidean dist for high dimensional data ?

    n_cells = 5 * np.sqrt(samples.shape[0] * samples.shape[1])
    x = int(np.ceil(np.sqrt(n_cells * 2)))  # n_cells = x * y = x * x/2 -> solve for x
    y = int(np.ceil(x / 2))  # one half rule

    print(samples.shape, labels.shape, n_cells, x, y)

    som = MiniSom(x, y, samples.shape[1], sigma=4, learning_rate=0.5, neighborhood_function='triangle')
    som.pca_weights_init(samples)

    print('training')
    som.train_random(samples, 5000)


    plt.figure(figsize=(8, 8))
    wmap = {}
    # im = 0
    for i in range(len(labels)):
        w = som.winner(samples[i])
        wmap[w] = i
        plt.text(w[0]+0.5,  w[1]+0.5,  str(labels[i]), color=plt.cm.rainbow(labels[i]/5)) #, fontdict={'weight': 'bold',  'size': 11})

    # for x, t in zip(samples, labels):
    #     w = som.winner(x)
    #     wmap[w] = im
    #     plt.text(w[0]+0.5,  w[1]+0.5,  str(t), color=plt.cm.rainbow(t / 10.))# , fontdict={'weight': 'bold',  'size': 11})
    #     im = im + 1
    plt.axis([0, som.get_weights().shape[0], 0,  som.get_weights().shape[1]])
    plt.savefig(os.path.join('dump','som_traf.png'))
    plt.show()





def som():
    # load the digits dataset from scikit-learn
    # 901 samples,  about 180 samples per class
    # the digits represented 0, 1, 2, 3, 4

    digits = datasets.load_digits(n_class=10)
    data = digits.data  # matrix where each row is a vector that represent a digit.
    data = scale(data)
    num = digits.target  # num[i] is the digit represented by data[i]


    som = MiniSom(30, 30, 64, sigma=4, learning_rate=0.5, neighborhood_function='triangle')
    som.pca_weights_init(data)
    

    print("Training...")
    som.train_random(data, 5000)  # random training
    print("\n...ready!")

    plt.figure(figsize=(8, 8))
    wmap = {}
    im = 0
    for x, t in zip(data, num):  # scatterplot
        w = som.winner(x)
        wmap[w] = im
        plt. text(w[0]+.5,  w[1]+.5,  str(t),
                color=plt.cm.rainbow(t / 10.), fontdict={'weight': 'bold',  'size': 11})
        im = im + 1
    plt.axis([0, som.get_weights().shape[0], 0,  som.get_weights().shape[1]])
    plt.savefig(os.path.join('dump','som_digts.png'))
    plt.show()






