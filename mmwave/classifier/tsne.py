from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import os
import pickle
import random
from functools import partial
from mmwave.classifier import network_base, utils
from mmwave.metadata import SampleClassification
from sklearn.manifold import TSNE

class T_SNE(network_base.NetworkBase):

    def build(self, input_dims, **kwargs):
        model = partial(TSNE, n_components=2, verbose=1, perplexity=40)
        return model


    @staticmethod
    def preprocess(train_samples, train_meta, validation_samples, validation_meta, test_samples, test_meta, n_samples, fclass=None, **kwargs):
        all_samples = np.concatenate([train_samples, validation_samples, test_samples], axis=0)
        all_meta = train_meta + validation_meta + test_meta
        utils.z_score_normalise_sets(all_samples)  # normalise before to avoid contamination

        # filter by class
        if fclass is not None:
            class_idxs = [i for i in range(len(all_meta)) if all_meta[i]['class'] == fclass.value]
            all_samples = all_samples[class_idxs]
            all_meta = [all_meta[i] for i in class_idxs]

        # sample down to n_samples
        if n_samples > 0 and n_samples < len(all_samples):
            selected_idxs = random.sample(range(len(all_samples)), n_samples)
            samples = all_samples[selected_idxs]
            meta = [all_meta[i] for i in selected_idxs]
        else:
            samples = all_samples
            meta = all_meta

        samples = samples.reshape((len(samples), -1))  # flatten all samples
        return (samples,), meta, (samples,), meta, (samples,), meta


    def train(self, train_x, validation_x, epochs, **kwargs):
        self.model = self.model(n_iter=epochs)
        # self.model.fit(train_x)  # ?? no standalone transform method
        

    def test(self, test_x, test_meta, **kwargs):
        results = self.model.fit_transform(test_x)
        plt.figure(figsize=(8, 8))
        # plt.gca().set_facecolor('xkcd:black')

        for i in range(len(results)):

            participants = test_meta[i]['participants'] if test_meta[i]['participants'] else [0]
            label = str(test_meta[i]['class']) #+ '.' + test_meta[i]['scenario'][2:5].replace('.', '') + '.' + ''.join(str(v) for v in participants)
            colour = participants[0]/11 # test_meta[i]['class']/len(SampleClassification)
            plt.text(results[i,0]+0.5,  results[i,1]+0.5, label, color=plt.cm.rainbow(colour), fontdict={'size': 8}) #'weight': 'bold',  'size': 11})
        
        plt.axis([np.min(results[:,0]), np.max(results[:,0]), np.min(results[:,1]), np.max(results[:,1])])
        
        if self.verbose:
            plt.savefig(os.path.join('dump', 'tsne', f'{datetime.now().strftime("%y-%m-%d_%H-%M-%S")}.png'))
        plt.show()




    def evaluate(self, *args, **kwargs):
        pass

    def predict(self, x, **kwargs):
        pass

    def load(self, path=''):
        pass

    def save(self, path=''):
        pass

    def summary(self):
        pass
