import matplotlib.pyplot as plt
import numpy as np
import os
import random
import sklearn.preprocessing
import sys
from functools import reduce
from mmwave.metadata import SampleClassification
from mmwave.postprocessing import postprocessing
from scipy.stats import describe
from tensorflow import keras


def z_score_normalise_sets(*sample_sets):
    for s in sample_sets:
        # sklearn.preprocessing.StandardScaler(copy=False).fit_transform(s.reshape((len(s), -1)))  # in place normalisation
        # sklearn.preprocessing.scale(s.reshape((len(s), -1)), axis=1, copy=False)  # in place normalisation
        sklearn.preprocessing.scale(s.reshape((1, -1)), axis=1, copy=False)  # in place normalisation


def minmax_normalise_sets(*sample_sets, fmin=0, fmax=1):
    for s in sample_sets:
        sklearn.preprocessing.MinMaxScaler(feature_range=(fmin, fmax), copy=False).fit_transform(s.reshape((len(s), -1)))
        # sklearn.preprocessing.minmax_scale(s.reshape(len(s), -1), feature_range=(fmin, fmax), axis=1, copy=False)
        # sklearn.preprocessing.minmax_scale(s.reshape(1, -1), feature_range=(fmin, fmax), axis=1, copy=False)  # in place normalisation


def pca_preprocess_sample_sets(varience_prop, *sample_sets, reconstruct=True, verbose=False):
    for samples in sample_sets:
        pca = sklearn.decomposition.PCA(varience_prop)
        reduced_samples = pca.fit_transform(samples.reshape(len(samples), -1))

        # plt.scatter(reduced_samples[:,0], reduced_samples[:,1], s=1.5, c=sample_labels(meta), label=[c.name for c in SampleClassification])
        # plt.show()
        # exit(1)

        # fig = plt.figure()
        # ax = fig.add_subplot(1, 1, 1)
        
        # d0 = np.array([reduced_samples[i] for i in range(len(reduced_samples)) if meta[i]['class'] == 0])
        # d1 = np.array([reduced_samples[i] for i in range(len(reduced_samples)) if meta[i]['class'] == 1])
        # d2 = np.array([reduced_samples[i] for i in range(len(reduced_samples)) if meta[i]['class'] == 2])
        # d3 = np.array([reduced_samples[i] for i in range(len(reduced_samples)) if meta[i]['class'] == 3])
        # d4 = np.array([reduced_samples[i] for i in range(len(reduced_samples)) if meta[i]['class'] == 4])
        # datag = (d0, d1, d2, d3, d4)
        # colors = ['red', 'green', 'blue', 'orange', 'purple']
        # groups = [c.name for c in SampleClassification]

        # for data, color, group in zip(datag, colors, groups):
        #     print(color, group, data.shape)
        #     ax.scatter(data[:, 0], data[:, 1], c=color, label=group, s=1, alpha=0.8)#, c=color, s=1.2, label=group)
        
        # plt.legend(loc=2)
        # plt.show()
        # exit(1)

        if verbose:
            print(f'PCA. Variance retained: {varience_prop:.5f}\tDimensionality: {reduced_samples.shape}')

        if reconstruct:
            yield pca.inverse_transform(reduced_samples).reshape(samples.shape)  # return reconstructed samples
        else:
            yield reduced_samples


def to_categorical(sample_labels):
    # mimic keras to_categorical, but with a known method
    #    e.g. keras.utils.to_categorical(np.array(sample_labels, dtype=np.uint8))
    cat_labels = np.zeros((len(sample_labels), len(SampleClassification)), dtype=np.uint8)
    for i, label in enumerate(sample_labels):
        cat_labels[i][SampleClassification(label).value] = 1
    return cat_labels


def from_categorical(sample_labels, confidence=False):
    if not confidence:
        return np.argmax(sample_labels, axis=1)
    return np.argmax(sample_labels, axis=1), np.max(sample_labels, axis=1)


def sample_labels(sample_set_metadata):
    labels = [0] * len(sample_set_metadata)
    for i in range(len(sample_set_metadata)):
        labels[i] = sample_set_metadata[i]['class']
    return labels


def outer_fold_validation(sample_set_dict, participant_test_set_config, test_none_prop=0.2):
    for participant_set in participant_test_set_config:
        train_set_sids = []
        # validation_set_sids = []
        test_set_sids = []
        none_sids = []

        for sid in sample_set_dict:
            if sample_set_dict[sid]['participants'] in participant_set:
                test_set_sids.append(sid)
            elif sample_set_dict[sid]['class'] != SampleClassification.NONE.value:
                train_set_sids.append(sid)
            else:
                none_sids.append(sid)

        # select NONE class samples for the test set
        test_none_idxs = sorted(random.sample(range(len(none_sids)), int(len(none_sids) * test_none_prop)))
        test_set_sids.extend([none_sids[i] for i in test_none_idxs])  # add selected sids to test set
        for i in reversed(test_none_idxs):
            del none_sids[i]  # remove selected NONE class samples from the list of sids
        train_set_sids.extend(none_sids)  # add remaining sids to the training set

        yield train_set_sids, test_set_sids


def inner_fold_validation(train_sids, kfolds, n_max):
    kf = sklearn.model_selection.KFold(n_splits=kfolds, shuffle=True, random_state=0)  # TODO: plumb seed value here
    
    for n, (train_idx, valid_idx) in enumerate(kf.split(train_sids)):
        if n >= n_max:
            break  # only do N folds
        yield [train_sids[i] for i in train_idx], [train_sids[i] for i in valid_idx]


def print_sample_set_description(train_samples, validation_samples, test_samples):
    def description_to_str(sample_set):
        if sample_set is None:
            return str(sample_set)
        d = describe(sample_set, axis=None)
        return f'{str(sample_set.shape):<22}min: {d.minmax[0]:.5f}, max: {d.minmax[1]:.5f}, mean: {d.mean:.5f}, std dev: {np.sqrt(d.variance):.5f}'

    if not (train_samples is None or validation_samples is None or test_samples is None):
        print(f'\tTotal:      {len(train_samples) + len(validation_samples) + len(test_samples)}')
    print(f'\tTrain:      ' + description_to_str(train_samples))
    print(f'\tValidation: ' + description_to_str(validation_samples))
    print(f'\tTest:       ' + description_to_str(test_samples))


def print_sample_set_distribution(train_meta, validation_meta, test_meta):
    print(f'Distribution')
    print(f'\tTrain:      {sample_set_class_distribution(train_meta)}')
    print(f'\tValidation: {sample_set_class_distribution(validation_meta)}')
    print(f'\tTest:       {sample_set_class_distribution(test_meta)}')


def sample_set_class_distribution(sample_set):
    counts = {
        SampleClassification.NONE.name: 0,
        SampleClassification.PEDESTRIAN.name: 0,
        SampleClassification.PEDEST_TWO.name: 0,
        SampleClassification.BICYCLE.name: 0,
        SampleClassification.CAR.name: 0}
    for classification in SampleClassification:
        counts[classification.name] = len(list(filter(lambda s: s['class'] == classification.value, sample_set)))
    return counts


class Logger(object):
    original_stdout = None
    original_stderr = None

    def __init__(self, log_dir, stderr=False):
        if Logger.original_stdout is None:
            Logger.original_stdout = sys.stdout
        if Logger.original_stderr is None:
            Logger.original_stderr = sys.stderr
        # self.term = sys.stdout if not stderr else sys.stderr
        self.term = Logger.original_stdout if not stderr else Logger.original_stderr
        filename = 'stdout.log' if not stderr else 'stderr.log'
        self.log = open(os.path.join(log_dir, filename), 'a')
    
    def write(self, msg):
        self.term.write(msg)
        self.log.write(msg)
    
    def flush(self):
        self.term.flush()
        self.log.flush()


def visualise_multi(n, labels, *args, vmin=None, vmax=None):
    if vmin is None:
        vmin = np.percentile(args[0], 0.1)
    if vmax is None:
        vmax = np.percentile(args[0], 99.9)

    if isinstance(n, list):
        idxs = n
        n = len(n)
    else:
        idxs = random.sample(range(len(args[0])), n)

    fig, axes = plt.subplots(nrows=len(args), ncols=n)
    for i, k in enumerate(idxs):
        axes[0, i].set_title(str(labels[k]))
        for j in range(len(args)):
            # if len(args[j].shape > 3):
            #     d = reduce(lambda x, y: x*y, args[j].shape[2:])
            # else:
            #     d = -1
            # im = axes[j, i].imshow(args[j][k].reshape(args[j].shape[1], d), vmin=vmin, vmax=vmax)
            im = axes[j, i].imshow(args[j][k].reshape(args[j].shape[1], -1), vmin=vmin, vmax=vmax)

        plt.viridis()

    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
    fig.colorbar(im, cax=cbar_ax)
    # plt.tight_layout()
    plt.show()
