from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import os
import pickle
import random
from functools import reduce
from minisom import MiniSom
from mmwave.classifier import network_base, utils
from mmwave.metadata import SampleClassification


class SelfOrganisingMap(network_base.NetworkBase):

    def build(self, input_dims, n_samples, **kwargs):
        self.n_samples = n_samples
        self.flat_len = reduce(lambda x, y: x*y, input_dims)

        n_cells = 5 * np.sqrt(self.n_samples * self.flat_len)
        self.x = int(np.ceil(np.sqrt(n_cells * 2)))  # n_cells = x * y = x * x/2 -> solve for x
        self.y = int(np.ceil(self.x / 2))  # one half rule

        model = MiniSom(self.x, self.y, self.flat_len, sigma=4, learning_rate=0.5, neighborhood_function='triangle')
        return model


    @staticmethod
    def preprocess(train_samples, train_meta, validation_samples, validation_meta, test_samples, test_meta, n_samples, fclass=None, **kwargs):
        all_samples = np.concatenate([train_samples, validation_samples, test_samples], axis=0)
        all_meta = train_meta + validation_meta + test_meta
        utils.z_score_normalise_sets(all_samples)  # normalise before to avoid contamination

        # filter by class
        if fclass is not None:
            class_idxs = [i for i in range(len(all_meta)) if SampleClassification(all_meta[i]['class']) in fclass]
            all_samples = all_samples[class_idxs]
            all_meta = [all_meta[i] for i in class_idxs]

        # sample down to n_samples
        if n_samples > 0 and n_samples < len(all_samples):
            selected_idxs = random.sample(range(len(all_samples)), n_samples)
            samples = all_samples[selected_idxs]
            meta = [all_meta[i] for i in selected_idxs]
        else:
            samples = all_samples
            meta = all_meta
        samples = samples.reshape((len(samples), -1))  # flatten all samples
        return (samples,), meta, (samples,), meta, (samples,), meta


    def train(self, train_x, validation_x, epochs, **kwargs):
        self.epochs = epochs
        self.model.random_weights_init(train_x)
        self.model.train_random(train_x, self.epochs)


    def test(self, test_x, test_meta, **kwargs):
        plt.figure(figsize=(8, 8))
        plt.gca().set_facecolor('xkcd:black')
        wmap = {}

        for i in range(len(test_x)):
            w = self.model.winner(test_x[i])
            wmap[w] = i

            participants = test_meta[i]['participants'] if test_meta[i]['participants'] else [0]
            label = str(test_meta[i]['class']) #+ '.' + test_meta[i]['scenario'][4:].replace('.', '') # + '.' + ''.join(str(v) for v in test_meta[i]['participants'])
            colour = participants[0]/11 #test_meta[i]['class']/len(SampleClassification)

            # colour = sum(ord(s) for s in label) / 700

            plt.text(w[0]+0.5,  w[1]+0.5, label, color=plt.cm.rainbow(colour), fontdict={'size': 8}) #'weight': 'bold',  'size': 11})
        
        plt.axis([0, self.model.get_weights().shape[0], 0, self.model.get_weights().shape[1]])
        plt.title(f'SOM Size: {self.x}x{self.y}, Input Dims: {test_x.shape}, Epochs: {self.epochs}')
        # plt.figtext(0.5, 0.05, 'Key: Number = class, Colour = participant', fontsize=10, ha='center')
        plt.figtext(0.5, 0.03, str(kwargs), fontsize=10, ha='center')
        
        if self.verbose:
            plt.savefig(os.path.join('dump', 'som', f'{datetime.now().strftime("%y-%m-%d_%H-%M-%S")}.png'))
        plt.show()


    def evaluate(self, *args, **kwargs):
        pass


    def predict(self, x, **kwargs):
        pass


    def load(self, path=''):
        if self.verbose:
            print(f'Loading model: {path}')
        with open(path, 'rb') as file:
            self.model = pickle.load(file)


    def save(self, path=''):
        # TODO: save selected indices too
        if self.verbose:
            print(f'Saving model: {path}')
        with open(path, 'wb') as file:
            pickle.dump(self.model, file)


    def summary(self):
        print(f'SOM Size: {self.x}x{self.y}')
        print(f'Data Input Dims: {self.n_samples}x{self.flat_len}')
