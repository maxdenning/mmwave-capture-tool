import tensorflow as tf
from mmwave.classifier import network_base, evaluation, utils
from mmwave.postprocessing import postprocessing, utils as pp_utils
import numpy as np
from tensorflow import keras
from tensorflow.python.keras import backend as K, layers

import cv2 #as cv



class OpticalFlow(network_base.TensorflowNetwork):

    def build(self, input_dims, **kwargs):
        return None

    @staticmethod
    def preprocess(train_samples, train_meta, validation_samples, validation_meta, test_samples, test_meta, **kwargs):
        utils.z_score_normalise_sets(train_samples, validation_samples, test_samples)


        cv2.namedWindow('raw', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('raw', 256, 256)
        cv2.namedWindow('img0', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('img0', 256, 256)
        cv2.namedWindow('img1', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('img1', 256, 256)
        cv2.namedWindow('img2', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('img2', 256, 256)

        for sample in train_samples[:10]:

            # sample = train_samples[0]
            # sample = postprocessing.high_pass_filter(sample, 1, 1)

            prv = cv2.equalizeHist(postprocessing.rdm_to_greyscale(sample[0]).squeeze())
            # prv = postprocessing.rdm_to_greyscale(sample[0]).squeeze()
            hsv = np.zeros((32,32,3), dtype=np.uint8)
            hsv[...,1] = 255
            
            optical_flow = cv2.DualTVL1OpticalFlow_create()
            flow = None

            for i, frame in enumerate(sample[1:]):
                nxt = cv2.equalizeHist(postprocessing.rdm_to_greyscale(frame).squeeze())
                
                # print(frame.shape, hsv.shape, prv.shape, nxt.shape)

                # flow = cv2.calcOpticalFlowFarneback(prv, nxt, None, 0.5, 3, 15, 3, 5, 1.2, 0)
                # flow = cv2.calcOpticalFlowFarneback(prv, nxt,   None, 0.5, 3,  2, 3, 2, 1.1, 0)
                flow = optical_flow.calc(prv, nxt, flow)

                # flow = (32,32,2)
                mag, ang = cv2.cartToPolar(flow[...,0], flow[...,1])
                hsv[...,0] = ang*180/np.pi/2
                hsv[...,2] = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)

                bgr = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
                cv2.imshow('raw', nxt)
                cv2.imshow('img0', bgr)
                cv2.imshow('img1', flow[:,:,0])
                cv2.imshow('img2', flow[:,:,1])
                
                k = cv2.waitKey(100) & 0xff
                if k == 27:
                    break
                prv = nxt

        cv2.destroyAllWindows()


        # cap = cv2.VideoCapture("data\\19-02-16\\15-14\\scene_video.avi")

        # ret, frame1 = cap.read() #bgr
        # prvs = cv2.cvtColor(frame1,cv2.COLOR_BGR2GRAY)
        # hsv = np.zeros_like(frame1)
        # hsv[...,1] = 255

        #     #   (240, 320, 3)   (240, 320, 3) (240, 320)
        # print(frame1.shape,   hsv.shape,    prvs.shape, frame1.dtype, hsv.dtype, prvs.dtype)

        # while(1):
        #     ret, frame2 = cap.read()
        #     next = cv2.cvtColor(frame2,cv2.COLOR_BGR2GRAY)
        #     flow = cv2.calcOpticalFlowFarneback(prvs, next, None, 0.5, 3, 15, 3, 5, 1.2, 0)
        #     mag, ang = cv2.cartToPolar(flow[...,0], flow[...,1])
        #     hsv[...,0] = ang*180/np.pi/2
        #     hsv[...,2] = cv2.normalize(mag,None,0,255,cv2.NORM_MINMAX)
        #     bgr = cv2.cvtColor(hsv,cv2.COLOR_HSV2BGR)
        #     cv2.imshow('frame2',bgr)
        #     k = cv2.waitKey(30) & 0xff
        #     if k == 27:
        #         break
        #     prvs = next
        # cap.release()
        # cv2.destroyAllWindows()


        # params for ShiTomasi corner detection
        # feature_params = dict( maxCorners = 100,
        #                     qualityLevel = 0.3,
        #                     minDistance = 7,
        #                     blockSize = 7 )
        # # Parameters for lucas kanade optical flow
        # lk_params = dict( winSize  = (15,15),
        #                 maxLevel = 2,
        #                 criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))
        # # Create some random colors
        # color = np.random.randint(0,255,(100,3))
        # # Take first frame and find corners in it

        # # ret, old_frame = cap.read()
        # old_gray = postprocessing.rdm_to_greyscale(sample[0]).squeeze() #cv2.cvtColor(old_frame, cv2.COLOR_BGR2GRAY)
        # p0 = cv2.goodFeaturesToTrack(old_gray, mask = None, **feature_params)
        # # Create a mask image for drawing purposes
        # mask = np.zeros((32,32,3), dtype=np.uint8)

        # for i, frame in enumerate(sample[1:]):
        #     # ret,frame = cap.read()
        #     frame_hsv = cv2.applyColorMap(postprocessing.rdm_to_greyscale(frame).squeeze(), cv2.COLORMAP_HSV)
        #     frame_gray = postprocessing.rdm_to_greyscale(frame).squeeze() # cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        #     # calculate optical flow
        #     p1, st, err = cv2.calcOpticalFlowPyrLK(old_gray, frame_gray, p0, None, **lk_params)
        #     # Select good points
        #     good_new = p1[st==1]
        #     good_old = p0[st==1]
        #     # draw the tracks
        #     for i,(new,old) in enumerate(zip(good_new,good_old)):
        #         a,b = new.ravel()
        #         c,d = old.ravel()
        #         mask = cv2.line(mask, (a,b),(c,d), color[i].tolist(), 2)
        #         frame_hsv = cv2.circle(frame_hsv,(a,b),5,color[i].tolist(),-1)
        #     # img = cv2.add(frame,mask)
        #     img = cv2.add(frame_hsv,mask)
        #     cv2.imshow('img',img)
        #     k = cv2.waitKey(30) & 0xff
        #     if k == 27:
        #         break
        #     # Now update the previous frame and previous points
        #     old_gray = frame_gray.copy()
        #     p0 = good_new.reshape(-1,1,2)
        # cv2.destroyAllWindows()
        # cap.release()



        exit(1)
        return None
        # return \
        #     (train_samples, train_labels), train_meta, \
        #     (validation_samples, validation_labels), validation_meta, \
        #     (test_samples, test_labels), test_meta
