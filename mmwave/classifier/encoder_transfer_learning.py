import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from mmwave.classifier import network_base, utils, partitioned_autoencoder
from mmwave.metadata import SampleClassification
from tensorflow import keras
from tensorflow.python.keras import activations, layers, backend as K


class EncoderTransferLearning(network_base.TensorflowNetwork):

    def build(self, input_dims, **kwargs):

        in0 = layers.Input(shape=input_dims)

        x = layers.TimeDistributed(self._load_encoder(), input_shape=input_dims)(in0)
        x = layers.TimeDistributed(layers.Conv2D(32, (1, 1), activation='relu', padding='same'))(x)
        x = layers.TimeDistributed(layers.Dropout(0.4))(x)
        x = layers.TimeDistributed(layers.Conv2D(32, (2, 2), activation='relu', padding='same'))(x)
        x = layers.TimeDistributed(layers.BatchNormalization(axis=-1))(x)
        x = layers.TimeDistributed(layers.LeakyReLU(0))(x)
        x = layers.TimeDistributed(layers.MaxPool2D((2, 2)))(x)

        x = layers.Flatten()(x)
        x = layers.Dense(units=512, activation='relu')(x)
        x = layers.Dense(units=128, activation='relu')(x)
        out0 = layers.Dense(units=len(SampleClassification), activation='softmax')(x)
        
        model = keras.models.Model(inputs=[in0], outputs=[out0])
        model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
        return model


    def _load_encoder(self):
        # path = 'dump\\models\\PartitionedAutoencoder\\19-04-07_14-33-10\\00\\weights.h5'  # fixed-camshift (50)
        # path = 'dump\\models\\PartitionedAutoencoder\\19-04-16_20-21-58\\00\\weights.h5'  # fixed-peak
        # path = 'dump\\models\\PartitionedAutoencoder\\19-04-16_21-31-07\\00\\weights.h5'  # dynamic-max
        # path = 'dump\\models\\PartitionedAutoencoder\\19-04-16_22-25-24\\00\\weights.h5'  # dynamic-min
        # path = 'dump\\models\\PartitionedAutoencoder\\19-04-19_19-59-16\\00\\weights.h5'  # fixed-camshift (350)
        
        if self.verbose:
            print(f'Loading PAE')
        # encoder_model = partitioned_autoencoder.PartitionedAutoencoder(load=path, load_model=False, verbose=self.verbose, input_dims=(32,32,1)).export_encoder_model()
        encoder_model = partitioned_autoencoder.PartitionedAutoencoder(verbose=self.verbose, input_dims=(32,32,1)).export_encoder_model()

        for layer in encoder_model.layers:
            layer.trainable=False
        encoder_model.trainable = False

        return encoder_model


    @staticmethod
    def preprocess(train_samples, train_meta, validation_samples, validation_meta, test_samples, test_meta, batch_size, **kwargs):
        # if batch_size != 16:
        #     raise ValueError(f'Batch size incorrect: {batch_size} != 16')  # TODO: is this relevant?

        # if len(train_samples) % batch_size:
        #     train_samples = train_samples[:-(len(train_samples) % batch_size)]
        #     train_meta = train_meta[:-(len(train_meta) % batch_size)]
        # if len(validation_samples) % batch_size:
        #     validation_samples = validation_samples[:-(len(validation_samples) % batch_size)]
        #     validation_meta = validation_meta[:-(len(validation_meta) % batch_size)]
        # if len(test_samples) % batch_size:
        #     test_samples = test_samples[:-(len(test_samples) % batch_size)]
        #     test_meta = test_meta[:-(len(test_meta) % batch_size)]

        utils.z_score_normalise_sets(train_samples, validation_samples, test_samples)

        train_labels = utils.to_categorical(utils.sample_labels(train_meta))
        validation_labels = utils.to_categorical(utils.sample_labels(validation_meta))
        test_labels = utils.to_categorical(utils.sample_labels(test_meta))
        return \
            (train_samples, train_labels), train_meta, \
            (validation_samples, validation_labels), validation_meta, \
            (test_samples, test_labels), test_meta
