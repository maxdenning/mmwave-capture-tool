import matplotlib.pyplot as plt
import numpy as np
import os
import random
from mmwave.classifier import encoder_decoder, som, utils


class DAESelfOrganisingMap(som.SelfOrganisingMap):

    # def preprocess(self, t_samples, t_labels, v_samples, v_labels, n_samples, load, **kwargs):
    #     selected_idxs = random.sample(range(len(t_samples)), n_samples)
    #     samples = t_samples[selected_idxs]
    #     labels = utils.from_categorical(t_labels[selected_idxs])

    #     utils.normalise_sets(samples)
    #     encoded_samples = self.encoder.predict(samples)

    #     if self.verbose:
    #         decoded_samples = self.decoder.model.predict(samples)
    #         encoder_decoder.visualise_encoder_decoder(10, samples, decoded_samples, encoded_samples, labels)

    #     samples = encoded_samples.reshape((self.n_samples, self.flat_len))  # flatten all images
    #     return samples, labels, samples, labels


    @staticmethod
    def preprocess(t_samples, t_meta, v_samples, v_meta, n_samples, load, **kwargs):
        selected_idxs = random.sample(range(len(t_samples)), n_samples)
        samples = t_samples[selected_idxs]
        meta = [t_meta[i] for i in selected_idxs]
        # labels = utils.from_categorical(t_labels[selected_idxs])

        utils.normalise_sets(samples)

        decoder = encoder_decoder.EncoderDecoder(logs=None, load=load)
        encoder = decoder.export_encoder()
        encoded_samples = encoder.predict(samples)

        decoded_samples = decoder.model.predict(samples)
        encoder_decoder.visualise_encoder_decoder(10, samples, decoded_samples, encoded_samples, labels)

        samples = encoded_samples.reshape((n_samples, -1))  # flatten all images
        return (samples,), meta, (samples,), meta


    # def load(self, path):
    #     paths = path.split(',', 1)  # [0] = encoder, [1] = som
    #     self.decoder = encoder_decoder.EncoderDecoder(logs=None, load=paths[0])
    #     self.encoder = self.decoder.export_encoder()
    #     if len(paths) > 1 and paths[1]:
    #         super().load(paths[1])


    # def save(self, path):
    #     print('Saving disabled')
