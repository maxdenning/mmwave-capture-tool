from mmwave.classifier import network_base
from tensorflow import keras
from tensorflow.python.keras import layers


class FixedEncoderDecoder(network_base.TensorflowNetwork):


# from keras.optimizers import SGD
# lrate = 0.01
# decay = lrate/epochs
# sgd = SGD(lr=lrate, momentum=0.9, decay=decay, nesterov=False)

# model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])


    def build(self, input_dims, **kwargs):

        # self._model = keras.Sequential([
        #     # encoder
        #     # keras.layers.Conv2D(16, (3, 3), activation='relu', padding='same', input_shape=(28,28,1)),
        #     keras.layers.Conv2D(32, (3, 3), activation='relu', padding='same', input_shape=(32,32,1)),
        #     keras.layers.MaxPooling2D((2, 2), padding='same'),
        #     keras.layers.Conv2D(32, (3, 3), activation='relu', padding='same'),
        #     keras.layers.MaxPooling2D((2, 2), padding='same'),
        #     keras.layers.Conv2D(32, (3, 3), activation='relu', padding='same'),
        #     keras.layers.MaxPooling2D((2, 2), padding='same'),
        #     # decoder - at this point the representation is (4, 4, 8) i.e. 128-dimensional
        #     keras.layers.Conv2D(32, (3, 3), activation='relu', padding='same'),
        #     keras.layers.UpSampling2D((2, 2)),
        #     keras.layers.Conv2D(32, (3, 3), activation='relu', padding='same'),
        #     keras.layers.UpSampling2D((2, 2)),
        #     keras.layers.Conv2D(32, (3, 3), activation='relu', padding='same'),
        #     keras.layers.UpSampling2D((2, 2)),
        #     keras.layers.Conv2D(1, (3, 3), activation='sigmoid', padding='same'),
        # ])

        # self._model = keras.Sequential([
        #     # encoder
        #     # keras.layers.Conv2D(16, (3, 3), activation='relu', padding='same', input_shape=(28,28,1)),
        #     keras.layers.Conv2D(16, (3, 3), activation='relu', padding='same', input_shape=(32,32,1)),
        #     keras.layers.MaxPooling2D((2, 2), padding='same'),
        #     keras.layers.Conv2D(64, (3, 3), activation='relu', padding='same'),
        #     keras.layers.MaxPooling2D((2, 2), padding='same'),
        #     keras.layers.Conv2D(256, (3, 3), activation='relu', padding='same'),
        #     keras.layers.MaxPooling2D((2, 2), padding='same'),
        #     # decoder - at this point the representation is (4, 4, 8) i.e. 128-dimensional
        #     keras.layers.Conv2D(256, (3, 3), activation='relu', padding='same'),
        #     keras.layers.UpSampling2D((2, 2)),
        #     keras.layers.Conv2D(64, (3, 3), activation='relu', padding='same'),
        #     keras.layers.UpSampling2D((2, 2)),
        #     keras.layers.Conv2D(16, (3, 3), activation='relu', padding='same'),
        #     keras.layers.UpSampling2D((2, 2)),
        #     keras.layers.Conv2D(1, (3, 3), activation='sigmoid', padding='same'),
        # ])


        i0 = keras.layers.Input(shape=(32, 32, 1))

        t1 = keras.layers.Conv2D(64, (1, 1), padding='same', activation='relu')(i0)
        t1 = keras.layers.Conv2D(64, (3, 3), padding='same', activation='relu')(t1)

        t2 = keras.layers.Conv2D(64, (1, 1), padding='same', activation='relu')(i0)
        t2 = keras.layers.Conv2D(64, (5, 5), padding='same', activation='relu')(t2)

        t3 = keras.layers.MaxPooling2D((3, 3), strides=(1, 1), padding='same')(i0)
        t3 = keras.layers.Conv2D(64, (1, 1), padding='same', activation='relu')(t3)


        o0 = keras.layers.concatenate([t1, t2, t3], axis=3)
        o0 = keras.layers.Flatten()(o0)
        o0 = keras.layers.Dense(5, activation='softmax')(o0)

        self._model = keras.models.Model(inputs=[i0], outputs=[o0])


        # b1 = keras.layers.Conv2D(32, (3, 3), activation='relu', padding='same')(input0)
        # b2 = keras.layers.Conv2D(32, (9, 9), activation='relu', padding='same')(input0)

        # c1 = keras.layers.concatenate([b1, b2])
        # out0 = keras.layers.MaxPooling2D((2, 2))(c1)

        # self._model = keras.models.Model(inputs=[input0], outputs=[out0])




        # i0 = keras.layers.Input(shape=(28,28,1))
        # b1 = keras.layers.Conv2D(32, (3,3), activation='relu')(i0) #, input_shape=(28,28,1))
        # b2 = keras.layers.Conv2D(32, (9,9), activation='relu')(i0) #, input_shape=(28,28,1))

        # out = keras.layers.Concatenate([b1, b2])
        # # out = keras.layers.Merge(mode='concat')([b1, b2])


        # conv_model = keras.models.Model(i0, out)

        # p0 = keras.layers.MaxPooling2D((2, 2))

        # self._model = keras.models.Sequential()
        # self._model.add(conv_model)
        # self._model.add(p0)

        self._model.compile(optimizer='adadelta', loss='binary_crossentropy')

        print(self._model.summary())


    def _build_encoder_unit(self, input_layer):
        br1 = layers.Conv2D(32, (3, 3), padding='same', activation='relu')(input_layer)
        br2 = layers.Conv2D(32, (9, 9), padding='same', activation='relu')(input_layer)
        cat = layers.concatenate([br1, br2])
        out = layers.MaxPooling2D((2, 2))(cat)
        return out


    def fit(self, t_samples, t_labels, v_samples, v_labels, epochs, batch_size, **kwargs):
        self.model.fit(
            t_samples, t_labels, epochs=epochs, batch_size=batch_size, shuffle=True,
            validation_data=(v_samples, v_labels), callbacks=[self.tensorboard])


    def validate(self, samples, labels):
        # predictions = self.model.predict(samples)
        # return evaluation.confusion_matrix(labels, predictions)
        return
