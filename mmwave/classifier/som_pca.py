import matplotlib.pyplot as plt
import numpy as np
import os
import random
import sklearn.decomposition
from minisom import MiniSom
from mmwave.classifier import som, utils
from mmwave.metadata import SampleClassification


class PCASelfOrganisingMap(som.SelfOrganisingMap):

    @staticmethod
    def preprocess(train_samples, train_meta, validation_samples, validation_meta, test_samples, test_meta, n_samples, pca_variance, **kwargs):
        (samples,), meta, _, _, _, _ = som.SelfOrganisingMap.preprocess(train_samples, train_meta, validation_samples, validation_meta, test_samples, test_meta, n_samples, **kwargs)
        pca = sklearn.decomposition.PCA(pca_variance)
        pca_samples = pca.fit_transform(samples)


        # class_idxs = [i for i in range(len(meta)) if meta[i]['class'] == SampleClassification.CAR.value]
        # pca_samples = pca_samples[class_idxs]
        # meta = [meta[i] for i in class_idxs]

        # print(pca_samples.shape)

        # recon = pca.inverse_transform(pca_samples).reshape(-1, 32, 32)
        # utils.visualise_multi(10, utils.sample_labels(meta), samples.reshape(-1, 32, 32), recon)
        
        # exit(1)

        return (pca_samples,), meta, (pca_samples,), meta, (pca_samples,), meta
