import matplotlib.pyplot as plt
import numpy as np
import random
import tensorflow as tf
from mmwave.classifier import network_base, utils
from tensorflow import keras
from tensorflow.python.keras import activations, layers, backend as K

from functools import partial, update_wrapper

from scipy.stats import describe


class PartitionedAutoencoder(network_base.TensorflowNetwork):
    # Based on:
    # https://arxiv.org/pdf/1509.05982.pdf
    # https://github.com/danstowell/autoencoder-specgram


    def build(self, input_dims, batch_size=16, background_batch=0.25, latents=32, background_latents=0.25, lam=0.75, **kwargs):
        self.batch_size = batch_size
        self.background_batch_prop = min(background_batch, 1)
        self.latents = latents
        self.background_latents_prop = min(background_latents, 1)
        self.bottleneck_layer_name = 'mask_dense_background'

        self.original_weights = None

        # X
        in0 = layers.Input(shape=input_dims)

        # encoder -> f(X)
        x = PartitionedAutoencoder._build_encoder_unit(in0, 64, name='encoder_step_1')
        x = PartitionedAutoencoder._build_encoder_unit(x, 64, name='encoder_step_2')
        x = PartitionedAutoencoder._build_encoder_unit(x, 64, name='encoder_step_3')

        # bottleneck
        x = layers.Flatten()(x)
        x = layers.Dense(latents, activation='linear', use_bias=False, name='bottleneck_input')(x)
        x = MaskDenseBackgroundLayer(self.latents, self.background_latents_prop, name=self.bottleneck_layer_name)(x)
        x = layers.Dense(4 * 4 * latents, activation='linear', use_bias=False)(x)
        x = layers.Reshape((4, 4, latents), name='bottleneck_export')(x)  # should be the same x and y dims as the encoder output

        # decoder -> g(f(X))
        x = PartitionedAutoencoder._build_decoder_unit(x, 64, name='decoder_step_1')
        x = PartitionedAutoencoder._build_decoder_unit(x, 64, name='decoder_step_2')
        x = PartitionedAutoencoder._build_decoder_unit(x, 64, name='decoder_step_3')

        # output
        out0 = PartitionedAutoencoder._build_output_unit(x)

        # build the model
        model = keras.models.Model(inputs=[in0], outputs=[out0])
        loss_fn = partial(
            partitioned_autoencoder_loss,
            encoder=model.get_layer(name=self.bottleneck_layer_name),
            y_labels=self._weak_batch_labels(),
            latents=self.latents,
            background_latents_prop=self.background_latents_prop,
            lam=lam
        )
        update_wrapper(loss_fn, partitioned_autoencoder_loss)
        model.compile(optimizer='adadelta', loss=loss_fn, metrics=['accuracy'])
        return model


    # @staticmethod
    # def _build_inception_unit(input_layer, f=4, act='relu', pad='same', name=None):
    #     # inception module based on 
    #     # https://arxiv.org/pdf/1512.00567v3.pdf

    #     b0 = layers.Conv2D(f, (1, 1), strides=(1, 1), activation=act, padding=pad)(input_layer)
    #     b0 = layers.Conv2D(f, (3, 3), strides=(1, 1), activation=act, padding=pad)(b0)
    #     b0b0 = layers.Conv2D(f, (1, 3), strides=(1, 1), activation=act, padding=pad)(b0)
    #     b0b1 = layers.Conv2D(f, (3, 1), strides=(1, 1), activation=act, padding=pad)(b0)
        
    #     b1 = layers.Conv2D(f, (1, 1), strides=(1, 1), activation=act, padding=pad)(input_layer)
    #     b1b0 = layers.Conv2D(f, (1, 3), strides=(1, 1), activation=act, padding=pad)(b1)
    #     b1b1 = layers.Conv2D(f, (3, 1), strides=(1, 1), activation=act, padding=pad)(b1)

    #     b2 = layers.MaxPooling2D((2, 2), strides=(1, 1), padding=pad)(input_layer)
    #     b2 = layers.Conv2D(f, (1, 1), strides=(1, 1), activation=act, padding=pad)(b2)

    #     b3 = layers.Conv2D(f, (1, 1), strides=(1, 1), activation=act, padding=pad)(input_layer)

    #     cat = layers.concatenate([b0b0, b0b1, b1b0, b1b1, b2, b3], axis=-1, name=name)
    #     return cat


    @staticmethod
    def _build_encoder_unit(input_layer, f, name=None):
        input_layer = layers.Conv2D(f, (1, 1), padding='same', activation='relu')(input_layer)        ###############
        br1 = layers.Conv2D(f, (3, 3), padding='same', activation='relu')(input_layer)
        br2 = layers.Conv2D(f, (5, 5), padding='same', activation='relu')(input_layer)
        cat = layers.concatenate([br1, br2])

        # cat = PartitionedAutoencoder._build_inception_unit(input_layer, f)
        out = layers.MaxPooling2D((2, 2), name=name)(cat)
        # out = layers.Dropout(0.1)(out)
        return out


    @staticmethod
    def _build_decoder_unit(input_layer, f, name=None):
        up = layers.UpSampling2D((2, 2))(input_layer)
        # cat = PartitionedAutoencoder._build_inception_unit(up, f, name=name)
        # cat = layers.Dropout(0.1)(cat)

        up = layers.Conv2D(f, (1, 1), padding='same', activation='relu')(up)        ###############
        br1 = layers.Conv2D(f, (3, 3), padding='same', activation='relu')(up)
        br2 = layers.Conv2D(f, (5, 5), padding='same', activation='relu')(up)
        cat = layers.concatenate([br1, br2], name=name)
        return cat


    @staticmethod
    def _build_output_unit(input_layer):
        return layers.Conv2D(1, (1, 1), padding='same', activation='linear')(input_layer)


    @staticmethod
    def preprocess(train_samples, train_meta, validation_samples, validation_meta, test_samples, test_meta, batch_size, background_batch, **kwargs):
        # utils.minmax_normalise_sets(train_samples, validation_samples, test_samples)
        utils.z_score_normalise_sets(train_samples, validation_samples, test_samples)

        # organise batches within sample sets
        train_samples, train_meta = PartitionedAutoencoder.sort_batches(train_samples, train_meta, batch_size, background_batch)
        validation_samples, validation_meta = PartitionedAutoencoder.sort_batches(validation_samples, validation_meta, batch_size, background_batch)

        # make test set length divisible by batch_size
        if len(test_samples) % batch_size:
            test_samples = test_samples[:-(len(test_samples) % batch_size)]
            test_meta = test_meta[:-(len(test_meta) % batch_size)]

        return \
            (train_samples,), train_meta, \
            (validation_samples,), validation_meta, \
            (test_samples,), test_meta


    def train(self, train_x, validation_x, epochs, **kwargs):
        self.model.fit(
            train_x, train_x, validation_data=(validation_x, validation_x), epochs=epochs, batch_size=self.batch_size,
            shuffle=False, initial_epoch=self.initial_epoch, callbacks=self.callbacks, verbose=2)


    def evaluate(self, x, meta, **kwargs):
        pass


    def test(self, x, meta, batch_size, **kwargs):
        # idxs = random.sample(range(len(x)), batch_size*10)
        # labels = [meta[i]['class'] for i in idxs]

        predicted_before = self.model.predict(x, batch_size=batch_size)
        self.model.get_layer(name=self.bottleneck_layer_name).mask_background()
        # self.background_stuff(False)
        predicted_after = self.model.predict(x, batch_size=batch_size)
        self.model.get_layer(name=self.bottleneck_layer_name).mask_foreground()
        # self.background_stuff(True)

        predicted_after_noise = self.model.predict(x, batch_size=batch_size)

        print(describe(x, axis=None))
        print(describe(predicted_before, axis=None))
        print(describe(predicted_after, axis=None))
        print(describe(predicted_after_noise, axis=None))
        utils.visualise_multi(10, utils.sample_labels(meta), x, predicted_before, predicted_after, predicted_after_noise)

        # predicted_enc4 = self.export_encoder_model(layer='bottleneck_input').predict(x, batch_size=batch_size)

        # predicted_enc8 = self.export_encoder_model(layer='bottleneck_input').predict(x, batch_size=batch_size)
        # predicted_enc4 = self.export_encoder_model(layer='bottleneck_export').predict(x, batch_size=batch_size)
        # predicted_dec8 = self.export_encoder_model(layer='decoder_step_3').predict(x, batch_size=batch_size)

        # utils.visualise_multi(2, utils.sample_labels(meta), x, predicted_before, predicted_enc8)


    @staticmethod
    def sort_batches(samples, meta, batch_size, background_batch_ratio):
        # split indices into all zero and all non-zero samples
        zero = np.array([i for i in range(len(meta)) if meta[i]['class'] == 0], dtype=int)
        other = np.array([i for i in range(len(meta)) if meta[i]['class'] != 0], dtype=int)
        np.random.shuffle(zero)
        np.random.shuffle(other)

        # organise into batches of zero and non-zero parts
        n_noise_only = int(batch_size * background_batch_ratio)
        batched_idxs_len_upper = int(len(zero) * batch_size/n_noise_only)
        batched_idxs = np.zeros((batched_idxs_len_upper - (batched_idxs_len_upper % batch_size)), dtype=int)
        for i in range(0, len(batched_idxs), batch_size):
            selected = random.sample(range(len(zero)), n_noise_only)
            batched_idxs[i:i+n_noise_only] = zero[selected]
            zero = np.delete(zero, selected)

            selected = random.sample(range(len(other)), batch_size - n_noise_only)
            batched_idxs[i+n_noise_only:i+batch_size] = other[selected]
            other = np.delete(other, selected)

        return samples[batched_idxs], [meta[i] for i in batched_idxs]


    def _weak_batch_labels(self):
        # labels: 1 = noise only sample, 0 = noise + signal sample
        labels = np.zeros((self.batch_size), dtype=int)
        labels[:int(self.batch_size * self.background_batch_prop)] = 1
        return labels


    def export_encoder_model(self, layer='bottleneck_export'):
        export_layer = self.model.get_layer(name=layer)
        export_model = keras.models.Model(inputs=[self.model.input], outputs=[export_layer.output])
        export_model.get_layer(name=self.bottleneck_layer_name).mask_background()
        return export_model


    def summary(self):
        super().summary()
        print(f'Batch Size: {self.batch_size} Background Prop: {self.background_batch_prop}')
        print(f'Latents: {self.latents} Background Prop: {self.background_latents_prop}')

    # @property
    # def custom_objects(self):
    #     class wrap():
    #         __init__ = partial(MaskDenseBackgroundLayer.__init__, latents=self.latents, background_latents_prop=self.background_batch_prop)
        
    #     # update_wrapper(wrap.__init__, MaskDenseBackgroundLayer.__init__)
    #     loss = partial(partitioned_autoencoder_loss, 
    #     return {
    #         'MaskDenseBackgroundLayer': wrap,
    #         'partitioned_autoencoder_loss': loss
    #     }


    # def background_stuff(self, mask_foreground):
    #     # set background latents to zero

    #     output_layer = self.model.get_layer(name='bottleneck_input')

    #     if self.original_weights == None:
    #         self.original_weights = output_layer.get_weights()

    #     print(len(self.original_weights), self.original_weights[0].shape)

    #     mask = np.ones((1, 32), dtype=int)
    #     if mask_foreground:
    #         mask[:, int(32 * self.background_batch_prop):] = 0
    #     else:
    #         mask[:, :int(32 * self.background_batch_prop)] = 0

    #     masked_weights = mask * self.original_weights[0]
    #     # weights[0] = masked_weights
    #     self.model.get_layer(name='bottleneck_input').set_weights([masked_weights,])





class MaskDenseBackgroundLayer(layers.Layer):
    def __init__(self, latents, background_latents_prop, name=None, **kwargs):
        super().__init__(name=name, **kwargs)
        self.latents = latents
        self.background_latents_prop = background_latents_prop
        self.k_mask = K.variable(np.ones((1, self.latents), dtype=float))

    def call(self, input_tensor):
        return self.k_mask * input_tensor

    def mask_background(self):
        # select background latents and zero them
        mask = np.ones((1, self.latents), dtype=float)
        mask[:, :int(self.latents * self.background_latents_prop)] = 0
        K.set_value(self.k_mask, mask)

    def mask_foreground(self):
        # select foreground latents and zero them
        mask = np.ones((1, self.latents), dtype=float)
        mask[:, int(self.latents * self.background_latents_prop):] = 0
        K.set_value(self.k_mask, mask)



def partitioned_autoencoder_loss(y_true, y_pred, encoder=None, y_labels=None, background_latents_prop=0.0, latents=0, lam=0.0):
    y_true_labels = K.variable(y_labels)
    y_true_samples = y_true
    y_pred_encoded = encoder.output
    y_pred_decoded = y_pred
    lam = K.constant(lam)

    # select foreground latents and set to 1
    mask_values = np.zeros((len(y_labels), latents), dtype=float)
    mask_values[:, int(latents * background_latents_prop):] = 1  # latents = background + foreground
    mask = K.constant(mask_values)

    def norm(a, axis):
        return tf.sqrt(K.sum(K.square(a), axis=axis))

    t1 = K.pow(norm(y_true_samples - y_pred_decoded, axis=[1,2,3]), 2)
    t2 = (lam * y_true_labels) / K.mean(mask)
    t3 = K.pow(norm(mask * y_pred_encoded, axis=-1), 2)
    res = t1 + (t2 * t3)
    return K.reshape(res, (-1,1,1))
