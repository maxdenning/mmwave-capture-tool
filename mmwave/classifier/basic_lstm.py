import tensorflow as tf
from mmwave.classifier import network_base, utils
from mmwave.metadata import SampleClassification
from tensorflow import keras
from tensorflow.python.keras import layers


class BasicLSTM(network_base.TensorflowNetwork):
    # Based on
    # https://www.researchgate.net/publication/324389438


    def build(self, input_dims, **kwargs):
        in0 = layers.Input(shape=input_dims)
        x = layers.TimeDistributed(layers.Conv2D(2, (5, 5), strides=(1, 1), activation='relu'), input_shape=input_dims)(in0)
        x = layers.TimeDistributed(layers.MaxPool2D((2, 2)))(x)
        x = layers.TimeDistributed(layers.Flatten())(x)
        # if tf.test.is_gpu_available():
        #     x = layers.CuDNNLSTM(units=50)(x)  # activation is tanh
        # else:
        x = layers.LSTM(units=50, activation='tanh')(x)
        out0 = layers.Dense(units=len(SampleClassification), activation='softmax')(x)

        model = keras.models.Model(inputs=[in0], outputs=[out0])
        model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
        return model


    def evaluate(self, x, y, meta, batch_size, **kwargs):
        loss, acc = self.model.evaluate(x, y, batch_size=batch_size, verbose=0)
        print(f'Loss: {loss:.5f}\tAccuracy: {acc:.5f}')
