import matplotlib.pyplot as plt
import numpy as np
import os
import random
from datetime import datetime
from mmwave.classifier import network_base, utils
from tensorflow import keras
from tensorflow.python.keras import layers


class EncoderDecoder(network_base.TensorflowNetwork):

    def build(self, input_dims, **kwargs):
        in0 = layers.Input(shape=input_dims)
        x = self._build_encoder_unit(in0)
        x = self._build_encoder_unit(x)
        encoder = self._build_encoder_unit(x)
        x = self._build_decoder_unit(encoder)
        x = self._build_decoder_unit(x)
        decoder = self._build_decoder_unit(x)
        out0 = self._build_output_unit(decoder)

        self.encoder_layer_name = encoder.op.name.split('/')[0]
        model = keras.models.Model(inputs=[in0], outputs=[out0])
        model.compile(optimizer='adadelta', loss='binary_crossentropy', metrics=['accuracy'])
        return model


    def _build_encoder_unit(self, input_layer):
        br1 = layers.Conv2D(32, (3, 3), padding='same', activation='relu')(input_layer)
        br2 = layers.Conv2D(32, (9, 9), padding='same', activation='relu')(input_layer)
        cat = layers.concatenate([br1, br2])
        out = layers.MaxPooling2D((2, 2))(cat)
        return out


    def _build_decoder_unit(self, input_layer):
        up = layers.UpSampling2D((2, 2))(input_layer)
        br1 = layers.Conv2D(32, (3, 3), padding='same', activation='relu')(up)
        br2 = layers.Conv2D(32, (9, 9), padding='same', activation='relu')(up)
        cat = layers.concatenate([br1, br2])
        return cat


    def _build_output_unit(self, input_layer):
        return layers.Conv2D(1, (1, 1), padding='same', activation='sigmoid')(input_layer)


    @staticmethod
    def preprocess(train_samples, train_meta, validation_samples, validation_meta, test_samples, test_meta, **kwargs):
        utils.minmax_normalise_sets(train_samples, validation_samples, test_samples)
        return \
            (train_samples,), train_meta, \
            (validation_samples,), validation_meta, \
            (test_samples,), test_meta


    def train(self, train_x, validation_x, epochs, batch_size, **kwargs):
        self.model.fit(
            train_x, train_x, validation_data=(validation_x, validation_x), epochs=epochs, batch_size=batch_size,
            shuffle=True, initial_epoch=self.initial_epoch, callbacks=self.callbacks, verbose=2)


    def evaluate(self, x, meta, **kwargs):
        pass


    def test(self, test_x, meta, **kwargs):
        decoded_samples = self.model.predict(test_x)
        encoded_samples = self.export_encoder().predict(test_x)

        visualise_encoder_decoder(10, test_x, decoded_samples, encoded_samples, utils.sample_labels(meta))

        # save decoded samples
        # if self.verbose:
        #     output_dir = os.path.join('dump', 'tfsamples', datetime.now().strftime('%y-%m-%d_%H-%M-%S'))
        #     os.makedirs(output_dir, exist_ok=True)
        #     for i in range(0, len(samples), 50):
        #         np.save(os.path.join(output_dir, f'{i}_{np.argmax(labels[i])}.npy'), decoded_samples[i:i+50].reshape(50,32,32))


    def export_encoder(self):
        encoder = self.model.get_layer(self.encoder_layer_name).output
        # output = self._build_output_unit(encoder)
        output = layers.Conv2D(4, (1, 1), padding='same', activation='sigmoid')(encoder)

        encoder_model = keras.models.Model(inputs=[self.model.input], outputs=[output])
        # encoder_model = keras.models.Model(inputs=[self.model.input], outputs=[encoder])
        encoder_model.compile(optimizer='adadelta', loss='binary_crossentropy')
        return encoder_model


def visualise_encoder_decoder(n, samples, decoded, encoded, labels, vmin=None, vmax=None):
    if isinstance(n, list):
        idxs = n
        n = len(n)
    else:
        idxs = random.sample(range(len(samples)), n)
    
    if vmin is None:
        vmin = np.percentile(samples, 0.1)
    if vmax is None:
        vmax = np.percentile(samples, 99.9)

    fig, axes = plt.subplots(nrows=3, ncols=n)
    for i, k in enumerate(idxs):
        axes[0, i].set_title(str(labels[k]))
        im = axes[0, i].imshow(samples[k].reshape(samples.shape[1], samples.shape[2]), vmin=vmin, vmax=vmax)
        im = axes[1, i].imshow(decoded[k].reshape(decoded.shape[1], decoded.shape[2]), vmin=vmin, vmax=vmax)
        if len(encoded.shape) > 3:
            # im = axes[0, i].imshow(encoded[k].reshape(encoded.shape[3], encoded.shape[1]*encoded.shape[2]))
            im = axes[2, i].imshow(encoded[k].reshape(encoded.shape[1], encoded.shape[2]*encoded.shape[3]), vmin=vmin, vmax=vmax)
        else:
            im = axes[2, i].imshow(encoded[k].reshape(encoded.shape[1], encoded.shape[2]), vmin=vmin, vmax=vmax)
        plt.viridis()

    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
    fig.colorbar(im, cax=cbar_ax)
    plt.show()



