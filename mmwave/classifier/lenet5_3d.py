from mmwave.classifier import LeNet5, utils
from mmwave.metadata import SampleClassification
from tensorflow import keras
from tensorflow.python.keras import layers


class LeNet5_3D(LeNet5):

    def build(self, input_dims, **kwargs):
        in0 = layers.Input(shape=input_dims)
        x = layers.TimeDistributed(layers.Conv2D(filters=6, kernel_size=(5,5), activation='relu'), input_shape=(50,32,32))(in0)
        x = layers.TimeDistributed(layers.AveragePooling2D(pool_size=(2,2), strides=(2,2)))(x)
        x = layers.TimeDistributed(layers.Conv2D(filters=16, kernel_size=(5,5), activation='relu'))(x)
        x = layers.TimeDistributed(layers.AveragePooling2D(pool_size=(2,2), strides=(2,2)))(x)
        x = layers.Flatten()(x)
        x = layers.Dense(units=120, activation='relu')(x)
        x = layers.Dense(units=84, activation='relu')(x)
        out0 = layers.Dense(units=len(SampleClassification), activation='softmax')(x)

        model = keras.models.Model(inputs=[in0], outputs=[out0])
        model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
        return model
