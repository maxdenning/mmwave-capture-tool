import json
import os
import uuid
from enum import Enum
from hashlib import md5

import random

class SampleClassification(Enum):
    NONE = 0
    PEDESTRIAN = 1
    PEDEST_TWO = 2
    BICYCLE = 3
    CAR = 4


class SampleScenario(Enum):
    EMPTY_SCENE_FIELD                   = '0.0.a.i'
    EMPTY_SCENE_ROAD                    = '0.0.a.ii'
    S_PEDESTRIAN_STRAIGHT_RECEDING      = '1.0.a.i'  # S = single target
    S_PEDESTRIAN_STRAIGHT_APPROACHING   = '1.0.a.ii'
    S_PEDESTRIAN_DIAGONAL_RECEDING      = '1.0.b.i'
    S_PEDESTRIAN_DIAGONAL_APPROACHING   = '1.0.b.ii'
    S_BICYCLE_STRAIGHT_RECEDING         = '1.1.a.i'
    S_BICYCLE_STRAIGHT_APPROACHING      = '1.1.a.ii'
    S_BICYCLE_DIAGONAL_RECEDING         = '1.1.b.i'
    S_BICYCLE_DIAGONAL_APPROACHING      = '1.1.b.ii'
    S_CAR_STRAIGHT_RECEDING_FORWARD     = '1.2.a.i'
    S_CAR_STRAIGHT_RECEDING_REVERSE     = '1.2.a.ii'
    S_CAR_STRAIGHT_APPROACHING_FORWARD  = '1.2.a.iii'
    S_CAR_STRAIGHT_APPROACHING_REVERSE  = '1.2.a.iv'
    S_CAR_DIAGONAL_RECEDING_FORWARD     = '1.2.b.i'
    S_CAR_DIAGONAL_RECEDING_REVERSE     = '1.2.b.ii'
    S_CAR_DIAGONAL_APPROACHING_FORWARD  = '1.2.b.iii'
    S_CAR_DIAGONAL_APPROACHING_REVERSE  = '1.2.b.iv'
    M_PEDESTRIAN_STRAIGHT_RECEDING      = '2.0.a.i'  # M = multiple targets
    M_PEDESTRIAN_STRAIGHT_APPROACHING   = '2.0.a.ii'
    M_PEDESTRIAN_DIAGONAL_RECEDING      = '2.0.b.i'
    M_PEDESTRIAN_DIAGONAL_APPROACHING   = '2.0.b.ii'

    M_PEDESTRIAN_CAR_APPROACHING = '2.1.a.i'
    # TODO: the rest of the scenario codes


class SampleSet(Enum):
    TRAIN = 'training'
    VALIDATION = 'validation'
    TEST = 'testing'


class SampleMetaData(object):
    def __init__(self, sample_path, classification, scenario, sample_set, source_file, centre_range_bin, centre_doppler_bin):
        source_dir = os.path.dirname(source_file)
        if os.path.basename(source_dir) == 'slices':
            source_dir = os.path.dirname(source_dir)  # move up another directory

        self.data = sample_path.replace('\\', '/')
        self.classification = classification
        self.scenario = scenario
        self.sample_set = sample_set
        self.source_scene = source_file.replace('\\', '/')
        self.source_scene_video = os.path.join(source_dir, 'scene_video.avi').replace('\\', '/')
        self.id = md5((self.data + self.source_scene).encode('utf-8')).hexdigest()  # predictable and unique id
        self.centre_range_bin = centre_range_bin
        self.centre_doppler_bin = centre_doppler_bin

        if not all(map(os.path.isfile, [self.data, self.source_scene])):
            raise ValueError(f'Not all valid paths:\n\t{self.data}\n\t{self.source_scene}')

        with open(os.path.join(source_dir, 'meta.json'), 'r') as source_meta_file:
            source_meta = json.load(source_meta_file)
            self.time = source_meta['time']
            self.location = source_meta['location']
            self.participants = source_meta['participants']

    def properties(self):
        return {
            'id': self.id,
            'data': self.data,
            'class': self.classification.value,
            'scenario': self.scenario.value,
            'set': self.sample_set.value,
            'sourceScene': self.source_scene,
            'sourceSceneVideo': self.source_scene_video,
            'time': self.time,
            'location': self.location,
            'participants': self.participants,
            'centre_range_bin': self.centre_range_bin,
            'centre_doppler_bin': self.centre_doppler_bin
        }
    

class SampleDatabase(object):
    def __init__(self, filepath):
        self.database = self._open_database(filepath)
        self.samples = self.database['samples']  # sample dicts indexed by sample id
        self.training = self.database['training']  # sample ids of training set
        self.validation = self.database['validation']  # sample ids of validation set
        self.testing = self.database['testing']  # sample ids of testing set
    
    def __len__(self):
        return len(self.samples)

    def _open_database(self, filepath):
        self.source = filepath
        if os.path.isfile(self.source):
            with open(self.source, 'r') as db_file:
                return json.load(db_file)
        else:
            return json.loads('{ "samples": {}, "training": [], "validation": [], "testing":[] }')

    def add_sample(self, sample_metadata, overwrite=True):
        # check for ID conflicts
        if not overwrite and sample_metadata.id in self.samples:
            return False

        # write to database
        self.samples[sample_metadata.id] = sample_metadata.properties()
        return True
    
    def clear(self):
        self.samples = {}
        self.training = []
        self.validation = []
        self.testing = []

    def generate_sample_sets(self, validation_proportion):
        if validation_proportion > 1:
            raise ValueError('training set proportion + validation set proportion > 1')

        # reset validation labels before generation
        for sid in self.samples:
            if self.samples[sid]['set'] == SampleSet.VALIDATION.value:
                self.samples[sid]['set'] = SampleSet.TRAIN.value

        self.training = []
        self.validation = []
        self.testing = []
        for sid in self.samples:
            sample_set = SampleSet(self.samples[sid]['set'])
            if sample_set == SampleSet.TRAIN:
                self.training.append(sid)
            elif sample_set == SampleSet.TEST:
                self.testing.append(sid)

        # generate validation set from a random subset of training
        validation_idxs = sorted(random.sample(range(len(self.training)), int(len(self.training) * validation_proportion)))
        self.validation = [self.training[i] for i in validation_idxs]        
        
        for sid in self.validation:
            self.samples[sid]['set'] = SampleSet.VALIDATION.value  # alter validation sample set label

        for i in reversed(validation_idxs):
            del self.training[i]  # remove validation samples from training set

        # confirm sets are distinct
        for sid in self.samples:
            if sid in self.training and sid in self.validation:
                raise ValueError('sample id {} appears in both training and validation sets')


    def save(self):
        self.database['samples'] = self.samples
        self.database['training'] = self.training
        self.database['validation'] = self.validation
        self.database['testing'] = self.testing
        with open(self.source, 'w') as db_file:
            json.dump(self.database, db_file, indent=4)

    @property
    def training_set(self):
        return [self.samples[sid] for sid in self.training]

    @property
    def validation_set(self):
        return [self.samples[sid] for sid in self.validation]

    @property
    def testing_set(self):
        return [self.samples[sid] for sid in self.testing]

    @property
    def sample_set(self):
        return self.samples.values()
    
    def subset_list(self, sids):
        return [self.samples[sid] for sid in sids]

    def training_size(self, classification=None):
        if classification is not None:
            return len(list(filter(lambda s: s['class'] == classification, self.training_set)))
        else:
            return len(self.training)
    
    def validation_size(self, classification=None):
        if classification is not None:
            return len(list(filter(lambda s: s['class'] == classification, self.validation_set)))
        else:
            return len(self.validation)

    def test_size(self, classification=None):
        if classification is not None:
            return len(list(filter(lambda s: s['class'] == classification, self.testing_set)))
        else:
            return len(self.testing)
