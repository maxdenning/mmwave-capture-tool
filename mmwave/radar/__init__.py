from .radar_config import RadarConfig
try:
    # only available in windows
    from .radar_client import SimpleRadarClient
except ImportError:
    pass
