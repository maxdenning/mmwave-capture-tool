import json
import os
from math import sqrt


class RadarConfig(object):
    def __init__(self, json_config_path):
        if not os.path.isfile(json_config_path) or not os.path.exists(json_config_path):
            raise ValueError('json_config_path is not a file or does not exist')

        self.SPEED_LIGHT = 3e8
        self.source_path = json_config_path

        config_file = open(self.source_path, 'r')
        self.root_config = json.load(config_file)
        config_file.close()

        self.config = self.root_config['mmWaveDevices'][0]['rfConfig']
        self.profile = self.config['rlProfiles'][0]['rlProfileCfg_t']
        self.frame = self.config['rlFrameCfg_t']
        self.adc = self.config['rlAdcOutCfg_t']['fmt']

    def __str__(self):
        # return string of comma separated property names and values
        return ', \n'.join(
            "%s: %s" % (k, getattr(self, k)) for k,v in vars(RadarConfig).items() if isinstance(v, property))

    @property
    def source(self):
        return self.source_path

    @property
    def bandwidth_total(self):
        return self.profile['rampEndTime_usec'] * self.profile['freqSlopeConst_MHz_usec'] * 1e6 # to Hz

    @property
    def bandwidth_valid(self):
        # bandwidth over which all samples are collected
        return self.n_adc_samples / self.sample_rate * self.frequency_slope

    @property
    def complex_factor(self):
        # return self.adc['b2AdcOutFmt']
        return 2

    @property
    def doppler_res(self):
        # (chrip rate / doppler bins) * (wavelength / 2)
        # return ((self.n_chirp_loops_per_frame * (1/self.frame_period)) / self.n_chirp_loops_per_frame) * (self.wavelength / 2)
        # return self.wavelength / (2 * self.frame_period)  # when n_chirp_loops_per_frame == n_doppler_bins
        return 0.1717  # as calculated by the matlab tool, ^^ above formula is for physical minimum, maybe?

    @property
    def doppler_max(self):
        return (self.n_doppler_bins // 2) * self.doppler_res

    @property
    def frame_period(self):
        return self.frame['framePeriodicity_msec'] * 1e-3  # to sec

    @property
    def frame_size(self): # bytes
        bit_16_to_bytes = 2
        return self.n_adc_samples * self.complex_factor * self.nRx * bit_16_to_bytes * self.n_chirp_loops

    @property
    def frequency(self):
        # TODO: verify this against mmWave Studio output
        # end_freq = start_freq + (((ramp end - (idle + adc_start)) * slope) * 1e-3)
        #              GHz      +    usec    -  usec + usec        * MHz/usec   to GHz
        # ramp_start = self.profile['idleTimeConst_usec'] + self.profile['adcStartTimeConst_usec'] + self.profile['txStartTime_usec']        
        # ramp_time = self.profile['rampEndTime_usec'] - ramp_start  # usec
        # freq_delta = (ramp_time * self.profile['freqSlopeConst_MHz_usec']) * 1e-3  # to GHz/usec
        # avg_freq = self.profile['startFreqConst_GHz'] + (freq_delta/2)  # GHz
        # return avg_freq * 1e9

        return (self.profile['startFreqConst_GHz'] * 1e9) + (self.bandwidth_total / 2)  # Hz
    
    @property
    def frequency_slope(self): # hz/sec
        return self.profile['freqSlopeConst_MHz_usec'] * 1e12 # 1e6 / 1e-6 = 1e12

    @property
    def n_adc_bits(self):
        opt = self.adc['b2AdcBits']
        return [12, 14, 16][opt]

    @property
    def n_adc_samples(self):
        return self.profile['numAdcSamples']

    @property
    def n_angle_bins(self):
        return 16 # TODO: this needs to be calculated (using nTx?)

    @property
    def n_chirp_loops(self):
        return self.frame['numLoops']

    @property
    def n_chirp_loops_per_frame(self):
        # n_chirp_loops corresponds to the number of chirp loops composed of TX1 and TX2 chirps
        # therefore number of chirp loops per TX is (n_chirp_loops / nTx)
        return int(self.n_chirp_loops / self.nTx)

    @property
    def n_doppler_bins(self):
        # nearest power of 2 >= n chrip loops per frame
        # TODO: include bin resolution somewhere? in postprocessing?
        return 1<<(self.n_chirp_loops_per_frame - 1).bit_length()

    @property
    def n_range_bins(self):
        # nearest power of 2 >= n adc samples
        # TODO: include bin resolution somewhere? in postprocessing?
        return 1<<(self.n_adc_samples - 1).bit_length()

    @property
    def norm_factor(self):
        return sqrt(2)/(2**(self.n_adc_bits-1)-1)

    @property
    def nRx(self):
        return 4  # self.config['rlChanCfg_t']['rxChannelEn']

    @property
    def nTx(self):
        # TODO: figure out how to interpret the Rx/Tx properties
        tx_hex = self.config['rlChanCfg_t']['txChannelEn']
        if tx_hex == '0x1':
            return 1
        elif tx_hex == '0x3':
            return 2
        else:
            raise ValueError('Unrecognised nTx: ' + tx_hex)

    @property
    def range_max(self):
        return (self.sample_rate / self.frequency_slope) * self.SPEED_LIGHT / 2

    @property
    def range_res(self):
        # physical minimun
        return self.SPEED_LIGHT / (2 * self.bandwidth_valid)

    @property
    def sample_rate(self):
        return self.profile['digOutSampleRate'] * 1e3  # output in samples/sec

    @property
    def wavelength(self):
        return self.SPEED_LIGHT / self.frequency


if __name__ == '__main__':
    conf = RadarConfig('/home/max/git/mmwave-capture-tool/config/config.mmwave.json')
    print(conf)
    # print(', \n'.join("%s: %s" % (k, getattr(conf, k)) for k,v in vars(RadarConfig).items() if isinstance(v, property)))
