import os
import win32com.client
from multiprocessing import Process, Queue

'''
Interacts with the mmwave studio lua radar interface via the MSMQ.
Requires the corresponding lua script (MSMQCommandListener.lua) be running in mmwave studio.
'''

class SimpleMSMQSender:
    MSG_EXIT = 'STOP'

    def __init__(self, pathname):
        MQ_SEND_ACCESS = 2
        MQ_DENY_NONE = 0
        self.queueinfo = win32com.client.Dispatch('MSMQ.MSMQQueueInfo')
        self.queueinfo.PathName = pathname
        self.queue = self.queueinfo.Open(MQ_SEND_ACCESS, MQ_DENY_NONE)

    def send(self, label, body):
        msg = win32com.client.Dispatch('MSMQ.MSMQMessage')
        msg.Label = label
        msg.Body = body
        msg.Send(self.queue)

    def close(self):
        self.queue.Close()
    
    @staticmethod
    def run(msmq_pathname, outbound_message_queue):
        msmq_sender = SimpleMSMQSender(msmq_pathname)

        while True:
            msg = outbound_message_queue.get()
            msmq_sender.send(msg[0], msg[1])

            if msg[1] == SimpleMSMQSender.MSG_EXIT:
                break

        msmq_sender.close()


class SimpleRadarClient:
    def __init__(self):
        self.msmq_path = '.\\private$\\mmwave-message-interface'
        self.label = 'py2lua'
        self.message_queue = Queue()
        self.msmq_process = Process(target=SimpleMSMQSender.run, args=(self.msmq_path, self.message_queue,))
        self.msmq_process.start()
    
    def __enqueue(self, msg):
        self.message_queue.put((self.label, msg))

    def close(self):
        self.__enqueue(SimpleMSMQSender.MSG_EXIT)
        self.msmq_process.join()

    def sleep(self, ms):
        self.__enqueue('RSTD.Sleep(' + str(ms) + ')')

    def begin_capture(self, path):
        self.__enqueue('ar1.CaptureCardConfig_StartRecord("' + path.replace('\\', '\\\\') + '", 1)')
        self.sleep(1000)
        self.__enqueue('ar1.StartFrame()')
        self.sleep(1000)

    def end_capture(self):
        self.__enqueue('ar1.StopFrame()')
        self.sleep(1000)
        self.__enqueue('ar1.CaptureCardConfig_StopRecord()')
        self.sleep(1000)
