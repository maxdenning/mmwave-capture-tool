import numpy as np
import os
import sys
from functools import partial
from mmwave.postprocessing import postprocessing, utils
from mmwave.radar import RadarConfig
from multiprocessing import Pool, cpu_count


def autoslice(scene, radar_config, rbin_min, rbin_max, vbin_offset_min, vbin_offset_max):
    zero_vbin = scene.shape[2] / 2
    postprocessing.zero_vbin_removal(scene, radar_config, val=utils.POSTPROC_dB_BASELINE)

    # check to see if the highest peak of the frame is within the rectangle described by rbin min/max and vbin min/max
    def peak_in_threshold_rect(frame, i):
        peaks = utils.peaks_of_rdm(frame, npeaks=1, m=9, n=9, P_fa=0.528)
        if not peaks:
            return False

        rbin, vbin = peaks[0]
        vbin_offset = abs(zero_vbin - vbin)

        # print(i, rbin, vbin, vbin_offset, '{:.3f}'.format(frame[rbin, vbin]), rbin_min < rbin < rbin_max, vbin_offset_min < vbin_offset < vbin_offset_max)

        if rbin_min < rbin < rbin_max and vbin_offset_min < vbin_offset < vbin_offset_max:
            return True

        return False
    
    # for start in range(slices[0], slices[1]):
    for start in range(len(scene)):
        if peak_in_threshold_rect(scene[start], start):
            break

    # for end in range(slices[1], slices[0], -1):
    for end in range(len(scene)-1, -1, -1):
        if peak_in_threshold_rect(scene[end], end):
            break

    return start, end + 1


if __name__ == '__main__':
    if len(sys.argv) < 8 or (len(sys.argv)-6) % 2:
        print(f'usage: {sys.argv[0]} <input file> <rmin> <rmax> <voffsetmin> <voffsetmax> <output path 0> <start 0>:<end 0> ... <output N> <start N>:<end N>')

    input_filepath = sys.argv[1]
    rmin, rmax, vbin_offset_min, vbin_offset_max = sys.argv[2:6]
    slices = [(sys.argv[i], *tuple(int(v) for v in sys.argv[i+1].split(':')),) for i in range(6, len(sys.argv), 2)]

    if not os.path.isfile(input_filepath):
        raise ValueError(f'input filepath is not a file: "{input_filepath}"')

    radar_config = RadarConfig(utils.get_radar_config_path(os.path.dirname(input_filepath)))
    scene = np.load(input_filepath)

    # auto-slice arguments
    range_scale = radar_config.n_range_bins / radar_config.range_max
    rmin = int(rmin) * range_scale
    rmax = int(rmax) * range_scale
    vbin_offset_min = int(vbin_offset_min)
    vbin_offset_max = int(vbin_offset_max)

    frames_per_sample = utils.SAMPLE_LENGTH_SEC / radar_config.frame_period

    slice_marker_dir = os.path.dirname(os.path.dirname(slices[0][0]))
    os.makedirs(slice_marker_dir, exist_ok=True)
    slice_marker_file = open(os.path.join(slice_marker_dir, 'slice_markers.csv'), 'w+')

    with Pool(cpu_count()) as pool:
        auto_fn = partial(autoslice, radar_config=radar_config, rbin_min=rmin, rbin_max=rmax, vbin_offset_min=vbin_offset_min, vbin_offset_max=vbin_offset_max)

        for i, (start, end) in enumerate(pool.imap(auto_fn, [scene[s:e] for o, s, e in slices])):
            start = slices[i][1] + start
            end = slices[i][1] + end
            print(f'Autoslice: Start({slices[i][1]}->{start}), End({slices[i][2]}->{end}), Samples({(end-start)//frames_per_sample})')
            os.makedirs(os.path.dirname(slices[i][0]), exist_ok=True)
            np.save(slices[i][0], scene[start:end])
            slice_marker_file.write(f'{slices[i][0]},{start},{end}\n')

        slice_marker_file.close()