# Source for pyMOT:
# https://github.com/Videmo/pymot


import json
import numpy as np
import os
import sys

from external.pyMOT.pymot import MOTEvaluation



def import_csv(filepath):
    indexed_windows = {}
    with open(filepath, 'r') as csv:
        lines = csv.readlines()

    for line in lines:
        n = [int(v) for v in line.strip().split(',')]
        indexed_windows[n[0]] = (n[1], n[2], n[3], n[4])
    return indexed_windows


def populate_json(windows, truth):
    frames = []
    for i in windows:
        inf = {
            'height': float(windows[i][3]),
            'width': float(windows[i][2]),
            'id': 'target',
            'y': float(windows[i][1] + (windows[i][3] / 2)),
            'x': float(windows[i][0] + (windows[i][2] / 2)),
        }
        key = 'hypotheses'
        if truth:
            inf['dco'] = True
            key = 'annotations'
        frame = {}
        frame['timestamp'] = float(i)
        frame['class'] = 'frame'
        frame['num'] = float(0)
        frame[key] = [inf]
        frames.append(frame)
    return [{'class': 'video', 'filename': 'doot', 'frames' : frames}]





if __name__ == '__main__':
    # if len(sys.argv) < 2:
    #     print('usage: ' + sys.argv[0] + ' <truth> <hypothesis>')
    #     exit(1)

    truth_filepath = sys.argv[1]
    # hyopthesis_filepath = sys.argv[2]

    truth = import_csv(truth_filepath)
    # hypothesis = import_csv(hyopthesis_filepath)

    truth_json = populate_json(truth, True)
    # hypothesis_json = populate_json(truth, False)

    # print(json.dumps(truth_json, indent=4)[:1000])
    # print(json.dumps(hypothesis_json, indent=4))

    with open(os.path.join(os.path.dirname(truth_filepath), 'groundtruth.json'), 'w') as ofile:
        json.dump(truth_json, ofile, indent=4)

    # with open(os.path.join(os.path.dirname(hyopthesis_filepath), 'hypothesis.json'), 'w') as ofile:
    #     json.dump(hypothesis_json, ofile, indent=4)


    # evaluator = MOTEvaluation(truth_json[0], hypothesis_json[0], 1.0)
    # print(evaluator.getMOTA())
    # print(evaluator.getMOTP())
    # print(evaluator.getRelativeStatistics())
    # print(evaluator.getAbsoluteStatistics())

