import matplotlib.pyplot as plt
import numpy as np
import os
import sys
from mmwave.metadata import SampleScenario
from mmwave.postprocessing import tracking

# Based on:
# https://arxiv.org/pdf/1502.05803.pdf


def import_csv_as_dict(filepath, f_indices=None):
    indexed_windows = {}
    with open(filepath, 'r') as csv:
        lines = csv.readlines()

    for line in lines:
        n = [int(v) for v in line.strip().split(',')]
        if not f_indices or n[0] in f_indices:
            indexed_windows[n[0]] = (n[1], n[2], n[3], n[4])
    return indexed_windows


def window_centres(window_dict):
    offset = min(window_dict)
    cen = np.zeros((len(window_dict), 2), dtype=float)
    for t in sorted(window_dict.keys()):
        x, y, w, h = window_dict[t]
        cen[t - offset] = [x+(w/2), y+(h/2)]
    return cen


def centre_error_rms(gtc, hpc):
    # root mean square error
    #   RMSE(ΛG,ΛT) =√[(1/N)(∑t=1‖xGt−xHt‖^2)]
    return np.sqrt( np.sum( np.square( np.linalg.norm(gtc - hpc, axis=1))) / len(gtc))


def centre_error_by_frame(gtc, hpc):
    return np.linalg.norm(gtc - hpc, axis=1)


def window_intersection(a, b):
    # Based on:
    # https://github.com/SFML/SFML/blob/12d81304e63e333174d943ba3ff572e38abd56e0/include/SFML/Graphics/Rect.inl#L109

    # input form is tuple(left, bottom, width, height)
    # negative dimensions are not accepted

    aleft, abottom, awidth, aheight = a
    bleft, bbottom, bwidth, bheight = b
    ileft = max(aleft, bleft)
    ibottom = max(abottom, bbottom)
    iright = min(aleft + awidth, bleft + bwidth)
    itop = min(abottom + aheight, bbottom + bheight)
    
    if ileft >= iright or ibottom >= itop:
        return False
    return (ileft, ibottom, iright-ileft, itop-ibottom)


def calculate_region_overlap_measures(gtw, hpw):
    # pixel-wise metrics
    overlap = window_intersection(gtw, hpw)
    overlap_area = overlap[2] * overlap[3] if overlap else 0.0
    gt_area = gtw[2] * gtw[3]
    hp_area = hpw[2] * hpw[3]

    tp = overlap_area
    fp = hp_area - overlap_area
    fn = gt_area - overlap_area
    # tn = (512 * 128) - (tp + fp + fn)  # not useful as it is so large, most metrics tend to 1.0

    overlap_measure = tp / (tp + fn + fp)  # (rGt intersect rHt) / (rGt union rHt)
    precision = tp / (tp + fp)
    # true positive rate has been added beyond what is defined in the source paper, as in this context most hypothesis
    # windows will entirely contain the smaller ground truth window
    true_positive_rate = tp / (tp + fn)
    f1_score = 2 * (precision * true_positive_rate) / (precision + true_positive_rate) if overlap_area > 0.0 else 0.0
    return overlap_measure, precision, true_positive_rate, f1_score


def region_overlap_measures(groundtruths, hypotheses):
    results = {k: calculate_region_overlap_measures(groundtruths[k], hypotheses[k]) for k in groundtruths}
    mean_overlap = sum(results[k][0] for k in results) / len(groundtruths)
    mean_precision = sum(results[k][1] for k in results) / len(groundtruths)
    mean_tpr = sum(results[k][2] for k in results) / len(groundtruths)
    mean_f1 = sum(results[k][3] for k in results) / len(groundtruths)
    return mean_overlap, mean_precision, mean_tpr, mean_f1


def calculate_metrics(groundtruth_filepath, hypothesis_filepath):
    groundtruth = import_csv_as_dict(groundtruth_filepath, None) # 0, 1000)
    hypothesis = import_csv_as_dict(hypothesis_filepath, groundtruth.keys()) # min(groundtruth), max(groundtruth)+1)
    if len(groundtruth) != len(hypothesis):
        raise ValueError(f'Lengths not equal: gt={groundtruth_filepath} ({len(groundtruth)}), hp={hypothesis_filepath} ({len(hypothesis)})')

    if sorted(groundtruth.keys()) != sorted(hypothesis.keys()):
        raise ValueError(f'Frame keys do not match!')

    results = {}
    results['centre_error_rms'] = centre_error_rms(window_centres(groundtruth), window_centres(hypothesis))
    results['overlap_mean'], \
    results['overlap_precision_mean'], \
    results['overlap_tpr_mean'], \
    results['overlap_f1_mean'] = region_overlap_measures(groundtruth, hypothesis)
    return results


def hypothesis_path(scenario_dir):
    with open(os.path.join(scenario_dir, 'source.txt')) as source:
        data_source_filepath = source.read(1024)
    scene_dir = os.path.dirname(os.path.dirname(data_source_filepath))
    slice_name = os.path.basename(data_source_filepath).split('.')[0]
    return os.path.join(scene_dir, 'samples', '{}', slice_name, 'track.csv')


def graph_results(input_dir, output_filepath):
    with open(os.path.join(input_dir, 'results.csv'), 'r') as ifile:
        results_csv = ifile.readlines()[1:]  # leave out header line
    results = {'fcs': [], 'fp': [], 'dcsx': [], 'dcsn': []}
    for line in results_csv:
        values = line.split(',')
        results[values[0]].append(values[2])

    plt.hist(results['fcs'])
    plt.hist(results['fp'])
    plt.hist(results['dcsx'])
    plt.hist(results['dcsn'])
    plt.show()


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print(f'usage: {sys.argv[0]} <input dir> <output file> <graph>')
        exit(1)
    input_dir = sys.argv[1]
    output_filepath = sys.argv[2]

    if len(sys.argv) > 3:
        graph_results(input_dir, output_filepath)
        exit(0)

    tracker_results = {'fcs': {}, 'fp': {}, 'dcsx': {}, 'dcsn': {}}
    for s in SampleScenario:
        groundtruth_path = os.path.join(input_dir, s.name, 'groundtruth.csv')
        if not os.path.isfile(groundtruth_path):
            print(f'Not Found: {s.name}')
            continue
        hypothesis_path_fmt = hypothesis_path(os.path.join(input_dir, s.name))
        tracker_results['fcs'][s] = calculate_metrics(groundtruth_path, hypothesis_path_fmt.format('camshift'))
        tracker_results['fp'][s] = calculate_metrics(groundtruth_path, hypothesis_path_fmt.format('peaks'))
        tracker_results['dcsx'][s] = calculate_metrics(groundtruth_path, hypothesis_path_fmt.format('dynamic_max'))
        tracker_results['dcsn'][s] = calculate_metrics(groundtruth_path, hypothesis_path_fmt.format('dynamic_min'))

    with open(output_filepath, 'w') as ofile:
        ofile.write(
            'tracking_type,scenario_code,centre_error_rms,region_overlap_mean,region_overlap_precision_mean,'
            'region_overlap_true_positive_rate_mean,region_overlap_f1_mean\n')  # TODO: update headers
        for tracking_type, scenario_results in tracker_results.items():
            for scenario, results in scenario_results.items():
                ofile.write(
                    f'{tracking_type},{scenario.name},{results["centre_error_rms"]},'
                    f'{results["overlap_mean"]},{results["overlap_precision_mean"]},{results["overlap_tpr_mean"]},'
                    f'{results["overlap_f1_mean"]}\n')
