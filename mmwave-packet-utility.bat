@echo off

REM     Recursive search of files under root dir matching "*adc_Raw_*.bin"
REM     Iterate over these, get path and file name (without extension)
REM     Put into packet reorder utility
REM         Packet_Reorder_Zerofill.exe <in file> <out file> <log file>
REM     Exmaple files produced for adc_Raw_0.bin,
REM         adc_Raw_0_pp.bin, adc_Raw_0_pp_pkt.log

setlocal enabledelayedexpansion

REM set PACKET_UTILITY=C:\ti\mmwave_studio_02_00_00_02\mmWaveStudio\PostProc\Packet_Reorder_Zerofill.exe
set PACKET_UTILITY=Packet_Reorder_Zerofill.exe
set ROOTDIR=%1
echo Searching: %ROOTDIR%
echo Found:
dir /s/b %ROOTDIR%\*adc_Raw_?.bin
pause
echo .

for /f %%F in ('dir /s/b %ROOTDIR%\*adc_Raw_?.bin') do (
    set DIRNAME=%%~dpF
    set FILENAME=%%~nF
    REM File name with extension is %%~nxf

    echo .
    echo Processing: %%F
    echo Output: !DIRNAME!!FILENAME!_pp.bin !DIRNAME!!FILENAME!_pp_pkt.log
    %PACKET_UTILITY% %%F !DIRNAME!!FILENAME!_pp.bin !DIRNAME!!FILENAME!_pp_pkt.log
)
