@echo off
REM get date and time for log file naming
For /f "tokens=1-3 delims=/ " %%a in ('date /t') do (set mydate=%%c-%%b-%%a)
For /f "tokens=1-3 delims=/:" %%a in ("%TIME%") do (set mytime=%%a:%%b:%%c)

REM launch capture tool
call workon mmwave-capture3
SET logfile=E:\\radar-project\\logs\\capture_%mydate%_%mytime%.log
python mmwave-capture-tool.py > %logfile% 2>&1
exit
