import matplotlib.pyplot as plt
plt.ion()  # Note this correction
import processData
import socket
from thread_flag import read_flag


def runUDPClient(jflag, rawDataQueue):
    """ Connect to the ADC port and parse+signal processing on data."""
    print('hi2')
    # Create UDP socket to the data port.
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_address = ('192.168.33.30', 4098)
    sock.bind(server_address)

    try:
        fileName = 'adc_data.bin'
        with open(fileName, 'wb') as f:

            # Receive response over ethernet.
            print('Waiting to receive')

            # Define the data downloaded and initialise to 0.

            # Initialise some variables based on first data received.
            for i in range(0,500):
                data, server = sock.recvfrom(4096)
            
            print('Receiving')

            # TODO: use this for saving raw data?
            #f.write(data)

            # Get the sequence number and byte count of this UDP message.
            seqNumb = int.from_bytes(data[0:4], byteorder='little')
            byteCount = int.from_bytes(data[4:10], byteorder='little')  # This lags by at least set of data transmitted.
            adcDataSize = len(data[10:])
            prevFrameID = int((byteCount + adcDataSize) / processData.FRAME_SIZE_BYTES)
            prevSeqNumb = seqNumb
            seqIDFlag = True # Assume this is true until proven false.

            # Store the data from this first download.
            nextFrameData = bytearray()
            nextFrameData.extend(data[10:])

            # while True:
            while not read_flag(jflag):
                # Retrieve a UDP message.
                data, server = sock.recvfrom(4096)

                # Get the sequence number and byte count of this UDP message.
                seqNumb = int.from_bytes(data[0:4], byteorder='little')
                byteCount = int.from_bytes(data[4:10], byteorder='little') # This lags by at least set of data transmitted.
                adcDataSize = len(data[10:])

                #print(seqNumb)

                # Perform a check that no UDP packets are lost. This ignores an out of sequence arrival which should really be
                # checked as well.
                if(seqNumb -1 != prevSeqNumb):
                    seqIDFlag = False

                # Check to see whether a new frames worth of data is available. The new frame can start lie somewhere within the
                # latest message.
                curFrameID = int( (byteCount+adcDataSize) / processData.FRAME_SIZE_BYTES  )
                

                # print('LEN C   : [' + str(processData.FRAME_SIZE_BYTES) + '] ' + str(len(nextFrameData)))
                # print('SEQ P, S: ' + str(prevSeqNumb) + ', ' + str(seqNumb) + ', ' + str(seqIDFlag))
                # print('FRM P, S: ' + str(prevFrameID) + ', ' + str(curFrameID))



                
                if(prevFrameID < curFrameID):
                    #print(curFrameID)

                    #print("HAVE A FRAME TRANSITION.")
                    #print("Seq numb %d, byte count %d." % (seqNumb, byteCount))

                    # If this frame is a tansition then define the point in it which corresponds to all data.
                    endOfFrameDataIdx = curFrameID * processData.FRAME_SIZE_BYTES - byteCount

                    # Check if sequence numbers are good.
                    if(seqIDFlag == True):
                        #print("END OF DATA LIES IN THIS FRAME!!!")
                        # Extract the current frame of data to process.
                        #endOfFrameDataIdx = (byteCount+adcDataSize) % processData.FRAME_SIZE_BYTES
                        nextFrameData.extend(data[10:endOfFrameDataIdx+10])

                        # Do radar signal processing on this frame of data.
                        if(len(nextFrameData) == processData.FRAME_SIZE_BYTES):
                            #processData.processFrame(nextFrameData, dataQueue)

                            # print('LEN C   : [' + str(processData.FRAME_SIZE_BYTES) + '] ' + str(len(nextFrameData)))
                            # print('SEQ P, S: ' + str(prevSeqNumb) + ', ' + str(seqNumb) + ', ' + str(seqIDFlag))
                            # print('FRM P, S: ' + str(prevFrameID) + ', ' + str(curFrameID))
                            # print('got [%i %i] [%i %i]' % (prevSeqNumb, seqNumb, prevFrameID, curFrameID))

                            if rawDataQueue.qsize() <= 1:  # NOTE: this is interesting
                                rawDataQueue.put(nextFrameData)
                            #f.write(nextFrameData)

                        # Store any surplus data transmitted for the next frame.
                        nextFrameData = bytearray()
                        nextFrameData.extend(data[10+endOfFrameDataIdx:])

                    # Missing data therefore don't process this data.
                    else:
                        # Reset the sequence ID flag.
                        seqIDFlag = True

                        # Reset the amount of data for the next download.
                        nextFrameData = bytearray()
                        # Extend it by any data from this packet.
                        nextFrameData.extend(data[10+endOfFrameDataIdx:])

                        # print('drop [%i %i] [%i %i]' % (prevSeqNumb, seqNumb, prevFrameID, curFrameID))

                    # Update the previous frame ID to the current frame.
                    prevFrameID = curFrameID

                else:
                    # Just update the data with all of this packet.
                    nextFrameData.extend(data[10:])

                # Update the previous sequence number record to the current one.
                prevSeqNumb = seqNumb

    finally:
        print('closing socket')
        sock.close()
