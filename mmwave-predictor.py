import importlib
network_opts = importlib.import_module('mmwave-trainer', package='.').network_opts
import numpy as np
import os
import sys
from mmwave.classifier import utils as cl_utils


def load_sample(sample_filepath, as_frames):
    # input must be (x,y), or (n, x, y), or (n, x, y, c)
    sample = np.load(sample_filepath)

    if len(sample.shape) < 2 or len(sample.shape) > 4:
        raise ValueError(f'Invalid sample dimensions: {sample.shape}')

    elif as_frames and len(sample.shape) == 2:
        sample = sample.reshape(1, *sample.shape, 1)  # (x, y) -> (n, x, y, c)

    elif as_frames and len(sample.shape) == 3:
        sample = sample.reshape(*sample.shape, 1)  # (n, x, y) -> (n, x, y, c)

    elif not as_frames and len(sample.shape) == 3:
        sample = sample.reshape(1, *sample.shape, 1)

    return sample  # output = (n, x, y, c)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print(f'usage: {sys.argv[0]} <network> <model file> <sample file>')
        print(f'valid networks: {", ".join(network_opts().keys())}')
        exit(1)
    
    network_name = sys.argv[1].lower()
    if network_name not in network_opts():
        print(f'network does not exist: {network_name}')
        exit(1)
    model_filepath = sys.argv[2]
    sample_filepath = sys.argv[3]

    net_opts = network_opts()[network_name]
    network_type = net_opts['type']
    network_args = net_opts['args']

    verbose = False
    test_samples = load_sample(sample_filepath, net_opts['frames'])
    placeholder_samples = np.zeros((1, 1, 1, 1))
    placeholder_meta = []

    if verbose:
        print('\nSample Sets:')
        cl_utils.print_sample_set_description(None, None, test_samples)


    # preprocess sample sets
    _, _, _, _, (test_samples, _), _ = network_type.preprocess(
        placeholder_samples, placeholder_meta, placeholder_samples, placeholder_meta, test_samples, placeholder_meta, verbose=verbose, **network_args)

    if verbose:
        print('\nPreprocessed Sample Sets:')
        cl_utils.print_sample_set_description(None, None, test_samples)

    # initialise classifier
    net = network_type(checkpoints=None, logs=None, load=model_filepath, verbose=verbose, input_dims=test_samples.shape[1:], **network_args)
    if verbose:
        print('')
        net.summary()

    # test network with sample
    predictions = net.predict(test_samples, **network_args)
    print(f'\nResult:\n{"Classification":^20}{"Confidence":^20}')
    print('\n'.join(f'{p[0].name:^20}{p[1]:^20.5f}' for p in predictions))
