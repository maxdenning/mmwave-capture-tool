import cmapy
import cv2 as cv
import numpy as np
import os
import sys
from mmwave.postprocessing import postprocessing, utils
from mmwave.radar import RadarConfig


def playback(scene, windows, frame_period, xres, yres):
    cv.namedWindow('img', cv.WINDOW_NORMAL)
    cv.resizeWindow('img', xres, yres)

    for i in range(len(scene)):
        if i not in windows:
            print(f'Frame not found: {i}')
            continue
        y,x,h,w = windows[i]  # swap x and y
        img = cv.applyColorMap(postprocessing.rdm_to_greyscale(scene[i]), cmapy.cmap('viridis'))
        img2 = cv.rectangle(img, (x,y), (x+w, y+h), 255, 1)
        rotimg = cv.rotate(img2, cv.ROTATE_90_COUNTERCLOCKWISE)
        cv.imshow('img', rotimg)
        if cv.waitKey(int(frame_period * 1000)) > -1:
            break

    cv.destroyAllWindows()


if __name__ == '__main__':
    if len(sys.argv) < 4:
        print(f'usage: {sys.argv[0]} <playback multiplier> <scene> <tracker type> <config>')
        exit(1)

    playback_multiplier = float(sys.argv[1])
    scene_filepath = sys.argv[2]
    tracker_subdir = sys.argv[3]
    track_filepath = os.path.join(os.path.dirname(scene_filepath), '..', 'samples', tracker_subdir, os.path.splitext(os.path.basename(scene_filepath))[0], 'track.csv')

    conf_filepath = sys.argv[4] if sys.argv[4] != '.' else utils.DEFAULT_RADAR_CONFIG  # if '.' use default

    if not all(map(os.path.isfile, [scene_filepath, track_filepath, conf_filepath])):
        raise ValueError(f'input file paths do not exist, or are not files: {[scene_filepath, track_filepath, conf_filepath]}')

    scene = np.load(scene_filepath)
    windows = utils.read_target_track(track_filepath)
    radar_config = RadarConfig(conf_filepath)

    scene = postprocessing.high_pass_filter(scene, cutoff_db=-32.0, val=-32.0)
    playback(scene, windows, radar_config.frame_period / playback_multiplier, radar_config.n_range_bins*2, radar_config.n_doppler_bins*2)
