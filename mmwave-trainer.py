import getopt
import numpy as np
import os
import random
import sys
import tensorflow as tf
from datetime import datetime
from mmwave import classifier
from mmwave.classifier import utils as cl_utils
from mmwave.metadata import SampleDatabase, SampleClassification
from mmwave.postprocessing import postprocessing, utils as pp_utils
from tensorflow import keras
from tensorflow.python import debug as tf_debug


def load_set(sample_set, x, y, sample_length, frames=False):
    multipier = sample_length if frames else 1
    meta = [{'class': 0, 'scenario': '0.0.a.i', 'participants': []}] * (len(sample_set) * multipier)

    if frames:
        samples = np.zeros((len(sample_set) * sample_length, x, y))
    else:
        samples = np.zeros((len(sample_set), sample_length, x, y))
    
    m = 0
    for i, sample in enumerate(sample_set):
        if not os.path.exists(sample['data']):
            m += 1
            continue
        j = slice(i * multipier, (i * multipier) + multipier) if frames else i
        samples[j] = np.load(sample['data'])
        meta[j] = [sample] * multipier if frames else sample

    if m > 0:
        print(f'Missed inputs: {m}/{len(sample_set)}')

    samples = samples[:, :, :, np.newaxis] if frames else samples[:, :, :, :, np.newaxis]

    shuffle_idx = np.random.permutation(len(samples))  # shuffle both samples and meta in the same way
    return samples[shuffle_idx], [meta[i] for i in shuffle_idx]


# def load_sample_sets(sample_database, frames):
#     sample_length, sample_x, sample_y = np.load(sample_db.training_set[0]['data']).shape  # get sample dimensions
#     print('Loading training set')
#     train_samples, train_meta = load_set(sample_db.training_set, sample_x, sample_y, sample_length, frames=frames)
#     print('Loading validation set')
#     validation_samples, validation_meta = load_set(sample_db.validation_set, sample_x, sample_y, sample_length, frames=frames)
#     print('Loading testing set')
#     test_samples, test_meta = load_set(sample_db.testing_set, sample_x, sample_y, sample_length, frames=frames)
#     return train_samples, train_meta, validation_samples, validation_meta, test_samples, test_meta


def load_sample_sets_cross_validation(sample_db, as_frames, k, n):#, val_prop=0.15, test_prop=0.2):
    # cross_validation_participant_sets = (
    outer_fold_participant_test_set_config = (
        ([2], [2,3], [10], [6]),  # set A (set as described by labels in scene_data.py)
        ([1], [8,9],  [5], [7]),  # set B
        ([3], [2,3],  [4], [6]))  # set C
    l, x, y = np.load(list(sample_db.samples.values())[0]['data']).shape  # get sample dimensions


    for o, (otrain, otest) in enumerate(cl_utils.outer_fold_validation(sample_db.samples, outer_fold_participant_test_set_config)):
        for i, (itrain, ivalid) in enumerate(cl_utils.inner_fold_validation(otrain, k, n)):
            print('Loading sample sets')
            tr_samples, tr_meta = load_set(sample_db.subset_list(itrain), x, y, l, as_frames)
            vl_samples, vl_meta = load_set(sample_db.subset_list(ivalid), x, y, l, as_frames)
            ts_samples, ts_meta = load_set(sample_db.subset_list(otest), x, y, l, as_frames)
            yield o, i, tr_samples, tr_meta, vl_samples, vl_meta, ts_samples, ts_meta


    # for tr, vl, ts in cl_utils.cross_validate_sample_sets(sample_db.samples, cross_validation_participant_sets, val_prop, test_prop):
    #     print('Loading sample sets')
    #     tr_samples, tr_meta = load_set(sample_db.subset_list(tr), x, y, l, as_frames)
    #     vl_samples, vl_meta = load_set(sample_db.subset_list(vl), x, y, l, as_frames)
    #     ts_samples, ts_meta = load_set(sample_db.subset_list(ts), x, y, l, as_frames)
    #     yield tr_samples, tr_meta, vl_samples, vl_meta, ts_samples, ts_meta


def set_logging(log_dir):
    if not log_dir:
        return
    sys.stdout = cl_utils.Logger(log_dir)
    sys.stderr = cl_utils.Logger(log_dir, stderr=True)


def execute_network(
        checkpoint_dir, epochs, load_path, log_dir, network_type, save_model_path, save_weights_path, verbose,
        train_samples, train_meta, validation_samples, validation_meta, test_samples, test_meta, **network_args):

    if verbose:
        print('\nSample Sets:')
        cl_utils.print_sample_set_description(train_samples, validation_samples, test_samples)
        cl_utils.print_sample_set_distribution(train_meta, validation_meta, test_meta)

    # preprocess sample sets
    train_params, train_meta, validation_params, validation_meta, test_params, test_meta = network_type.preprocess(
        train_samples, train_meta, validation_samples, validation_meta, test_samples, test_meta, verbose=verbose, load=load_path, **network_args)

    if verbose:
        print('\nPreprocessed Sample Sets:')
        cl_utils.print_sample_set_description(train_params[0], validation_params[0], test_params[0])
        cl_utils.print_sample_set_distribution(train_meta, validation_meta, test_meta)

    # initialise classifier
    net = network_type(checkpoints=checkpoint_dir, logs=log_dir, load=load_path, verbose=verbose, input_dims=train_params[0].shape[1:], **network_args)
    if verbose:
        print('')
        net.summary()

    # train the network
    if epochs > 0:
        if verbose and load_path:
            print('WARNING: loading from weights file does not include optimiser state so further training may fail!')
        net.train(*train_params, *validation_params, epochs=epochs, **network_args)

    # output the model or weights
    if save_model_path:
        net.save(save_model_path, model=True)
        net.save(save_weights_path, model=False)

    # test network performance
    print('\nEvaluation (Train):')
    net.evaluate(*train_params, train_meta, **network_args)

    print('\nEvaluation (Validation):')
    net.evaluate(*validation_params, validation_meta, **network_args)

    print('\nEvaluation (Test):')
    net.evaluate(*test_params, test_meta, **network_args)

    print('\nTesting:')
    results_output_filepath = None
    if log_dir:
        results_output_filepath = os.path.join(log_dir, 'results.npy')
    net.test(*test_params, test_meta, save_results=results_output_filepath, **network_args)

    # end session, in case another cross validation is going to be launched
    keras.backend.clear_session()


def build_paths(insert, checkpoint_dir, load_path, log_dir, save_model_path, save_weights_path):
    checkpoint_dir, load_path, log_dir, save_model_path, save_weights_path = [
        p.format(insert) if p else p for p in (checkpoint_dir, load_path, log_dir, save_model_path, save_weights_path)]
    if checkpoint_dir:
        os.makedirs(checkpoint_dir, exist_ok=True)
    if log_dir:
        os.makedirs(log_dir, exist_ok=True)
    if save_model_path:
        os.makedirs(os.path.dirname(save_model_path), exist_ok=True)
    if save_weights_path:
        os.makedirs(os.path.dirname(save_weights_path), exist_ok=True)
    return checkpoint_dir, load_path, log_dir, save_model_path, save_weights_path


def network_opts():
    return {
        'lenet5': {
            'type': classifier.LeNet5, 'frames': True, 'args': {'batch_size': 32}},
        'lenet5_pca': {
            'type': classifier.LeNet5_PCA, 'frames': True, 'args': {'batch_size': 32, 'pca_variance': 2}},
        'lenet5_3d': {
            'type': classifier.LeNet5_3D, 'frames': False, 'args': {'batch_size': 8}},
        'lenet5_3d_pca': {
            'type': classifier.LeNet5_3D_PCA, 'frames': False, 'args': {'batch_size': 8, 'pca_variance': 2}},

        'encoder_decoder': {
            'type': classifier.EncoderDecoder, 'frames': True, 'args': {'batch_size': 256}},
        'r_dae': {
            'type': classifier.RegularisedDAE, 'frames': True, 'args': {'batch_size': 16}},
        'pae': {
            'type': classifier.PartitionedAutoencoder, 'frames': True, 'args': {'batch_size': 128, 'background_batch': 0.25, 'load_model': False}},
        'etl': {
            'type': classifier.EncoderTransferLearning, 'frames': False, 'args': {'batch_size': 8, 'load_model': False}},

        'som': {
            'type': classifier.SelfOrganisingMap, 'frames': True, 'args': {'n_samples': 2000}},
        'som_pca': {
            'type': classifier.SelfOrganisingMap_PCA, 'frames': False, 'args': {'n_samples': 500, 'fclass': [SampleClassification.CAR], 'pca_variance': 0.4}},

        'tsne': {
            'type': classifier.T_SNE, 'frames': True, 'args': {'n_samples': 500}},
        'tsne_pca': {
            'type': classifier.T_SNE_PCA, 'frames': True, 'args': {'n_samples': 2000, 'pca_variance': 0.8}},


        'basic_lstm': {
            'type': classifier.BasicLSTM, 'frames': False, 'args': {'batch_size': 8}},
        'complex_lstm': {
            'type': classifier.ComplexLSTM, 'frames': False, 'args': {'batch_size': 4}},

        'optical_flow': {
            'type': classifier.OpticalFlow, 'frames': False, 'args': {'batch_size': 16}},
        'fusion': {
            'type': classifier.Fusion, 'frames': False, 'args': {'batch_size': 16}},
    }



if __name__ == '__main__':
    if len(sys.argv) < 2:
        print(f'usage: {sys.argv[0]} <sample database> <network> -c (checkpoints) -d (debug) -e <epochs> -i <load path> -l (logs) -o (save) -s <seed> -v (verbose) -x (cross validation)')
        print(f'valid networks: {", ".join(network_opts().keys())}')
        exit(1)

    sample_database_path = pp_utils.DEFAULT_SAMPLE_RECORD if sys.argv[1] == '.' else sys.argv[1]
    network_name = sys.argv[2].lower()
    if network_name not in network_opts():
        print(f'network does not exist: {network_name}')
        exit(1)

    net_opts = network_opts()[network_name]
    network_type = net_opts['type']
    network_args = net_opts['args']

    timestamp = datetime.now().strftime('%y-%m-%d_%H-%M-%S')
    checkpoint_dir = None
    cross_validate = False
    debug = False
    epochs = 0
    kfolds, kfolds_max = 10, 2
    load_path = None
    log_dir = None
    save_model_path = None
    save_weights_path = None
    seed = 0
    verbose = False
    output_root = os.path.join('dump', 'models', network_type.__name__, timestamp)

    # unpack optional arguments
    opts, args = getopt.getopt(sys.argv[3:], 'cde:i:los:vx')
    for o, a in opts:
        if o == '-c':
            # checkpoint_dir = os.path.join('dump', 'checkpoints', f'{network_type.__name__}_{timestamp}')
            checkpoint_dir = os.path.join(output_root, '{}', 'checkpoints')
            # os.makedirs(checkpoint_dir, exist_ok=True)
        elif o == '-d':
            debug = True
        elif o == '-e':
            epochs = int(a)
        elif o == '-i':
            load_path = a
            if not os.path.isfile(load_path):
                raise ValueError(f'load path is not valid: {load_path}')
        elif o == '-l':
            # log_dir = os.path.join('dump', 'logs', f'{network_type.__name__}_{timestamp}')
            log_dir = os.path.join(output_root, '{}') #, 'logs')
            # os.makedirs(log_dir, exist_ok=True)
        elif o == '-o':
            # save_path = a
            save_model_path = os.path.join(output_root, '{}', 'model.h5')
            save_weights_path = os.path.join(output_root, '{}', 'weights.h5')
            # os.makedirs(os.path.dirname(save_path), exist_ok=True)
        elif o == '-s':
            seed = int(a)
        elif o == '-v':
            verbose = True
        elif o == '-x':
            cross_validate = True

    # log args
    if log_dir:
        os.makedirs(output_root, exist_ok=True)
        set_logging(output_root)
        print('Input Command:', ' '.join(sys.argv))
        print('Network Args:', network_type.__name__, network_args)

    # control rng in keras and tf backend
    random.seed(seed)
    np.random.seed(seed)
    tf.set_random_seed(seed)

    # load sample sets
    sample_db = SampleDatabase(sample_database_path)
    as_frames = net_opts['frames']

    if verbose and cross_validate:
        print('Cross validation enabled')

    if debug:
        keras.backend.set_session(tf_debug.LocalCLIDebugWrapperSession(tf.Session()))
        # tf.keras.backend.set_session(tf_debug.TensorBoardDebugWrapperSession(tf.Session(), 'MAXPC-16:6064'))

    # cross validated run
    for o, i, tr_samples, tr_meta, vl_samples, vl_meta, ts_samples, ts_meta in load_sample_sets_cross_validation(sample_db, as_frames, k=kfolds, n=kfolds_max):
        f_checkpoint_dir, f_load_path, f_log_dir, f_save_model_path, f_save_weights_path = build_paths(f'{o}{i}', checkpoint_dir, load_path, log_dir, save_model_path, save_weights_path)
        set_logging(f_log_dir)
        print(f'{"#"*80}\n\t\tOUTER: {o}\tINNER: {i}\n{"#"*80}')
        execute_network(
            f_checkpoint_dir, epochs, f_load_path, f_log_dir, network_type, f_save_model_path, f_save_weights_path, verbose,
            tr_samples, tr_meta, vl_samples, vl_meta, ts_samples, ts_meta, **network_args)

        # single run if flag is false
        if not cross_validate:
            break
