import numpy as np
import os
import sys
from functools import partial
from multiprocessing import Pool, cpu_count
from mmwave.postprocessing import postprocessing, utils
from mmwave.radar import RadarConfig


"""
Takes adc input (after packet reorder) and outputs fft results:
    range power, range-angle map, and range-doppler map
"""


def estimate_total_frame_count(input_filepaths, radar_config):
    if not isinstance(input_filepaths, list):
        input_filepaths = [input_filepaths]
    return int(sum(map(lambda f: os.path.getsize(f) / radar_config.frame_size, input_filepaths), 0))


def get_input_filepaths(input_path):
    """Returns paths to all files matching *_pp.bin in the given directory"""

    if os.path.isfile(input_path):
        # only process the specific file given
        return [input_path]

    elif os.path.isdir(input_path):
        # process all files matching "*_pp.bin" in the directory, i.e. only those marked for post processing
        filepaths = list(filter(
            lambda f: os.path.isfile(f) and f[-7:] == '_pp.bin',
            map(lambda f: os.path.join(input_path, f), os.listdir(input_path))))
        filepaths.sort()  # make alphabetical for ordering Raw_0, Raw_1, etc
        return filepaths
    
    else:
        raise ValueError('Bad input path: ' + input_path)


def get_raw_adc_frames(input_filepaths, radar_config):
    # yield individual frames
    frame_overhang = 0  # bytes of incomplete frame remaining on the next file

    for path in input_filepaths:
        with open(path, 'rb') as data:

            if frame_overhang > 0:
                yield bytearray(chunk) + bytearray(data.read(frame_overhang))
                frame_overhang = 0

            for chunk in iter(partial(data.read, radar_config.frame_size), b''):
                if len(chunk) < radar_config.frame_size:
                    frame_overhang = radar_config.frame_size - len(chunk)
                    continue  # EOF reached, don't return incomplete frame data

                yield bytearray(chunk)


def scene_fft(input_filepaths, radar_config):
    # create containers for each frame produced by the post processing
    est_frames = estimate_total_frame_count(input_filepaths, radar_config)
    range_power_frames = np.zeros((est_frames, radar_config.n_range_bins))  # efx256
    rda_frames = np.zeros((est_frames, radar_config.n_range_bins, radar_config.n_angle_bins))  # efx256x16
    rdm_frames = np.zeros((est_frames, radar_config.n_range_bins, radar_config.n_doppler_bins))  # efx256x128

    print('Estimated Output Sizes:\n\tframes: {}  |  range_power: {}mb  |  rda_frames: {}mb  |  rdm_frames: {}mb'.format(
        est_frames, sys.getsizeof(range_power_frames)//2**20, sys.getsizeof(rda_frames)//2**20, sys.getsizeof(rdm_frames)//2**20))

    # use process pool to compute the raw ADC into usable data
    # use cpu_count number of processes, because it is a processor bounded problem, not io bounded
    with Pool(cpu_count()) as pool:
        fft_func = partial(postprocessing.range_doppler_ft, rconf=radar_config)  # attach the radar config to the func
        for i, result in enumerate(pool.imap(fft_func, get_raw_adc_frames(input_filepaths, radar_config), chunksize=2)):
            range_power_frames[i], rda_frames[i], rdm_frames[i] = result

    return range_power_frames, rda_frames, rdm_frames


if __name__ == '__main__':
    if len(sys.argv) < 5:
        print(f'usage: {sys.argv[0]} <input directory/file> <rp output> <ram output> <rdm output>')
        exit(1)

    input_dir = sys.argv[1]  # this can be a specific file OR a directory containing files to process
    rp_output = sys.argv[2]
    ram_output = sys.argv[3]
    rdm_output = sys.argv[4]

    os.makedirs(os.path.dirname(rp_output), exist_ok=True)
    os.makedirs(os.path.dirname(ram_output), exist_ok=True)
    os.makedirs(os.path.dirname(rdm_output), exist_ok=True)

    radar_config = RadarConfig(utils.get_radar_config_path(input_dir))
    filepaths = get_input_filepaths(input_dir)
    print(f'Processing: {filepaths}\nUsing radar config: {radar_config.source}')

    # fft the adc input scenes
    scene_rp, scene_ram, scene_rdm = scene_fft(filepaths, radar_config)
    np.save(rp_output, scene_rp)
    np.save(ram_output, scene_ram)
    np.save(rdm_output, scene_rdm)
