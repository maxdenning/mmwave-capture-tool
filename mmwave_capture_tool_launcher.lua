require("luacom")
require("io")

PATHNAME = ".\\private$\\mmwave-message-interface"
QUEUEINFO = "MSMQ.MSMQQueueInfo"
MQ_RECEIVE_ACCESS = 1
MQ_DENY_NONE = 0
MQMSG_JOURNAL_NONE = 0

-- Create & open queue --
print("Opening message queue")
queueinfo = luacom.CreateObject(QUEUEINFO)
queueinfo.PathName = PATHNAME
queueinfo.Journal = MQMSG_JOURNAL_NONE
queueinfo:Create()
queue = queueinfo:Open(MQ_RECEIVE_ACCESS, MQ_DENY_NONE)
if queue == nil then
	print("Failed to open MSMQ")
	return 0
end
--print("Emptying queue")
--queue:Purge()


-- Launch python process --
process_handle = io.popen("start cmd /k E:\\git\\mmwave-capture-tool\\mmwave_capture_tool_launcher.bat")

-- Listen loop --
print("Listening...")
while( true )
do
	local msg = queue:Receive()
	print("Recieved: \""..msg.Body.."\"")
		
	if msg.Body == "STOP" then
		print("Exiting")
		break
	else
		ret = assert(loadstring("return "..msg.Body))()
		if ret ~= nil then
			print("Retval: \""..ret.."\"")
		end
	end
end

-- Clean up & exit --
print("Closing message queue")
queue:Close()
queueinfo:Delete()
